from flask import url_for
from . import ocpi

from ocpi.v2_2.core.responses import SuccessResponseJson, ServerErrorResponseJson
from ocpi.v2_2.auth.decorators import require_credentials_token

# 6.1. Version information endpoint
# This endpoint lists all the available OCPI versions and the corresponding URLs 
# to where version specific details such as the supported endpoints can be found.

@ocpi.route('/versions', methods=['GET'])
@require_credentials_token(True)
# Fetch information about the supported versions.
def get_versions():
	try:
		url = url_for('ocpi.get_version_2_2_details', _external = True)
		data = [{ 
			"version": "2.2", # The version number.
			"url": url # URL to the endpoint containing version specific information.
			}]
		return SuccessResponseJson(data).make_response()
	except:
		return ServerErrorResponseJson().make_response()