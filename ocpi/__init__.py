from flask import Blueprint

ocpi = Blueprint('ocpi', __name__)

from .versions import *
from ocpi.v2_2 import version
from ocpi.v2_2.emsp import credentials
from ocpi.v2_2.emsp import locations
from ocpi.v2_2.emsp import sessions
from ocpi.v2_2.emsp import cdrs
from ocpi.v2_2.emsp import tariffs
from ocpi.v2_2.emsp import tokens
from ocpi.v2_2.emsp import commands
from ocpi.v2_2.emsp import clientinfo

