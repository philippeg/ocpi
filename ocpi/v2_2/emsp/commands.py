from flask import request, url_for
from ocpi import ocpi
from ocpi.v2_2.core.responses import SuccessResponseJson, ServerErrorResponseJson, InvalidOrMissingParametersResponseJson
from ocpi.v2_2.objects.commands import CommandResult
from ocpi.v2_2.core.decorators import validate_json_request
from ocpi.v2_2.auth.decorators import require_credentials_token
from ocpi.v2_2.db.controller import DBController

@ocpi.route('/emsp/2.2/commands/<string:command_type>/<string:command_id>', methods=['POST'])
@require_credentials_token()
@validate_json_request
def post_command(command_type, command_id):
	try:
		command_result = CommandResult(request.get_json())
	except ValueError as e:
		return InvalidOrMissingParametersResponseJson().make_response(), 400
	try:
		db = DBController.controller()
		success = db.update_command_result(command_type, command_id, command_result)
		if success:
			return SuccessResponseJson().make_response(), 201
		return InvalidOrMissingParametersResponseJson(status_message='Command {0}/{1} does not exist in the eMSP system'.format(command_type, command_id)).make_response(), 400
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()