from flask import request, url_for
from ocpi import ocpi
from ocpi.v2_2.core.utils import build_next_url
from ocpi.v2_2.core.responses import SuccessResponseJson, ServerErrorResponseJson, InvalidOrMissingParametersResponseJson
from ocpi.v2_2.core.queries import skip, take
from ocpi.v2_2.objects.tokens import Token
from ocpi.v2_2.core.decorators import validate_json_request
from ocpi.v2_2.auth.decorators import require_credentials_token, require_role_authorization
from ocpi.v2_2.db.controller import DBController


@ocpi.route('/emsp/2.2/tokens', methods=['GET'])
@require_credentials_token()
@require_role_authorization()
def get_tokens():
	try:
		date_from = request.args.get('date_from', None) # date_from is inclusive
		date_to = request.args.get('date_to', None) # date_to is exclusive
		offset = int(request.args.get('offset', 0))
		limit = int(request.args.get('limit', 100))

		db = DBController.controller()
		all_tokens = db.get_tokens(date_from, date_to)

		total_count = len(all_tokens)
		queried_tokens = take(skip(all_tokens, offset), limit) 
		response = SuccessResponseJson(queried_tokens).make_response()
		if total_count > offset + limit: 
			next_url = build_next_url(request.base_url, request.args, offset + limit)
			response.headers['Link'] = next_url
		response.headers['X-Total-Count'] = total_count 
		response.headers['X-Limit'] = 100
		return response

	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()



