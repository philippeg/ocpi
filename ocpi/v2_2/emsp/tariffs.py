from flask import request, g
from ocpi import ocpi
from ocpi.v2_2.core.responses import SuccessResponseJson, ServerErrorResponseJson, InvalidOrMissingParametersResponseJson
from ocpi.v2_2.core.decorators import validate_json_request
from ocpi.v2_2.core.queries import update_or_add, first_or_none
from ocpi.v2_2.core.utils import patch_json_type
from ocpi.v2_2.core.decorators import require_last_updated_json_field
from ocpi.v2_2.auth.decorators import require_credentials_token, require_role_authorization
from ocpi.v2_2.db.controller import DBController
from ocpi.v2_2.objects.tariffs import Tariff


@ocpi.route('/emsp/2.2/tariffs/<string:country_code>/<string:party_id>/<string:tariff_id>', methods=['GET'])
@require_credentials_token()
@require_role_authorization()
def get_tariff(country_code, party_id, tariff_id):
	try:
		db = DBController.controller()
		tariff = db.get_tariff(country_code, party_id, tariff_id)
		if tariff:
			return SuccessResponseJson(tariff).make_response()
		return InvalidOrMissingParametersResponseJson(status_message='Tariff {0}/{1}/{2} does not exist in the eMSP system'.format(country_code, party_id, tariff_id)).make_response(), 400
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()


@ocpi.route('/emsp/2.2/tariffs/<string:country_code>/<string:party_id>/<string:tariff_id>', methods=['PUT'])
@require_credentials_token()
@require_role_authorization()
@validate_json_request
def put_tariff(country_code, party_id, tariff_id):
	try:
		tariff = Tariff(request.get_json())
	except ValueError as e:
		return InvalidOrMissingParametersResponseJson().make_response(), 400
	try:
		db = DBController.controller()
		db.add_or_update_tariff(country_code, party_id, tariff_id, tariff)
		return SuccessResponseJson(tariff).make_response(), 200
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()


@ocpi.route('/emsp/2.2/tariffs/<string:country_code>/<string:party_id>/<string:tariff_id>', methods=['DELETE'])
@require_credentials_token()
@require_role_authorization()
def delete_tariff(country_code, party_id, tariff_id):
	try:
		db = DBController.controller()
		success = db.delete_tariff(country_code, party_id, tariff_id)
		if not success:
			return InvalidOrMissingParametersResponseJson(status_message='Session {0}/{1}/{2} does not exist in the eMSP system'.format(country_code, party_id, tariff_id)).make_response(), 400
		return SuccessResponseJson().make_response(), 200
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()