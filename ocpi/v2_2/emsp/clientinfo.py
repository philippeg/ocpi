from flask import request, g
from ocpi import ocpi
from ocpi.v2_2.core.responses import SuccessResponseJson, ServerErrorResponseJson, InvalidOrMissingParametersResponseJson
from ocpi.v2_2.objects.clientinfo import ClientInfo
from ocpi.v2_2.core.decorators import validate_json_request
from ocpi.v2_2.core.queries import update_or_add, first_or_none
from ocpi.v2_2.core.utils import patch_json_type
from ocpi.v2_2.core.decorators import require_last_updated_json_field
from ocpi.v2_2.auth.decorators import require_credentials_token, require_role_authorization
from ocpi.v2_2.db.controller import DBController


@ocpi.route('/emsp/2.2/clientinfo/<string:country_code>/<string:party_id>', methods=['GET'])
@require_credentials_token()
@require_role_authorization()
def get_clientinfo(country_code, party_id):
	try:
		db = DBController.controller()
		clientinfo = db.get_clientinfo(country_code, party_id)
		if clientinfo:
			return SuccessResponseJson(clientinfo).make_response()
		return InvalidOrMissingParametersResponseJson(status_message='ClientInfo {0}/{1} does not exist in the eMSP system'.format(country_code, party_id)).make_response(), 400
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()


@ocpi.route('/emsp/2.2/clientinfo', methods=['PUT'])
@require_credentials_token()
@validate_json_request
def put_clientinfo():
	try:
		client_info = ClientInfo(request.get_json())
	except ValueError as e:
		return InvalidOrMissingParametersResponseJson().make_response(), 400
	try:
		db = DBController.controller()
		db.add_or_update_clientinfo(client_info.country_code, client_info.party_id, client_info)
		return SuccessResponseJson(client_info).make_response(), 200
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()