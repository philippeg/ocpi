from flask import g, url_for, request

from ocpi import ocpi
from ocpi.config import *
from ocpi.v2_2.core.responses import SuccessResponseJson, ServerErrorResponseJson, InvalidOrMissingParametersResponseJson, HttpErrorResponse, NoMatchingOrMissingEnpointsResponseJson, UnableToUseClientAPIResponseJson
from ocpi.v2_2.core.decorators import validate_json_request
from ocpi.v2_2.core.exceptions import RequestException, ClientResponseException
from ocpi.v2_2.core.requests import client_get, client_post, client_put
from ocpi.v2_2.auth.decorators import require_credentials_token
from ocpi.v2_2.auth.services import AuthorizationService
from ocpi.v2_2.db.controller import DBController
from ocpi.v2_2.core.utils import pick_version, get_client_url_for_module
from ocpi.v2_2.objects.credentials import Credentials
from ocpi.v2_2.objects.versions import Versions, Endpoints

# 7. Credentials module
# The credentials module is used to exchange the credentials token that has to be used by parties for authorization of requests.
# Every OCPI request is required to contain a credentials token in the HTTP Authorization header.

@ocpi.route('/emsp/2.2/credentials', methods=['GET'])
@require_credentials_token(True)
# Retrieves the credentials object to access the server’s platform.
def get_credentials():
	# Retrieves the credentials object to access the server’s platform.
	# The request body is empty, the response contains the credentials object to access the server’s platform.
	# This credentials object also contains extra information about the server such as its business details.
	try:
		token = g.credentials['token']
		versions_endpoint = url_for('ocpi.get_versions', _external = True)
		credentials = Credentials(token=token, url=versions_endpoint, roles=[SERVER_CREDENTIALS_ROLE])
		return SuccessResponseJson(credentials).make_response()
	except:
		return ServerErrorResponseJson().make_response()


def get_client_versions_and_endpoints_for_2_2(client_versions_url, token_headers):
	client_versions = client_get(client_versions_url, Versions, headers=token_headers)
	client_version_2_2 = pick_version(client_versions, '2.2')
	if not client_version_2_2:
		raise ClientResponseException('minimal expected implementation version (2.2) is not met')

	client_endpoints = client_get(client_version_2_2.url, Endpoints, headers=token_headers)
	if not client_endpoints:
		raise ClientResponseException('missing or invalid reponse for client available endpoints for v2.2')
	return client_version_2_2, client_endpoints

	
def create_credentials(token):
	versions_endpoint = url_for('ocpi.get_versions', _external = True)
	return Credentials(token=token, url=versions_endpoint, roles=[SERVER_CREDENTIALS_ROLE])

def create_credentials_with_new_token():
	return create_credentials(AuthorizationService.generate_token())
	
@ocpi.route('/emsp/2.2/credentials', methods=['POST'])
@require_credentials_token(True)
@validate_json_request
# Provides the server with a credentials object to access the client’s system (i.e. register).
def post_credentials():
	# Provides the server with credentials to access the client’s system.
	# This credentials object also contains extra information about the client such as its business details.
	# A POST initiates the registration process for this endpoint’s version.
	# The server must also fetch the client’s endpoints for this version.
	# If successful, the server must generate a new credentials token and respond with the client’s new credentials to access the server’s system.
	# The credentials object in the response also contains extra information about the server such as its business details.
	# This method MUST return a HTTP status code 405: method not allowed if the client has already been registered before.
	try:
		credentials = Credentials(request.get_json())
	except ValueError:
		return InvalidOrMissingParametersResponseJson().make_response(), 400
	try:

		db = DBController.controller()
		if db.is_registered(credentials):
			return HttpErrorResponse('the client has already been registered before', 405).make_response()

		token_headers = AuthorizationService.make_header(credentials.token)
		client_version_2_2, client_endpoints = get_client_versions_and_endpoints_for_2_2(credentials.url, token_headers)
		
		new_credentials = create_credentials_with_new_token()
		credentials.token = new_credentials.token
		db.register_client(credentials, client_version_2_2, client_endpoints)

		return SuccessResponseJson(new_credentials).make_response(), 201
	
	except ClientResponseException as e:
		return NoMatchingOrMissingEnpointsResponseJson(status_message= str(e)).make_response()
	except RequestException as e:
		return UnableToUseClientAPIResponseJson(status_message=str(e)).make_response()
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()
	

@ocpi.route('/emsp/2.2/credentials', methods=['PUT'])
@require_credentials_token(False)
@validate_json_request
# Provides the server with an updated credentials object to access the client’s system.
def put_credentials():
	try:
		credentials = Credentials(request.get_json())
	except ValueError:
		return InvalidOrMissingParametersResponseJson().make_response(), 400

	try:
		db = DBController.controller()
		if not db.is_registered(credentials):
			return HttpErrorResponse('the client has not been registered yet', 405).make_response()
		
		token_headers = AuthorizationService.make_header(credentials.token)
		client_version_2_2, client_endpoints = get_client_versions_and_endpoints_for_2_2(credentials.url, token_headers)

		new_credentials = create_credentials_with_new_token()
		credentials.token = new_credentials.token
		db.register_client(credentials, client_version_2_2, client_endpoints)

		return SuccessResponseJson(new_credentials).make_response(), 200

	except ClientResponseException as e:
		return NoMatchingOrMissingEnpointsResponseJson(status_message= str(e)).make_response()
	except RequestException as e:
		return UnableToUseClientAPIResponseJson(status_message=str(e)).make_response()
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()


@ocpi.route('/emsp/2.2/credentials', methods=['DELETE'])
@require_credentials_token(False)
@validate_json_request
# Informs the server that its credentials to the client’s system are now invalid (i.e. unregister).
def delete_credentials():
	try:
		credentials = Credentials(request.get_json())
	except ValueError:
		return InvalidOrMissingParametersResponseJson().make_response(), 400

	try:
		db = DBController.controller()
		if not db.is_registered(credentials):
			return HttpErrorResponse('the client has not been registered yet', 405).make_response()
		
		db.unregister_client(credentials)
		
		return SuccessResponseJson(status_message='client successful unregistered').make_response()

	except ClientResponseException as e:
		return NoMatchingOrMissingEnpointsResponseJson(status_message= str(e)).make_response()
	except RequestException as e:
		return UnableToUseClientAPIResponseJson(status_message=str(e)).make_response()
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()


# 7.1. Use cases
# 7.1.1. Registration

# To register a CPO in an eMSP platform (or vice versa), the CPO must create a unique credentials token that can be used to
# authenticate the eMSP. This credentials token along with the versions endpoint SHOULD be sent to the eMSP in a secure way (for example via email).
# CREDENTIALS_TOKEN_A is given offline, after registration store the CREDENTIALS_TOKEN_C which will be used in future
# exchanges. The CREDENTIALS_TOKEN_A can then be thrown away.
# The eMSP starts the registration process, retrieves the version information and details (using CREDENTIALS_TOKEN_A in the
# HTTP Authorization header). The eMSP generates CREDENTIALS_TOKEN_B, sends it to the CPO in a POST request to the
# credentials module of the CPO. The CPO stores CREDENTIALS_TOKEN_B and uses it for any requests to the eMSP, including
# the version information and details.
def registration(token, url): # TODO unit tests!
	try:
		token_headers = AuthorizationService.make_header(token)

		client_version_2_2, client_endpoints = get_client_versions_and_endpoints_for_2_2(url, token_headers)
		
		# TODO check if required endpoints are available # 7.1.6. respond then with 3003 if not available
		client_credentials_url = get_client_url_for_module('credentials', client_endpoints)
		new_credentials = create_credentials_with_new_token()
		client = client_post(client_credentials_url, new_credentials, Credentials, token_headers)

		db = DBController.controller()
		if db.is_registered(client):
			return HttpErrorResponse('the client has already been registered before', 405).make_response()
		db.register_client(client, client_version_2_2, client_endpoints)

		return SuccessResponseJson(client).make_response()

	except ClientResponseException as e:
		return NoMatchingOrMissingEnpointsResponseJson(status_message= str(e)).make_response()
	except RequestException as e:
		return UnableToUseClientAPIResponseJson(status_message=e).make_response()
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()



# 7.1.2. Updating to a newer version
# At some point, both parties will have implemented a newer OCPI version. 
# To start using the newer version, one party has to send a PUT request to the credentials endpoint of the other party.

def update_to_new_version(token, url): # TODO unit tests!
	try:
		token_headers = AuthorizationService.make_header(token)

		client_version_2_2, client_endpoints = get_client_versions_and_endpoints_for_2_2(url, token_headers)
		
		client_credentials_url = get_client_url_for_module('credentials', client_endpoints)
		credentials = create_credentials(token)
		client = client_put(client_credentials_url, credentials, Credentials, token_headers)

		db = DBController.controller()
		#if not db.is_registered(client):
		#	return HttpErrorResponse('the client has not been registered yet', 405).make_response()
		db.register_client(client, client_version_2_2, client_endpoints)

		return SuccessResponseJson(client).make_response()

	except ClientResponseException as e:
		return NoMatchingOrMissingEnpointsResponseJson(status_message= str(e)).make_response()
	except RequestException as e:
		return UnableToUseClientAPIResponseJson(status_message=e).make_response()
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()


# 7.1.3. Changing endpoints for the current version
# This can be done by following the update procedure for the same version. 
# By sending a PUT request to the credentials endpoint of this version, the other party will fetch and store the corresponding set of endpoints.

def change_endpoints_for_current_version():
	pass

# 7.1.4. Updating the credentials and resetting the credentials token
# The credentials (or parts thereof, such as the credentials token) can be updated by sending the new credentials via a PUT request
# to the credentials endpoint of the current version, similar to the update procedure described above.

def update_credentials_token():
	pass