from flask import request, g
from ocpi import ocpi
from ocpi.v2_2.core.responses import SuccessResponseJson, ServerErrorResponseJson, InvalidOrMissingParametersResponseJson
from ocpi.v2_2.objects.locations import Location, EVSE, Connector, GeoLocation
from ocpi.v2_2.core.decorators import validate_json_request
from ocpi.v2_2.core.queries import update_or_add, first_or_none
from ocpi.v2_2.core.utils import patch_json_type
from ocpi.v2_2.core.decorators import require_last_updated_json_field
from ocpi.v2_2.auth.decorators import require_credentials_token, require_role_authorization
from ocpi.v2_2.db.controller import DBController


@ocpi.route('/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>', methods=['GET'])
@require_credentials_token()
@require_role_authorization()
def get_location(country_code, party_id, location_id): # TODO Note case insensitive strings!
	try:
		db = DBController.controller()
		location = db.get_location(country_code, party_id, location_id)
		if location:
			return SuccessResponseJson(location).make_response()
		return InvalidOrMissingParametersResponseJson(status_message='Location {0}/{1}/{2} does not exist in the eMSP system'.format(country_code, party_id, location_id)).make_response(), 400
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()

@ocpi.route('/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>/<string:evse_uid>', methods=['GET'])
@require_credentials_token()
@require_role_authorization()
def get_evse_info(country_code, party_id, location_id, evse_uid): # TODO Note case insensitive strings!
	try:
		db = DBController.controller()
		evse = db.get_evse(country_code, party_id, location_id, evse_uid)
		if evse:
			return SuccessResponseJson(evse).make_response()
		return InvalidOrMissingParametersResponseJson(status_message='EVSE {0}/{1}/{2}/{3} does not exist in the eMSP system'.format(country_code, party_id, location_id,evse_uid)).make_response(), 400
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()

@ocpi.route('/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>/<string:evse_uid>/<string:connector_id>', methods=['GET'])
@require_credentials_token()
@require_role_authorization()
def get_connector_info(country_code, party_id, location_id, evse_uid, connector_id): # TODO Note case insensitive strings!
	try:
		db = DBController.controller()
		connector = db.get_connector(country_code, party_id, location_id, evse_uid, connector_id)
		if connector:
			return SuccessResponseJson(connector).make_response()
		return InvalidOrMissingParametersResponseJson(status_message='Connector {0}/{1}/{2}/{3}/{4} does not exist in the eMSP system'.format(country_code, party_id, location_id,evse_uid,connector_id)).make_response(), 400
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()


# PUT
@ocpi.route('/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>', methods=['PUT'])
@require_credentials_token()
@require_role_authorization()
@validate_json_request
@require_last_updated_json_field
def put_location(country_code, party_id, location_id): # TODO Note case insensitive strings!
	try:
		location = Location(request.get_json())
	except ValueError as e:
		return InvalidOrMissingParametersResponseJson().make_response(), 400
	try:
		db = DBController.controller()
		db.add_or_update_location(country_code, party_id, location_id, location)
		return SuccessResponseJson(location).make_response(), 200
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()


@ocpi.route('/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>/<string:evse_uid>', methods=['PUT'])
@require_credentials_token()
@require_role_authorization()
@validate_json_request
@require_last_updated_json_field
def put_evse(country_code, party_id, location_id, evse_uid): # TODO Note case insensitive strings!
	try:
		evse = EVSE(request.get_json())
	except ValueError as e:
		return InvalidOrMissingParametersResponseJson().make_response(), 400
	try:
		db = DBController.controller()
		location = db.get_location(country_code, party_id, location_id)
		if not location:
			return InvalidOrMissingParametersResponseJson(status_message='Location {0}/{1}/{2} does not exist in the eMSP system'.format(country_code, party_id, location_id)).make_response(), 400
		
		update_or_add(lambda e: e.uid == evse_uid, evse, location.evses)
		location.last_updated = evse.last_updated
		
		db.add_or_update_location(country_code, party_id, location_id, location)
		return SuccessResponseJson(evse).make_response(), 200
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()


@ocpi.route('/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>/<string:evse_uid>/<string:connector_id>', methods=['PUT'])
@require_credentials_token()
@require_role_authorization()
@validate_json_request
@require_last_updated_json_field
def put_connector(country_code, party_id, location_id, evse_uid, connector_id):
	try:
		connector = Connector(request.get_json())
	except ValueError as e:
		return InvalidOrMissingParametersResponseJson().make_response(), 400
	try:
		db = DBController.controller()
		location = db.get_location(country_code, party_id, location_id)
		if not location:
			return InvalidOrMissingParametersResponseJson(status_message='Location {0}/{1}/{2} does not exist in the eMSP system'.format(country_code, party_id, location_id)).make_response(), 400
		
		evse = first_or_none(lambda evse: evse.uid == evse_uid, location.evses)
		if not evse:
			return InvalidOrMissingParametersResponseJson(status_message='EVSE {0}/{1}/{2}/{3} does not exist in the eMSP system'.format(country_code, party_id, location_id, evse_uid)).make_response(), 400
		
		update_or_add(lambda c: c.id == connector_id, connector, evse.connectors)
		evse.last_updated = connector.last_updated
		location.last_updated = connector.last_updated

		db.add_or_update_location(country_code, party_id, location_id, location)
		return SuccessResponseJson(connector).make_response(), 200
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()

# PATCH
@ocpi.route('/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>', methods=['PATCH'])
@require_credentials_token()
@require_role_authorization()
@validate_json_request
@require_last_updated_json_field
def patch_location(country_code, party_id, location_id): # TODO Note case insensitive strings!
	try:
		db = DBController.controller()
		location = db.get_location(country_code, party_id, location_id)
		if not location:
			return InvalidOrMissingParametersResponseJson(status_message='Location {0}/{1}/{2} does not exist in the eMSP system'.format(country_code, party_id, location_id)).make_response(), 400
		
		updated_location = patch_json_type(location, request.get_json())
		db.add_or_update_location(country_code, party_id, location_id, updated_location)

		return SuccessResponseJson(updated_location).make_response(), 200
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()


@ocpi.route('/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>/<string:evse_uid>', methods=['PATCH'])
@require_credentials_token()
@require_role_authorization()
@validate_json_request
@require_last_updated_json_field
def patch_evse(country_code, party_id, location_id, evse_uid): # TODO Note case insensitive strings!
	try:
		db = DBController.controller()
		location = db.get_location(country_code, party_id, location_id)
		if not location:
			return InvalidOrMissingParametersResponseJson(status_message='Location {0}/{1}/{2} does not exist in the eMSP system'.format(country_code, party_id, location_id)).make_response(), 400
		
		evse = first_or_none(lambda evse: evse.uid == evse_uid, location.evses)
		if not evse:
			return InvalidOrMissingParametersResponseJson(status_message='EVSE {0}/{1}/{2}/{3} does not exist in the eMSP system'.format(country_code, party_id, location_id, evse_uid)).make_response(), 400
		
		updated_evse = patch_json_type(evse, request.get_json())
		update_or_add(lambda e: e.uid == evse_uid, updated_evse, location.evses)
		location.last_updated = updated_evse.last_updated
		db.add_or_update_location(country_code, party_id, location_id, location)

		return SuccessResponseJson(updated_evse).make_response(), 200
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()


@ocpi.route('/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>/<string:evse_uid>/<string:connector_id>', methods=['PATCH'])
@require_credentials_token()
@require_role_authorization()
@validate_json_request
@require_last_updated_json_field
def patch_connector(country_code, party_id, location_id, evse_uid, connector_id):
	try:
		db = DBController.controller()
		location = db.get_location(country_code, party_id, location_id)
		if not location:
			return InvalidOrMissingParametersResponseJson(status_message='Location {0}/{1}/{2} does not exist in the eMSP system'.format(country_code, party_id, location_id)).make_response(), 400
		
		evse = first_or_none(lambda evse: evse.uid == evse_uid, location.evses)
		if not evse:
			return InvalidOrMissingParametersResponseJson(status_message='EVSE {0}/{1}/{2}/{3} does not exist in the eMSP system'.format(country_code, party_id, location_id, evse_uid)).make_response(), 400
		
		connector = first_or_none(lambda c: c.id == connector_id, evse.connectors)
		if not connector:
			return InvalidOrMissingParametersResponseJson(status_message='Connector {0}/{1}/{2}/{3}/{4} does not exist in the eMSP system'.format(country_code, party_id, location_id, evse_uid, connector_id)).make_response(), 400
		
		updated_connector = patch_json_type(connector, request.get_json())

		update_or_add(lambda c: c.id == connector_id, updated_connector, evse.connectors)
		evse.last_updated = location.last_updated = updated_connector.last_updated

		db.add_or_update_location(country_code, party_id, location_id, location)
		return SuccessResponseJson(connector).make_response(), 200
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()



