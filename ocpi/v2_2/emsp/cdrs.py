from flask import request, url_for
from ocpi import ocpi
from ocpi.v2_2.core.responses import SuccessResponseJson, ServerErrorResponseJson, InvalidOrMissingParametersResponseJson
from ocpi.v2_2.core.decorators import validate_json_request
from ocpi.v2_2.objects.cdrs import CDR
from ocpi.v2_2.auth.decorators import require_credentials_token, require_role_authorization
from ocpi.v2_2.db.controller import DBController


@ocpi.route('/emsp/2.2/cdrs/<string:country_code>/<string:party_id>/<string:cdr_id>', methods=['GET'])
@require_credentials_token()
@require_role_authorization()
def get_cdr(country_code, party_id, cdr_id): # TODO Note case insensitive strings!
	try:
		db = DBController.controller()
		cdr = db.get_cdr(country_code, party_id, cdr_id)
		if cdr:
			return SuccessResponseJson(cdr).make_response()
		return InvalidOrMissingParametersResponseJson(status_message='CDR {0}/{1}/{2} does not exist in the eMSP system'.format(country_code, party_id, cdr_id)).make_response(), 400
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()


@ocpi.route('/emsp/2.2/cdrs/<string:country_code>/<string:party_id>', methods=['POST'])
@require_credentials_token()
@require_role_authorization()
@validate_json_request
def post_cdr(country_code, party_id):
	try:
		cdr = CDR(request.get_json())
	except ValueError as e:
		return InvalidOrMissingParametersResponseJson().make_response(), 400
	try:
		db = DBController.controller()
		db.add_cdr(country_code, party_id, cdr)
		location = { 'location': url_for('ocpi.get_cdr', _external = True, country_code=country_code, party_id=party_id,cdr_id=cdr.id)}
		return SuccessResponseJson(location).make_response(), 201
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()

