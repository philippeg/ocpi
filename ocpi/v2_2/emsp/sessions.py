from flask import request, g
from ocpi import ocpi
from ocpi.v2_2.core.responses import SuccessResponseJson, ServerErrorResponseJson, InvalidOrMissingParametersResponseJson
from ocpi.v2_2.core.decorators import validate_json_request
from ocpi.v2_2.core.queries import update_or_add, first_or_none
from ocpi.v2_2.core.utils import patch_json_type
from ocpi.v2_2.core.decorators import require_last_updated_json_field
from ocpi.v2_2.auth.decorators import require_credentials_token, require_role_authorization
from ocpi.v2_2.db.controller import DBController
from ocpi.v2_2.objects.sessions import Session

@ocpi.route('/emsp/2.2/sessions/<string:country_code>/<string:party_id>/<string:session_id>', methods=['GET'])
@require_credentials_token()
@require_role_authorization()
def get_session(country_code, party_id, session_id): # TODO Note case insensitive strings!
	try:
		db = DBController.controller()
		session = db.get_session(country_code, party_id, session_id)
		if session:
			return SuccessResponseJson(session).make_response()
		return InvalidOrMissingParametersResponseJson(status_message='Session {0}/{1}/{2} does not exist in the eMSP system'.format(country_code, party_id, session_id)).make_response(), 400
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()


@ocpi.route('/emsp/2.2/sessions/<string:country_code>/<string:party_id>/<string:session_id>', methods=['PUT'])
@require_credentials_token()
@require_role_authorization()
@validate_json_request
@require_last_updated_json_field
def put_session(country_code, party_id, session_id):
	try:
		session = Session(request.get_json())
	except ValueError as e:
		return InvalidOrMissingParametersResponseJson().make_response(), 400
	try:
		db = DBController.controller()
		db.add_or_update_session(country_code, party_id, session_id, session)
		return SuccessResponseJson(session).make_response(), 200
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()


@ocpi.route('/emsp/2.2/sessions/<string:country_code>/<string:party_id>/<string:session_id>', methods=['PATCH'])
@require_credentials_token()
@require_role_authorization()
@validate_json_request
@require_last_updated_json_field
def patch_session(country_code, party_id, session_id):
	try:
		db = DBController.controller()
		session = db.get_session(country_code, party_id, session_id)
		if not session:
			return InvalidOrMissingParametersResponseJson(status_message='Session {0}/{1}/{2} does not exist in the eMSP system'.format(country_code, party_id, session_id)).make_response(), 400
		
		updated_session = patch_json_type(session, request.get_json())
		db.add_or_update_session(country_code, party_id, session_id, updated_session)

		return SuccessResponseJson(updated_session).make_response(), 200
	except Exception as e:
		return ServerErrorResponseJson(status_message=str(e)).make_response()