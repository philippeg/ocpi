import json

# Versions
def pick_version(client_versions, version_number):
	try:
		return next((v for v in client_versions if v.version == version_number), None)
	except Exception: 
		return None
		
# Routing
def get_client_url_for_module(identifier, endpoints):
	try:
		return next((e.url for e in endpoints.endpoints if e.identifier == identifier), None)
	except Exception: 
		return None

# PATCH methods
def patch_json_type(json_type_instance, json_update_dict):
	merged_dict = dict(list(json_type_instance.items()) +  list(json_update_dict.items())) 
	instanceType = type(json_type_instance)
	return instanceType(merged_dict)

# Pagination
def build_next_url(base_url, request_args, offset):
	separator = '?'
	for (arg,value) in request_args.items():
		if arg != 'offset':
			base_url +=  separator + arg + '=' + value
			separator = '&'
	return "<{0}{1}offset={2}>; rel=\"next\"".format(base_url, separator, offset)