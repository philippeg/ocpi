from werkzeug.exceptions import BadRequest
from functools import wraps
from flask import request
from .responses import HttpErrorResponse

# 5. Status codes
# The transport layer ends after a message is correctly parsed into a (semantically unvalidated) JSON structure. 
# When a message does not contain a valid JSON string, the HTTP error 400 - Bad request MUST be returned.

def validate_json_request(func):
	@wraps(func)
	def wrapper(*args, **kwargs):
		try:
			request.json
			if request.json is None:
				return HttpErrorResponse("Content-Type=application/json required", 400).make_response()
		except BadRequest:
			return HttpErrorResponse("payload must be a valid json", 400).make_response()
		return func(*args, **kwargs)
	return wrapper

def require_last_updated_json_field(func):
	@wraps(func)
	def wrapper(*args, **kwargs):
		if not 'last_updated' in request.json:
			return HttpErrorResponse('Any request to the PUT/PATCH methods SHALL contain the last_updated field.', 400).make_response()
		return func(*args, **kwargs)
	return wrapper
