from itertools import islice, takewhile, dropwhile

def any_item(predicate, source):
	return any(predicate(e) for e in source)

def all_item(predicate, source):
	return all(predicate(e) for e in source)

def min_value(predicate, source):
	return min(predicate(e) for e in source)

def max_value(predicate, source):
	return max(predicate(e) for e in source)

def take(source, count):
	return list(islice(source, count))

def skip(source, count):
	return list(islice(source, count, None))

def first_or_none(predicate, source):
	return next((e for e in source if predicate(e)), None)

def count(predicate, source):
	return sum(1 for e in source if predicate(e))

def select(predicate, source):
	return list(map(predicate, source))

def where(predicate, source):
	return list(filter(predicate, source))

def update_or_add(predicate, value, source):
	for index,e in enumerate(source):
		if predicate(e):
			source[index] = value
			return source
	source.append(value)
	return source
