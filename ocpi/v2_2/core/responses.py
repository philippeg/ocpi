"""
	simple classes that define standardized JSON structures for OCPI responses
"""

from flask import jsonify, json
#import simplejson as json
import datetime


# 5. Status codes
#There are two types of status codes:
# - Transport related (HTTP)
# - Content related (OCPI)
# The transport layer ends after a message is correctly parsed into a (semantically unvalidated) JSON structure.
# When a message does not contain a valid JSON string, the HTTP error 400 - Bad request MUST be returned.
# If a request is syntactically valid JSON and addresses an existing resource, a HTTP error MUST NOT be returned.
# Those requests are supposed to have reached the OCPI layer.
# If the requested resource does NOT exist, the server SHOULD return a HTTP 404 - Not Found.
# When the server receives a valid OCPI object it SHOULD respond with:
# - HTTP 200 - Ok when the object already existed and has successfully been updated.
# - HTTP 201 - Created when the object has been newly created in the server system.

class ResponseHttp():
	__msg = None
	__code = 0

	def __init__(self, msg: str, code: int=200):
		self.message = msg
		self.return_code = code

	@property
	def return_code(self):
		return self.__code

	@return_code.setter
	def return_code(self, code: int):
		self.__code = code

	@property
	def message(self):
		return self.__msg

	@message.setter
	def message(self, msg : str):
		self.__msg = msg

	def make_response(self):
		return self.__msg, self.__code




class HttpErrorResponse(ResponseHttp):
	""" json structure for returning client-caused error info to clients """
	def __init__(self, message: str, error_code: int = 0):
		super().__init__(message, error_code)


#class HttpExceptionResponseJson(ResponseHttp):
#	""" json structure for returning server-side errors/exceptions info to clients """
#
#	def __init__(self, message: str, exc: Exception = None):
#		super().__init__({"exception": message}, 500)

# 4.1.6. Response format
class ResponseJson():
	__data = {} # Contains the actual response data object or list of objects from each request
	__status_code = 0 # Response code indicates how the request was handled. To avoid confusion with HTTP codes, at least four digits are used.
	__status_message = None # An optional status message which may help when debugging.
	__timestamp = None # The time this message was generated.

	def __init__(self, data: dict, status_code: int, status_message: str = None):
		""" define a custom JSON response and return code """

		# this code looks weird but its to properly handle data types that can't be handled
		# by the standard flask jsonify method (e.g. Decimals). so we use simplejson to dump the
		# raw json to a string (simplejson converts decimals to strings) and then load it
		# back to json for flask to take care of later
		self.data = data #json.loads(json.dumps(data))
		self.status_code = status_code
		self.status_message = status_message

	def __str__(self):
		return json.dumps(self.__data)

	@property
	def data(self):
		return self.__data

	@data.setter
	def data(self, data):
		self.__data = data

	@property
	def status_code(self):
		return self.__status_code

	@status_code.setter
	def status_code(self, code: int):
		self.__status_code = code

	@property
	def status_message(self):
		return self.__status_message

	@status_message.setter
	def status_message(self, status_message):
		self.__status_message = status_message

	def __utcnow(self):
		now = datetime.datetime.utcnow().replace(microsecond=0).isoformat()
		return now

	def make_response(self):
		return jsonify(data = self.data, 
				 status_code= self.status_code,
				 status_message = self.status_message,
				 timestamp = self.__utcnow())



# 5.1. 1xxx: Success
class SuccessResponseJson(ResponseJson):
	""" Generic success code """
	def __init__(self, data: dict = None, status_message: str = "Success"):
		super().__init__(data, 1000, status_message)


# 5.2. 2xxx: Client errors
# Errors detected by the server in the message sent by a client where the client did something wrong.
class ClientErrorResponseJson(ResponseJson):
	""" Generic client error """
	def __init__(self, data: dict = None, status_message: str = "Generic client error"):
		super().__init__(data, 2000, status_message)

class InvalidOrMissingParametersResponseJson(ResponseJson):
	""" Invalid or missing parameters """
	def __init__(self, data: dict = None, status_message: str = "Invalid or missing parameters"):
		super().__init__(data, 2001, status_message)

class NotEnoughInformationResponseJson(ResponseJson):
	""" Not enough information, for example: Authorization request with too little information. """
	def __init__(self, data: dict = None, status_message: str = "Not enough information"):
		super().__init__(data, 2002, status_message)

class UnknownLocationResponseJson(ResponseJson):
	""" Unknown Location, for example: Command: START_SESSION with unknown location. """
	def __init__(self, data: dict = None, status_message: str = "Unknown Location"):
		super().__init__(data, 2003, status_message)

# 5.3. 3xxx: Server errors
# Error during processing of the OCPI payload in the server. 
# The message was syntactically correct but could not be processed by the server.
class ServerErrorResponseJson(ResponseJson):
	""" Generic server error """
	def __init__(self, data: dict = None, status_message: str = "Generic server error"):
		super().__init__(data, 3000, status_message)

class UnableToUseClientAPIResponseJson(ResponseJson):
	""" Unable to use the client’s API. 
	For example during the credentials registration: When the initializing
	party requests data from the other party during the open POST call to its credentials endpoint. If
	one of the GETs can not be processed, the party should return this error in the POST response. """
	def __init__(self, data: dict = None, status_message: str = "Unable to use the client’s API"):
		super().__init__(data, 3001, status_message)

class UnsupportedVersionResponseJson(ResponseJson):
	"""Unsupported version. """
	def __init__(self, data: dict = None, status_message: str = "Unsupported version"):
		super().__init__(data, 3002, status_message)

class NoMatchingOrMissingEnpointsResponseJson(ResponseJson):
	"""No matching endpoints or expected endpoints missing between parties. Used during the
	registration process if the two parties do not have any mutual modules or endpoints available, or
	the minimal implementation expected by the other party is not been met. """
	def __init__(self, data: dict = None, status_message: str = "No matching endpoints or expected endpoints missing"):
		super().__init__(data, 3003, status_message)


# 5.4. 4xxx: Hub errors
# When a server encounters an error, client side error (2xxx) or server side error (3xxx), 
# it sends the status code to the Hub. The Hub SHALL then forward this error to the client 
# which sent the request (when the request was not a Broadcast Push).
class UnknownReceiverResponseJson(ResponseJson):
	"""Unsupported version. """
	def __init__(self, data: dict = None, status_message: str = "Unknown receiver (TO address is unknown)"):
		super().__init__(data, 4001, status_message)

class TimeoutOnForwardedRequestResponseJson(ResponseJson):
	"""Unsupported version. """
	def __init__(self, data: dict = None, status_message: str = "Timeout on forwarded request (message is forwarded, but request times out)"):
		super().__init__(data, 4002, status_message)

class ConnectionProblemResponseJson(ResponseJson):
	"""Unsupported version. """
	def __init__(self, data: dict = None, status_message: str = "Connection problem (receiving party is not connected)"):
		super().__init__(data, 4003, status_message)
