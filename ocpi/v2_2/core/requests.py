import requests
from .exceptions import RequestException

def client_get(url, response_type, headers = None):
	try:
		response = requests.get(url, headers=headers)
		return response_type(response)
	except Exception as e:
		raise RequestException(e)

def client_post(url, json, response_type, headers = None):
	try:
		response = requests.post(url, json = json, headers=headers)
		return response_type(response)
	except Exception as e:
		raise RequestException(e)

def client_put(url, json, response_type, headers = None):
	try:
		response = requests.put(url, json = json, headers=headers)
		return response_type(response)
	except Exception as e:
		raise RequestException(e)