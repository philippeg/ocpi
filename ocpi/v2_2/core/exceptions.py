import requests

class RequestException(requests.RequestException):
	"""Raise for client request specific exception"""

class ClientResponseException(Exception):
	"""Raise when a client response does no met expectations"""