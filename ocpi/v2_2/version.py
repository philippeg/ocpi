from flask import url_for
from ocpi import ocpi

from ocpi.v2_2.core.responses import SuccessResponseJson, ServerErrorResponseJson
from ocpi.v2_2.auth.decorators import require_credentials_token

# 6.2. Version details endpoint
# This endpoint lists the supported endpoints and their URLs for a specific OCPI version.
# Via the version details, the parties can exchange which modules are implemented for a specific version of OCPI,
# which interface role is implemented, and what the endpoint URL is for this interface.

@ocpi.route('/2.2', methods=['GET'])
@require_credentials_token(True)
# Fetch information about the supported endpoints for this version.
def get_version_2_2_details():
	try:
		url_credentials = url_for('ocpi.get_credentials', _external = True)
		url_locations = url_for('ocpi.get_location', _external = True, country_code='C', party_id='P',location_id='L').split('/C',1)[0]
		url_sessions = url_for('ocpi.get_session', _external = True, country_code='C', party_id='P',session_id='L').split('/C',1)[0]
		url_cdrs = url_for('ocpi.get_cdr', _external = True, country_code='C', party_id='P',cdr_id='L').split('/C',1)[0]
		url_tariffs = url_for('ocpi.get_tariff', _external = True, country_code='C', party_id='P',tariff_id='L').split('/C',1)[0]
		url_tokens = url_for('ocpi.get_tokens', _external = True)
		url_clientinfo = url_for('ocpi.get_clientinfo', _external = True, country_code='C', party_id='P').split('/C',1)[0]
	

		credentials_endpoint = {
			"identifier": "credentials", # Endpoint identifier
			"role": "MSP", #  Interface role this endpoint implements
			"url": url_credentials # url: URL to the endpoint
			}
		locations_endpoint = {
			"identifier": "locations", 
			"role": "MSP", 
			"url": url_locations
			}
		sessions_endpoint = {
			"identifier": "sessions", 
			"role": "MSP", 
			"url": url_sessions
			}
		cdrs_endpoint = {
			"identifier": "cdrs", 
			"role": "MSP", 
			"url": url_cdrs
			}
		tariffs_endpoint = {
			"identifier": "tariffs", 
			"role": "MSP", 
			"url": url_tariffs
			}
		tokens_endpoint = {
			"identifier": "tokens", 
			"role": "MSP", 
			"url": url_tokens
			}
		clientinfo_endpoint = {
			"identifier": "hubclientinfo", 
			"role": "MSP", 
			"url": url_clientinfo
			}	

		data = {
			"version": "2.2", # The version number.
			"endpoints": [credentials_endpoint, locations_endpoint, sessions_endpoint, cdrs_endpoint, tariffs_endpoint, tokens_endpoint, clientinfo_endpoint] # A list of supported endpoints for this version.
			}
		return SuccessResponseJson(data).make_response()
	except Exception:
		return ServerErrorResponseJson().make_response()

