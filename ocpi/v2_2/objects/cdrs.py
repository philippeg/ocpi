import warlock

from ocpi.v2_2.objects.common import *
from ocpi.v2_2.objects.tariffs import tariff_schema

# OCPI 2.2 10.4.8
signed_value_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'SignedValue',
	'properties': {
		'nature': {'type':'string', 'maxLength':32},
		'plain_data': {'type':'string', 'maxLength':512},
		'signed_data': {'type':'string', 'maxLength':512},
	},
	'required':['nature', 'plain_data', 'signed_data'],
	'additionalProperties': False,
}

SignedValue = warlock.model_factory(signed_value_schema)

# OCPI 2.2 10.4.7
signed_data_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'SignedData',
	'properties': {
		'encoding_method': {'type':'string', 'maxLength':36},
		'encoding_method_version': {'type':'integer'},
		'public_key': {'type':'string', 'maxLength':512},
		'signed_values': {'type':'array', 'items': signed_value_schema, 'minItems':1  },
		'url': {'type':'string', 'maxLength':512},
	},
	'required':['encoding_method', 'signed_values'],
	'additionalProperties': False,
}

SignedData = warlock.model_factory(signed_data_schema)

# OCPI 2.2 10.4.5
cdr_token_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'CdrToken',
	'properties': {
		'uid': {'type':'string', 'maxLength':36},
		'type': token_type_schema,
		'contract_id': {'type':'string', 'maxLength':36},
	},
	'required':['uid', 'type', 'contract_id'],
	'additionalProperties': False,
}

CdrToken = warlock.model_factory(cdr_token_schema)

# OCPI 2.2 10.4.1
auth_method_schema = {'type':'string', 'enum':['AUTH_REQUEST', 'COMMAND', 'WHITELIST']}

# OCPI 2.2 10.4.3
cdr_dimension_type_schema = {'type':'string', 'enum':['CURRENT', 'ENERGY', 'ENERGY_EXPORT', 'ENERGY_IMPORT', 'MAX_CURRENT','MIN_CURRENT','MAX_POWER', 'MIN_POWER', 'PARKING_TIME', 'POWER', 'RESERVATION_TIME', 'STATE_OF_CHARGE', 'TIME']}


# OCPI 2.2 10.4.2
cdr_dimension_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'CdrDimension',
	'properties': {
		'type': cdr_dimension_type_schema,
		'volume': {'type': 'number'},
	},
	'required':['type', 'volume'],
	'additionalProperties': False,
}

CdrDimension = warlock.model_factory(cdr_dimension_schema)

# OCPI 2.2 10.4.4
cdr_location_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'CdrLocation',
	'properties': {
		'id': {'type':'string', 'maxLength':36},
		'name': {'type':'string', 'maxLength':255},
		'address': {'type':'string', 'maxLength':45},
		'city': {'type':'string', 'maxLength':45},
		'postal_code': {'type':'string', 'maxLength':10},
		'country': {'type':'string', 'maxLength':3},
		'coordinates': geo_location_schema,
		'evse_uid': {'type':'string', 'maxLength':36},
		'evse_id': {'type':'string', 'maxLength':48},
		'connector_id': {'type':'string', 'maxLength':36},
		'connector_standard': connector_type_schema,
		'connector_format': connector_format_schema,
		'connector_power_type': power_type_schema,
	},
	'required':['id', 'address', 'city', 'postal_code', 'country', 'coordinates','evse_uid', 'evse_id', 'connector_id', 'connector_standard', 'connector_format', 'connector_power_type'],
	'additionalProperties': False,
}

CdrLocation = warlock.model_factory(cdr_location_schema)


# OCPI 2.2 10.4.6
charging_period_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'ChargingPeriod',
	'properties': {
		'start_date_time': date_time_schema,
		'dimensions': {'type':'array', 'items': cdr_dimension_schema, 'minItems':1 },
		'tariff_id': {'type':'string', 'maxLength':36},
	},
	'required':['start_date_time', 'dimensions'],
	'additionalProperties': False,
}

ChargingPeriod = warlock.model_factory(charging_period_schema)



# OCPI 2.2 10.3.1
cdr_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'CDR',
	'properties': {
		'country_code': {'type':'string', 'maxLength':2},
		'party_id': {'type':'string', 'maxLength':3},
		'id': {'type':'string', 'maxLength':39},
		'start_date_time': date_time_schema,
		'end_date_time': date_time_schema,
		'session_id': {'type':'string', 'maxLength':36},
		'cdr_token': cdr_token_schema,
		'auth_method': auth_method_schema,
		'authorization_reference': {'type':'string', 'maxLength':36},
		'cdr_location': cdr_location_schema,
		'meter_id': {'type':'string', 'maxLength':255},
		'currency': {'type':'string', 'maxLength':3},
		'tariffs': {'type':'array', 'items': tariff_schema },
		'charging_periods': {'type':'array', 'items': charging_period_schema, 'minItems':1  }, 
		'signed_data': signed_data_schema,
		'total_cost': price_schema,
		'total_fixed_cost': price_schema,
		'total_energy': {'type': 'number'},
		'total_energy_cost': price_schema,
		'total_time': {'type': 'number'},
		'total_time_cost': price_schema,
		'total_parking_time': {'type': 'number'},
		'total_parking_cost': price_schema,
		'total_reservation_cost': price_schema,
		'remark': {'type':'string', 'maxLength':255},
		'credit': {'type': 'boolean'},
		'credit_reference_id': {'type':'string', 'maxLength':36},
		'last_updated': date_time_schema,
	},
	'required':['country_code', 'party_id', 'id', 'start_date_time', 'end_date_time', 'cdr_token','auth_method', 'cdr_location', 'currency', 'charging_periods', 'total_cost', 'total_energy', 'total_time','last_updated'],
	'additionalProperties': False,
}

CDR = warlock.model_factory(cdr_schema)