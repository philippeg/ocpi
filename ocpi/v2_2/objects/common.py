import warlock 

date_time_schema = {'type':'string', 'format':'date-time', 'maxLength':26, 'pattern':"^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}"}

response_schema = {
"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'Response',
	'properties': {
		'data': {'type':['array', 'object','string', 'null']},
		'status_code': {'type':'integer'},
		'status_message': {'type':'string'},
		'timestamp': date_time_schema,
	},
	'required':['status_code', 'timestamp'],
	'additionalProperties': False,
}

Response = warlock.model_factory(response_schema)


# OCPI 2.2 16.5.1
role_schema = {'type':'string', 'enum':['CPO', 'EMSP', 'HUB', 'NAP', 'NSP', 'OTHER', 'SCSP']}

# OCPI 2.2 12.4.4
token_type_schema = {'type':'string', 'enum':['AD_HOC_USER', 'APP_USER', 'OTHER', 'RFID']}



# OCPI 2.2 8.4.1
display_text_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'DisplayText',
	'properties': {
		'language': {'type':'string', 'maxLength':2},
		'text': {'type':'string', 'maxLength':512}
	},
	'required':['language', 'text'],
	'additionalProperties': False,
}

DisplayText = warlock.model_factory(display_text_schema)

# OCPI 2.2 16.5
price_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'Price',
	'properties': {
		'excl_vat': {'type': 'number'},
		'incl_vat': {'type': 'number'},
	},
	'required':['excl_vat'],
	'additionalProperties': False,
}

Price = warlock.model_factory(price_schema)

# OCPI 2.2 8.4.8
energy_source_category_schema = {'type':'string', 'enum':['NUCLEAR', 'GENERAL_FOSSIL', 'COAL', 'GAS', 'GENERAL_GREEN', 'SOLAR', 'WIND' ,'WATER']}


# OCPI 2.2 8.4.7
energy_source_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'EnergySource',
	'properties': {
		'source': energy_source_category_schema,
		'percentage': {'type': 'number'},
	},
	'required':['source', 'percentage'],
	'additionalProperties': False,
}

EnergySource = warlock.model_factory(energy_source_schema)

# OCPI 2.2 8.4.10
environmental_impact_category_schema = {'type':'string', 'enum':['NUCLEAR_WASTE', 'CARBON_DIOXIDE']}


# OCPI 2.2 8.4.9
environmental_impact_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'EnvironmentalImpact',
	'properties': {
		'category': environmental_impact_category_schema, 
		'amount': {'type':'number'},
	},
	'required':['category', 'amount'],
	'additionalProperties': False,
}

EnvironmentalImpact = warlock.model_factory(environmental_impact_schema)

# OCPI 2.2 8.4.6
energy_mix_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'EnergyMix',
	'properties': {
		'is_green_energy': {'type':'boolean'},
		'energy_sources': {'type':'array', 'items': energy_source_schema },
		'environ_impact': {'type':'array', 'items': environmental_impact_schema },
		'supplier_name': {'type':'string', 'maxLength':64},
		'energy_product_name': {'type':'string', 'maxLength':64},
	},
	'required':['is_green_energy'],
	'additionalProperties': False,
}

EnergyMix = warlock.model_factory(energy_mix_schema)


# OCPI 2.2 8.4.17
location_type_schema = {'type':'string', 'enum':['ON_STREET', 'PARKING_GARAGE', 'UNDERGROUND_GARAGE', 'PARKING_LOT', 'OTHER', 'UNKNOWN']}

latitude_schema = { 'type': 'string', 'maxLength':10, 'pattern':"^-?[0-9]{1,2}\.[0-9]{5,7}"}
longitude_schema = { 'type': 'string', 'maxLength':11, 'pattern':"^-?[0-9]{1,3}\.[0-9]{5,7}"}


# OCPI 2.2 8.4.13
geo_location_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'GeoLocation',
	'properties': {
		'latitude': latitude_schema,
		'longitude': longitude_schema,
	},
	'required':['latitude', 'longitude'],
	'additionalProperties': False,
}

GeoLocation = warlock.model_factory(geo_location_schema)



# OCPI 2.2 8.4.1
additional_geo_location_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'AdditionalGeoLocation',
	'properties': {
		'latitude': latitude_schema,
		'longitude': longitude_schema,
		'name': display_text_schema
	},
	'required':['latitude', 'longitude'],
	'additionalProperties': False,
}

AdditionalGeoLocation = warlock.model_factory(additional_geo_location_schema)

# OCPI 2.2 8.4.5
connector_type_schema = {'type':'string', 'enum':['CHADEMO', 'DOMESTIC_A', 'DOMESTIC_B','DOMESTIC_C', 'DOMESTIC_D', 'DOMESTIC_E', 'DOMESTIC_F', 'DOMESTIC_G', 'DOMESTIC_H', 'DOMESTIC_I', 'DOMESTIC_J', 'DOMESTIC_K', 'DOMESTIC_L', 'IEC_60309_2_single_16', 
'IEC_60309_2_three_16', 'IEC_60309_2_three_32', 'IEC_60309_2_three_64', 
'IEC_62196_T1', 'IEC_62196_T1_COMBO', 'IEC_62196_T2','IEC_62196_T2_COMBO', 'IEC_62196_T3A', 'IEC_62196_T3C',
'PANTOGRAPH_BOTTOM_UP', 'PANTOGRAPH_TOP_DOWN', 'TESLA_R', 'TESLA_S']}

# OCPI 2.2 8.4.4
connector_format_schema = {'type':'string', 'enum':['SOCKET', 'CABLE']}

# OCPI 2.2 8.4.19
power_type_schema = {'type':'string', 'enum':['AC_1_PHASE', 'AC_3_PHASE', 'DC']}



# OCPI 2.2 8.4.16
image_category_schema = {'type':'string', 'enum':['CHARGER', 'ENTRANCE', 'LOCATION', 'NETWORK', 'OPERATOR', 'OTHER', 'OWNER']}

# OCPI 2.2 8.4.15
image_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'Image',
	'properties': {
		'url': {'type':'string', 'format':'hostname'},
		'thumbnail': {'type':'string'},
		'category': image_category_schema,
		'type': {'type':'string', 'maxLength':4},
		'width': {'type':'integer'},
		'height': {'type':'integer'},
	},
	'required':['url', 'category', 'type'],
	'additionalProperties': False,
}
Image = warlock.model_factory(image_schema)


# OCPI 2.2 8.4.2
business_details_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'BusinessDetails',
	'properties': {
		'name': {'type':'string', 'maxLength': 100},
		'website': {'type':'string', 'format':'hostname'},
		'logo': image_schema,
	},
	'required':['name'],
	'additionalProperties': False,
}
BusinessDetails = warlock.model_factory(business_details_schema)
