import warlock

from ocpi.v2_2.objects.common import *

# OCPI 2.2 6.2.5
version_number_schema = {'type':'string', 'enum':['2.0', '2.1.1', '2.2']}

# OCPI 2.2 6.1.2
version_schema ={
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'Version',
	'properties': {
		'version': version_number_schema,
		'url': {'type':'string', 'format':'hostname'},
	},
	'required':['version', 'url'],
	'additionalProperties': False,
}

Version = warlock.model_factory(version_schema)

# warlock seems not to support pure array schema so we write our own class for Versions validation
class Versions(list):
	def __init__(self, versions):
		try:
			versions[0] # minItems = 1
			[Version(v) for v in versions]
		except KeyError as exc:
			raise ValueError('Failed validating: versions object must contain at least one version')
		except Exception as exc:
			raise ValueError(str(exc))
		else:
			list.__init__(self, versions)

# OCPI 2.2 6.2.4
module_id_schema = {'type':'string', 'enum':['cdrs','chargingprofiles','commands','credentials',
	'hubclientinfo','locations', 'sessions', 'tariffs', 'tokens']}


# OCPI 2.2 6.2.2
endpoint_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'Endpoint',
	'properties': {
		'identifier': module_id_schema,
		# The OCPI specifications are inconsistent regarding the role's property type (InterfaceRole? Role? but EMSP <> MSP used in the examples) so we define it as a simple string 
		'role': { 'type': 'string'}, 
		'url': {'type':'string', 'format':'hostname'},
	},
	'required':['identifier', 'role', 'url'],
	'additionalProperties': False,
}

Endpoint = warlock.model_factory(endpoint_schema)


# OCPI 2.2 6.2.1
endpoints_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'Endpoints',
	'properties': {
		'version': version_number_schema,
		'endpoints': {'type':'array', 'items': endpoint_schema, 'minItems':1 }
	},
	'required':['version', 'endpoints'],
	'additionalProperties': False,
}

Endpoints = warlock.model_factory(endpoints_schema)