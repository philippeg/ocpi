import warlock 
from ocpi.v2_2.objects.common import token_type_schema, date_time_schema

# OCPI 2.2 12.4.5
whitelist_type_schema = {'type':'string', 'enum':['ALWAYS', 'ALLOWED', 'ALLOWED_OFFLINE', 'NEVER']}

# OCPI 2.2 9.4.2
profile_type_schema = {'type':'string', 'enum':['CHEAP', 'FAST', 'GREEN', 'REGULAR']}

# OCPI 2.2 12.4.2
energy_contract_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'EnergyContract',
	'properties': {
		'supplier_name': {'type':'string', 'maxLength':64},
		'contract_id': {'type':'string', 'maxLength':64},
	},
	'required':['supplier_name'],
	'additionalProperties': False,
}

EnergyContract = warlock.model_factory(energy_contract_schema)

# OCPI 2.2 12.3.2
token_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'Token',
	'properties': {
		'country_code': {'type':'string', 'maxLength':2},
		'party_id': {'type':'string', 'maxLength':3},
		'uid': {'type':'string', 'maxLength':36},
		'type': token_type_schema,
		'contract_id': {'type':'string', 'maxLength':36},
		'visual_number': {'type':'string', 'maxLength':64},
		'issuer': {'type':'string', 'maxLength':64},
		'group_id': {'type':'string', 'maxLength':36},
		'valid': {'type':'boolean'},
		'whitelist': whitelist_type_schema,
		'language': {'type':'string', 'maxLength':2},
		'default_profile_type': profile_type_schema,
		'energy_contract': energy_contract_schema,
		'last_updated': date_time_schema,
	},
	'required':['country_code', 'party_id', 'uid', 'type', 'contract_id','issuer', 'valid', 'whitelist','last_updated'],
	'additionalProperties': False,
}

Token = warlock.model_factory(token_schema)
