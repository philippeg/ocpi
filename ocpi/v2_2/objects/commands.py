import warlock

from ocpi.v2_2.objects.common import display_text_schema, date_time_schema, token_type_schema
from ocpi.v2_2.objects.tokens import token_schema

# OCPI 2.2 13.4.1
command_response_type_schema = {'type':'string', 'enum':['NOT_SUPPORTED', 'REJECTED', 'ACCEPTED', 'UNKNOWN_SESSION']}

# OCPI 2.2 13.4.2
command_result_type_schema = {'type':'string', 'enum':['ACCEPTED', 'CANCELED_RESERVATION', 'EVSE_OCCUPIED', 'EVSE_INOPERATIVE', 'FAILED', 'NOT_SUPPORTED', 'REJECTED', 'TIMEOUT', 'UNKNOWN_RESERVATION']}

# OCPI 2.2 13.4.3
command_type_schema = {'type':'string', 'enum':['CANCEL_RESERVATION', 'RESERVE_NOW', 'START_SESSION', 'STOP_SESSION', 'UNLOCK_CONNEVCTOR']}



# OCPI 2.2 13.3.1
cancel_reservation_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'CancelReservation',
	'properties': {
		'response_url': {'type':'string', 'format':'hostname'},
		'reservation_id': {'type':'string', 'maxLength':36},
	},
	'required':['response_url', 'reservation_id'],
	'additionalProperties': False,
}

CancelReservation = warlock.model_factory(cancel_reservation_schema)

# OCPI 2.2 13.3.2
command_response_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'CommandResponse',
	'properties': {
		'result': command_response_type_schema,
		'timeout': {'type':'integer'},
		'message': display_text_schema
	},
	'required':['result', 'timeout'],
	'additionalProperties': False,
}

CommandResponse = warlock.model_factory(command_response_schema)


# OCPI 2.2 13.3.3
command_result_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'CommandResult',
	'properties': {
		'result': command_result_type_schema,
		'message': display_text_schema
	},
	'required':['result'],
	'additionalProperties': False,
}

CommandResult = warlock.model_factory(command_result_schema)


# OCPI 2.2 13.3.4
reserve_now_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'ReserveNow',
	'properties': {
		'response_url': {'type':'string', 'format':'hostname'},
		'token': token_schema,
		'expiry_date': date_time_schema,
		'reservation_id': {'type':'string', 'maxLength':36},
		'location_id': {'type':'string', 'maxLength':36},
		'evse_uid': {'type':'string', 'maxLength':36},
		'authorization_reference': {'type':'string', 'maxLength':36},
	},
	'required':['response_url', 'token', 'expiry_date','reservation_id', 'location_id'],
	'additionalProperties': False,
}

ReserveNow = warlock.model_factory(reserve_now_schema)

# OCPI 2.2 13.3.5
start_session_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'StartSession',
	'properties': {
		'response_url': {'type':'string', 'format':'hostname'},
		'token': token_schema,
		'location_id': {'type':'string', 'maxLength':36},
		'evse_uid': {'type':'string', 'maxLength':36},
		'authorization_reference': {'type':'string', 'maxLength':36},
	},
	'required':['response_url', 'token', 'location_id'],
	'additionalProperties': False,
}

StartSession = warlock.model_factory(start_session_schema)

# OCPI 2.2 13.3.6
stop_session_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'StopSession',
	'properties': {
		'response_url': {'type':'string', 'format':'hostname'},
		'session_id': {'type':'string', 'maxLength':36},
	},
	'required':['response_url', 'session_id'],
	'additionalProperties': False,
}

StopSession = warlock.model_factory(stop_session_schema)

# OCPI 2.2 13.3.7
unlock_connector_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'UnlockConnector',
	'properties': {
		'response_url': {'type':'string', 'format':'hostname'},
		'location_id': {'type':'string', 'maxLength':36},
		'evse_uid': {'type':'string', 'maxLength':36},
		'connector_id': {'type':'string', 'maxLength':36},
	},
	'required':['response_url', 'location_id', 'evse_uid', 'connector_id'],
	'additionalProperties': False,
}

UnlockConnector = warlock.model_factory(unlock_connector_schema)

# OCPI 2.2 15.5.1
connection_status_schema = {'type':'string', 'enum':['CONNECTED', 'OFFLINE', 'PLANNED', 'SUSPENDED']}