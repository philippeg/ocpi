import warlock
from ocpi.v2_2.objects.common import *

# OCPI 2.2 15.5.1
connection_status_schema = {'type':'string', 'enum':['CONNECTED', 'OFFLINE', 'PLANNED', 'SUSPENDED']}

# OCPI 2.2 15.4.1
clientinfo_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'ClientInfo',
	'properties': {
		'country_code': {'type':'string', 'maxLength':2},
		'party_id': {'type':'string', 'maxLength':3},
		'role': role_schema,
		'status': connection_status_schema,
		'last_updated': date_time_schema,
	},
	'required':['country_code', 'party_id', 'role', 'status','last_updated'],
	'additionalProperties': False,
}

ClientInfo = warlock.model_factory(clientinfo_schema)