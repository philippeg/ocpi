import warlock

from ocpi.v2_2.objects.common import *



# OCPI 2.2 8.4.21
status_schema = {'type':'string', 'enum':['AVAILABLE', 'BLOCKED', 'CHARGING', 'INOPERATIVE', 'OUTOFORDER', 'PLANNED', 'REMOVED','RESERVED','UNKNOWN']}

# OCPI 2.2 8.4.22
status_schedule_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'StatusSchedule',
	'properties': {
		'period_begin': date_time_schema,
		'period_end': date_time_schema,
		'status': status_schema,
	},
	'required':['period_begin', 'status'],
	'additionalProperties': False,
}

StatusSchedule = warlock.model_factory(status_schedule_schema)

# OCPI 2.2 8.4.21
capability_schema = {'type':'string', 'enum':['CHARGING_PROFILE_CAPABLE', 'CHARGING_PREFERENCES_CAPABLE', 'CREDIT_CARD_PAYABLE', 'DEBIT_CARD_PAYABLE', 'REMOTE_START_STOP_CAPABLE', 'RESERVABLE', 'RFID_READER','TOKEN_GROUP_CAPABLE','UNLOCK_CAPABLE']}


# OCPI 2.2 8.3.3
connector_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'Connector',
	'properties': {
		'id': {'type':'string', 'maxLength':36},
		'standard': connector_type_schema,
		'format': connector_format_schema,
		'power_type': power_type_schema,
		'max_voltage': {'type':'integer'},
		'max_amperage': {'type':'integer'},
		'max_electric_power': {'type':'integer'},
		'tariff_ids': {'type':'array', 'items': {'type':'string', 'maxLength':36} },
		'terms_and_conditions': {'type':'string', 'format':'hostname'},
		'last_updated': date_time_schema,
	},
	'required':['id', 'standard', 'format', 'power_type', 'max_voltage', 'max_amperage', 'last_updated'],
	'additionalProperties': False,
}

Connector = warlock.model_factory(connector_schema)

# OCPI 2.2 8.4.18
parking_restriction_schema = {'type':'string', 'enum':['EV_ONLY', 'PLUGGED', 'DISABLED', 'CUSTOMERS', 'MOTORCYCLES']}

# OCPI 2.2 8.3.2
evse_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'EVSE',
	'properties': {
		'uid': {'type':'string', 'maxLength':36},
		'evse_id': {'type':'string', 'maxLength':48},
		'status': status_schema,
		'status_schedule': {'type':'array', 'items': status_schedule_schema },
		'capabilities': {'type':'array', 'items': capability_schema },
		'connectors': {'type':'array', 'items': connector_schema, 'minItems':1 },
		'floor_level': {'type':'string', 'maxLength':4},
		'coordinates': geo_location_schema,
		'physical_reference': {'type':'string', 'maxLength':16},
		'directions': {'type':'array', 'items': display_text_schema },
		'parking_restrictions': {'type':'array', 'items': parking_restriction_schema },
		'images': {'type':'array', 'items': image_schema },
		'last_updated': date_time_schema,
	},
	'required':['uid', 'status', 'connectors', 'last_updated'],
	'additionalProperties': False,
}

EVSE = warlock.model_factory(evse_schema)


# OCPI 2.2 8.4.18
facility_schema = {'type':'string', 'enum':['HOTEL', 'RESTAURANT', 'CAFE', 'MALL', 'SUPERMARKET', 'SPORT', 'RECREATION_AREA','NATURE','MUSEUM','BIKE_SHARING','BUS_STOP','TAXI_STAND','TRAM_STOP','METRO_STATION','TRAIN_STATION','AIRPORT','PARKING_LOT','CARPOOL_PARKING','FUEL_STATION','WIFI']}

# OCPI 2.2 8.4.20
regular_hours_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'RegularHours',
	'properties': {
		'weekday': {'type':'integer'}, # TODO: custom validator to check that weekday is in [0-7]
		'period_begin': {'type':'string', 'maxLength':5},
		'period_end': {'type':'string', 'maxLength':5},
	},
	'required':['weekday', 'period_begin', 'period_end'],
	'additionalProperties': False,
}

RegularHours = warlock.model_factory(regular_hours_schema)

# OCPI 2.2 8.4.11
exceptional_period_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'ExceptionalPeriod',
	'properties': {
		'period_begin': date_time_schema,
		'period_end': date_time_schema,
	},
	'required':['period_begin', 'period_end'],
	'additionalProperties': False,
}

ExceptionalPeriod = warlock.model_factory(exceptional_period_schema)


# OCPI 2.2 8.4.14
hours_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'Hours',
	'properties': {
		'twentyfourseven': {'type':'boolean'},
		'period_begin': {'type':'string', 'maxLength':5},
		'regular_hours': {'type':'array', 'items': regular_hours_schema },
		'exceptional_openings': {'type':'array', 'items': exceptional_period_schema },
		'exceptional_closings': {'type':'array', 'items': exceptional_period_schema },
	},
	'required':['twentyfourseven'],
	'additionalProperties': False,
}

Hours = warlock.model_factory(hours_schema)

# OCPI 2.2 8.3.1
location_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'Location',
	'properties': {
		'country_code': {'type':'string', 'maxLength':2},
		'party_id': {'type':'string', 'maxLength':3},
		'id': {'type':'string', 'maxLength':36},
		'type': location_type_schema,
		'name': {'type':'string', 'maxLength':255},
		'address': {'type':'string', 'maxLength':45},
		'city': {'type':'string', 'maxLength':45},
		'postal_code': {'type':'string', 'maxLength':10},
		'state': {'type':'string', 'maxLength':20},
		'country': {'type':'string', 'maxLength':3},
		'coordinates': geo_location_schema,
		'related_locations': {'type':'array', 'items': additional_geo_location_schema },
		'evses': {'type':'array', 'items': evse_schema },
		'directions': {'type':'array', 'items': display_text_schema },
		'operator': business_details_schema,
		'suboperator': business_details_schema,
		'owner': business_details_schema,
		'facilities': {'type':'array', 'items': facility_schema },
		'time_zone': {'type':'string', 'maxLength':255},
		'opening_times': hours_schema,
		'charging_when_closed': {'type':'boolean'},
		'images': {'type':'array', 'items': image_schema },
		'energy_mix': energy_mix_schema,
		'last_updated': date_time_schema,
	},
	'required':['country_code', 'party_id', 'id', 'type', 'address', 'city', 'country', 'coordinates', 'last_updated'],
	'additionalProperties': False,
}

Location = warlock.model_factory(location_schema)
