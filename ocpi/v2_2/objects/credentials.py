import warlock

from ocpi.v2_2.objects.common import *

# OCPI 2.2 7.4.1
credentials_role_schema = {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'CredentialsRole',
	'properties': {
		'role': role_schema,
		'business_details': business_details_schema,
		'party_id': {'type':'string', 'maxLength': 3},
		'country_code': {'type':'string', 'maxLength': 2},
	},
	'required':['role', 'business_details', 'party_id', 'country_code'],
	'additionalProperties': False,
}

CredentialsRole = warlock.model_factory(credentials_role_schema)

# OCPI 2.2 7.3.1
credentials_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'Credentials',
	'properties': {
		'token': {'type':'string', 'maxLength': 64},
		'url': {'type':'string', 'format':'hostname'},
		'roles': {'type':'array', 'items': credentials_role_schema, 'minItems':1 },
	},
	'required':['token', 'url', 'roles'],
	'additionalProperties': False,
}

Credentials = warlock.model_factory(credentials_schema)