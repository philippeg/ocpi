import warlock

from ocpi.v2_2.objects.common import *
from ocpi.v2_2.objects.cdrs import cdr_token_schema, auth_method_schema, charging_period_schema

# OCPI 2.2 9.4.3
session_status_schema = {'type':'string', 'enum':['ACTIVE', 'COMPLETED', 'INVALID', 'PENDING']}


# OCPI 2.2 9.3.1
session_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'Session',
	'properties': {
		'country_code': {'type':'string', 'maxLength':2},
		'party_id': {'type':'string', 'maxLength':3},
		'id': {'type':'string', 'maxLength':36},
		'start_date_time': date_time_schema,
		'end_date_time': date_time_schema,
		'kwh': {'type': 'number'},
		'cdr_token': cdr_token_schema,
		'auth_method': auth_method_schema,
		'authorization_reference': {'type':'string', 'maxLength':36},
		'location_id': {'type':'string', 'maxLength':36},
		'evse_uid': {'type':'string', 'maxLength':36},
		'connector_id': {'type':'string', 'maxLength':36},
		'meter_id': {'type':'string', 'maxLength':255},
		'currency': {'type':'string', 'maxLength':3},
		'charging_periods': {'type':'array', 'items': charging_period_schema },
		'total_cost': price_schema,
		'status': session_status_schema,
		'last_updated': date_time_schema,
	},
	'required':['country_code', 'party_id', 'id', 'start_date_time', 'kwh', 'cdr_token', 'auth_method', 'location_id', 'evse_uid', 'connector_id', 'currency', 'status', 'last_updated'],
	'additionalProperties': False,
}

Session = warlock.model_factory(session_schema)
