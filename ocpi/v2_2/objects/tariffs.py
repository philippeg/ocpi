import warlock 
from ocpi.v2_2.objects.common import *


# OCPI 2.2 11.4.7
tariff_type_schema = {'type':'string', 'enum':['AD_HOC_PAYMENT', 'PROFILE_CHEAP', 'PROFILE_FAST', 'PROFILE_GREEN']}

# OCPI 2.2 11.4.7
tariff_dimension_type_schema = {'type':'string', 'enum':['ENERGY', 'FLAT', 'PARKING_TIME', 'TIME']}

# OCPI 2.2 11.4.4
price_component_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'PriceComponent',
	'properties': {
		'type': tariff_dimension_type_schema,
		'price': {'type': 'number'},
		'vat': {'type': 'number'},
		'step_size': {'type': 'integer'},
	},
	'required':['type', 'price', 'step_size'],
	'additionalProperties': False,
}

PriceComponent = warlock.model_factory(price_component_schema)

# OCPI 2.2 11.4.3
reservation_restriction_type_schema = {'type':'string', 'enum':['RESERVATION', 'RESERVATION_EXPIRES']}

# OCPI 2.2 11.4.1
day_of_week_schema = {'type':'string', 'enum':['MONDAY', 'TUESDAY', 'WEDNESDAY','THURSDAY','FRIDAY','SATURDAY','SUNDAY']}

# OCPI 2.2 11.4.6
tariff_restrictions_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'TariffRestrictions',
	'properties': {
		'start_time': {'type':'string', 'maxLength':5, 'pattern':"^[0-2][0-9]:[0-5][0-9]"}, 
		'end_time': {'type':'string', 'maxLength':5, 'pattern':"^[0-2][0-9]:[0-5][0-9]"},
		'start_date': {'type':'string', 'maxLength':10},
		'end_date': {'type':'string', 'maxLength':10},
		'min_kwh': {'type': 'number'},
		'max_kwh': {'type': 'number'},
		'min_current': {'type': 'number'},
		'max_current': {'type': 'number'},
		'min_power': {'type': 'number'},
		'max_power': {'type': 'number'},
		'min_duration': {'type': 'integer'},
		'max_duration': {'type': 'integer'},
		'day_of_week':  {'type':'array', 'items': day_of_week_schema, 'minItems':1  },
		'reservation': reservation_restriction_type_schema,
	},
	'additionalProperties': False,
}

TariffRestrictions = warlock.model_factory(tariff_restrictions_schema)


# OCPI 2.2 11.4.4
tariff_element_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'TariffElement',
	'properties': {
		'price_components': {'type':'array', 'items': price_component_schema, 'minItems':1  },
		'restrictions': tariff_restrictions_schema
	},
	'required':['price_components'],
	'additionalProperties': False,
}

TariffElement = warlock.model_factory(tariff_element_schema)

# OCPI 2.2 11.3.1
tariff_schema =  {
	"$schema":"http://json-schema.org/schema#",
	'type': 'object',
	'name': 'Tariff',
	'properties': {
		'country_code': {'type':'string', 'maxLength':2},
		'party_id': {'type':'string', 'maxLength':3},
		'id': {'type':'string', 'maxLength':36},
		'currency': {'type':'string', 'maxLength':3},
		'type': tariff_type_schema,
		'tariff_alt_text': display_text_schema,
		'tariff_alt_url': {'type':'string', 'format':'hostname'},
		'min_price': price_schema,
		'max_price': price_schema,
		'elements': {'type':'array', 'items': tariff_element_schema, 'minItems':1  },
		'start_date_time': date_time_schema,
		'end_date_time': date_time_schema,
		'energy_mix': energy_mix_schema,
		'last_updated': date_time_schema,
		
	},
	'required':['country_code', 'party_id', 'id', 'currency', 'elements', 'last_updated'],
	'additionalProperties': False,
}

Tariff = warlock.model_factory(tariff_schema)