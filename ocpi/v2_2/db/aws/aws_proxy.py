import boto3
import dateutil
from boto3.dynamodb.conditions import Key
from ocpi.v2_2.db.proxy import AbstractDbProxy
from ocpi import config
from ocpi.v2_2.core.queries import update_or_add, first_or_none, where

dynamodb = boto3.resource('dynamodb', 
						  aws_access_key_id= config.aws_access_key,
						  aws_secret_access_key= config.aws_secret_access_key,
						  region_name=config.aws_region_name)

client = boto3.client('dynamodb',
						aws_access_key_id= config.aws_access_key,
						aws_secret_access_key= config.aws_secret_access_key,
						region_name=config.aws_region_name)

class AWSProxy(AbstractDbProxy):
	

	def get_credentials_from_token_a(self, token_a):
		return self.get_item('credentials_a', {'token': token_a}, 'credentials')
	

	def get_credentials_from_token_c(self, token_c):
		return self.get_item('credentials_c', {'token': token_c}, 'credentials')


	def register_client(self, credentials, version, endpoints):
		# adds this client to the registration list
		table_c = dynamodb.Table('credentials_c')
		for role in credentials.roles:
			key = self.build_key(role.role, role.party_id, role.country_code)
			table_c.put_item(Item={
				'token':  credentials.token,
				'role_party_country': key,
				'credentials': credentials,
				'version': version,
				'endpoints': endpoints
			})
			# also invalidate temporary token
			try:
				table_a = dynamodb.Table('credentials_a')
				table_a.delete_item(Key={'role_party_country': key})
			except:
				pass


	def unregister_client(self, credentials):
		table = dynamodb.Table('credentials_c')
		for role in credentials.roles:
			key = self.build_key(role.role, role.party_id, role.country_code)
			table.delete_item(Key={'role_party_country': key})
	
	def is_registered(self, credentials):
		for role in credentials.roles:
			key = self.build_key(role.role, role.party_id, role.country_code)
			client = self.get_item('credentials_c', {'role_party_country': key}, 'credentials')
			if client:
				return True
		return False

	# Returns a boolean flag indicating whether the given date is between the given {date_from} (including) and {date_to} (excluding).
	def is_date_between(self, date, date_from, date_to):
		try:
			_date = dateutil.parser.parse(date)
			_from = dateutil.parser.parse(date_from) if date_from else None
			_to = dateutil.parser.parse(date_to)  if date_to else None
			return (not _from or _date >= _from) and (not _to or _date < _to)
		except:
			return False

	#TODO Implement tenant so that tokens can be added or not to a given CPO
	# only Tokens with (last_updated) between the given {date_from} (including) and {date_to} (excluding) will be returned.
	def get_tokens(self, date_from, date_to):
		try:
			table = dynamodb.Table('tokens')
			response = table.scan()
			all_tokens = [item['token'] for item in response['Items']]
			queried_tokens = where(lambda t: self.is_date_between(t['last_updated'], date_from, date_to), all_tokens)
			return queried_tokens
		except:
			return []

	def build_key(self, *args, separator = '_'):
		lower_case_args = [arg.lower() for arg in args]
		return separator.join(lower_case_args)

	def add_or_update_item(self, tablename, key_dict, update_field_name, item):
		table = dynamodb.Table(tablename)
		table.update_item(
			Key=key_dict, 
			UpdateExpressions='SET {} = :val1'.format(update_field_name),
			ExpressionAttributeValues={':val1': item})


	def get_item(self, tablename, key_dict, get_field_name):
		try:
			table = dynamodb.Table(tablename)
			response = table.get_item(Key=key_dict)
			item = response['Item']
			return item[get_field_name]
		except:
			return None
		return None


	def get_location(self, country_code, party_id, location_id):
		key = self.build_key(country_code, party_id, location_id)
		return self.get_item('locations', {'country_party_id': key}, 'location')
		
	def get_evse(self, country_code, party_id, location_id, evse_uid):
		location = self.get_location(country_code, party_id, location_id)
		uid = evse_uid.lower()
		if location:
			return first_or_none(lambda evse: evse['uid'].lower() == uid, location['evses'])
		else:
			return None

	def get_connector(self, country_code, party_id, location_id, evse_uid, connector_id):
		evse = self.get_evse(country_code, party_id, location_id, evse_uid)
		id = connector_id.lower()
		if evse:
			return first_or_none(lambda connector: connector['id'].lower() == id, evse['connectors'])
		else:
			return None


	def add_or_update_location(self, country_code, party_id, location_id, location):
		key = self.build_key(country_code, party_id, location_id)
		self.add_or_update_item('locations', {'country_party_id': key}, 'location', location)
		

	def get_session(self, country_code, party_id, session_id):
		key = self.build_key(country_code, party_id, session_id)
		return self.get_item('sessions', {'country_party_id': key}, 'session')


	def add_or_update_session(self, country_code, party_id, session_id, session):
		key = self.build_key(country_code, party_id, session_id)
		self.add_or_update_item('sessions', {'country_party_id': key}, 'session', session)


	def get_cdr(self, country_code, party_id, cdr_id):
		key = self.build_key(country_code, party_id, cdr_id)
		return self.get_item('cdrs', {'country_party_id': key}, 'cdr')

	def add_cdr(self, country_code, party_id, cdr):
		key = self.build_key(country_code, party_id, cdr.id)
		table = dynamodb.Table('cdrs')
		table.put_item(Item={'country_party_id': key, 'cdr': cdr})

	
	def get_tariff(self, country_code, party_id, tariff_id):
		key = self.build_key(country_code, party_id, tariff_id)
		return self.get_item('tariffs', {'country_party_id': key}, 'tariff')

	def add_or_update_tariff(self, country_code, party_id, tariff_id, tariff):
		key = self.build_key(country_code, party_id, tariff_id)
		self.add_or_update_item('tariffs', {'country_party_id': key}, 'tariff', tariff)


	def delete_tariff(self, country_code, party_id, tariff_id): # returns a bool indicating whether the delete operation was successfull
		try:
			key = self.build_key(country_code, party_id, tariff_id)
			table = dynamodb.Table('tariffs')
			table.delete_item(Key={'country_party_id': key})
		except:
			return False
		return True

	def get_clientinfo(self, country_code, party_id):
		key = self.build_key(country_code, party_id)
		return self.get_item('clientinfos', {'country_party_id': key}, 'clientinfo')

	def add_or_update_clientinfo(self, country_code, party_id, clientinfo):
		key = self.build_key(country_code, party_id)
		self.add_or_update_item('clientinfos', {'country_party_id': key}, 'clientinfo', clientinfo)


	# TODO implement db trigger to get notifications on command status changes
	# returns a boolean flag indicating whether the update process was successfull or not
	def update_command_result(self, command_type, command_id, command_result):
		try:
			self.add_or_update_item('commands', {'command_type': command_type, 'command_id': command_id}, 'command_result', command_result)
		except:
			return False
		return True


