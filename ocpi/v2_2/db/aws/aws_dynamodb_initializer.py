import boto3
from ocpi import config
from ocpi.v2_2.db.aws.aws_proxy import AWSProxy


class AwsDynamoDbInitializer():

    dynamodb = boto3.resource('dynamodb', 
	    					  aws_access_key_id= config.aws_access_key,
		    				  aws_secret_access_key= config.aws_secret_access_key,
			    			  region_name=config.aws_region_name)


    def create_all_table(self):
        self.create_cdrs_table()
        self.create_clientinfo_table()
        self.create_commands_table()
        self.create_credentials_a_table()
        self.create_credentials_c_table()
        self.create_locations_table()
        self.create_sessions_table()
        self.create_tariffs_table()
        self.create_tokens_table()


    def create_locations_table(self):
        locations_table = self.dynamodb.create_table(
            TableName='locations',
            KeySchema=[
                {
                    'AttributeName': 'country_party_id', 
                    'KeyType': 'HASH'
                },
            ], 
            AttributeDefinitions=[
                {
                    'AttributeName': 'country_party_id', 
                    'AttributeType': 'S'
                }, 
                {
                    'AttributeName': 'location', 
                    'AttributeType': 'S'
                }, 
            ], 
            ProvisionedThroughput={
                'ReadCapacityUnits': 1, 
                'WriteCapacityUnits': 1
            }
        )
        locations_table.meta.client.get_waiter('table_exists').wait(TableName='locations')

 

    def create_sessions_table(self):
        sessions_table = self.dynamodb.create_table(
            TableName='sessions',
            KeySchema=[
                {
                    'AttributeName': 'country_party_id', 
                    'KeyType': 'HASH'
                },
            ], 
            AttributeDefinitions=[
                {
                    'AttributeName': 'country_party_id', 
                    'AttributeType': 'S'
                }, 
                {
                    'AttributeName': 'session', 
                    'AttributeType': 'S'
                }, 
            ], 
            ProvisionedThroughput={
                'ReadCapacityUnits': 1, 
                'WriteCapacityUnits': 1
            }
        )

        sessions_table.meta.client.get_waiter('table_exists').wait(TableName='sessions')

    def create_cdrs_table(self):
        cdrs_table = self.dynamodb.create_table(
            TableName='cdrs',
            KeySchema=[
                {
                    'AttributeName': 'country_party_id', 
                    'KeyType': 'HASH'
                },
            ], 
            AttributeDefinitions=[
                {
                    'AttributeName': 'country_party_id', 
                    'AttributeType': 'S'
                }, 
                {
                    'AttributeName': 'cdr', 
                    'AttributeType': 'S'
                }, 
            ], 
            ProvisionedThroughput={
                'ReadCapacityUnits': 1, 
                'WriteCapacityUnits': 1
            }
        )

        cdrs_table.meta.client.get_waiter('table_exists').wait(TableName='cdrs')

    def create_tariffs_table(self):
        tariffs_table = self.dynamodb.create_table(
            TableName='tariffs',
            KeySchema=[
                {
                    'AttributeName': 'country_party_id', 
                    'KeyType': 'HASH'
                },
            ], 
            AttributeDefinitions=[
                {
                    'AttributeName': 'country_party_id', 
                    'AttributeType': 'S'
                }, 
                {
                    'AttributeName': 'tariff', 
                    'AttributeType': 'S'
                }, 
            ], 
            ProvisionedThroughput={
                'ReadCapacityUnits': 1, 
                'WriteCapacityUnits': 1
            }
        )

        tariffs_table.meta.client.get_waiter('table_exists').wait(TableName='tariffs')

    def create_clientinfo_table(self):
        clientinfo_table = self.dynamodb.create_table(
            TableName='clientinfos',
            KeySchema=[
                {
                    'AttributeName': 'country_party_id', 
                    'KeyType': 'HASH'
                },
            ], 
            AttributeDefinitions=[
                {
                    'AttributeName': 'country_party_id', 
                    'AttributeType': 'S'
                }, 
                {
                    'AttributeName': 'clientinfo', 
                    'AttributeType': 'S'
                }, 
            ], 
            ProvisionedThroughput={
                'ReadCapacityUnits': 1, 
                'WriteCapacityUnits': 1
            }
        )

        clientinfo_table.meta.client.get_waiter('table_exists').wait(TableName='clientinfos')

    def create_commands_table(self):
        commands_table = self.dynamodb.create_table(
            TableName='commands',
            KeySchema=[
                {
                    'AttributeName': 'command_type', 
                    'KeyType': 'HASH'
                },
                {
                    'AttributeName': 'command_id', 
                    'KeyType': 'HASH'
                },
            ], 
            AttributeDefinitions=[
                {
                    'AttributeName': 'command_type', 
                    'AttributeType': 'S'
                },
                {
                    'AttributeName': 'command_id', 
                    'AttributeType': 'S'
                }, 
                {
                    'AttributeName': 'command_result', 
                    'AttributeType': 'S'
                }, 
            ], 
            ProvisionedThroughput={
                'ReadCapacityUnits': 1, 
                'WriteCapacityUnits': 1
            }
        )

        commands_table.meta.client.get_waiter('table_exists').wait(TableName='commands')

    def create_tokens_table(self):
        tokens_table = self.dynamodb.create_table(
            TableName='tokens',
            KeySchema=[
                {
                    'AttributeName': 'country_party_uid', 
                    'KeyType': 'HASH'
                },
            ], 
            AttributeDefinitions=[
                {
                    'AttributeName': 'country_party_uid', 
                    'AttributeType': 'S'
                },
                {
                    'AttributeName': 'token', 
                    'AttributeType': 'S'
                },

            ], 
            ProvisionedThroughput={
                'ReadCapacityUnits': 1, 
                'WriteCapacityUnits': 1
            }
        )
        tokens_table.meta.client.get_waiter('table_exists').wait(TableName='tokens')


    def create_credentials_a_table(self):
        credentials_a_table = self.dynamodb.create_table(
            TableName='credentials_a',
            KeySchema=[
                {
                    'AttributeName': 'token', 
                    'KeyType': 'HASH'
                },
                {
                    'AttributeName': 'role_party_country', 
                    'KeyType': 'HASH'
                }
            ], 
            AttributeDefinitions=[
                {
                    'AttributeName': 'token', 
                    'AttributeType': 'S'
                },
                {
                    'AttributeName': 'role_party_country', 
                    'AttributeType': 'S'
                },
                {
                    'AttributeName': 'credentials', 
                    'AttributeType': 'S'
                }, 
            ], 
            ProvisionedThroughput={
                'ReadCapacityUnits': 1, 
                'WriteCapacityUnits': 1
            }
        )
        credentials_a_table.meta.client.get_waiter('table_exists').wait(TableName='credentials_a')
        #print(credentials_a_table.item_count)

    def create_credentials_c_table(self):
        credentials_c_table = self.dynamodb.create_table(
            TableName='credentials_c',
            KeySchema=[
                {
                    'AttributeName': 'token', 
                    'KeyType': 'HASH'
                },
                {
                    'AttributeName': 'role_party_country', 
                    'KeyType': 'HASH'
                }
            ], 
            AttributeDefinitions=[
                {
                    'AttributeName': 'token', 
                    'AttributeType': 'S'
                },
                {
                    'AttributeName': 'role_party_country', 
                    'AttributeType': 'S'
                },
                {
                    'AttributeName': 'credentials', 
                    'AttributeType': 'S'
                },
                {
                    'AttributeName': 'version', 
                    'AttributeType': 'S'
                },
                {
                    'AttributeName': 'endpoints', 
                    'AttributeType': 'S'
                }  
            ], 
            ProvisionedThroughput={
                'ReadCapacityUnits': 1, 
                'WriteCapacityUnits': 1
            }
        )
        credentials_c_table.meta.client.get_waiter('table_exists').wait(TableName='credentials_c')
        #print(credentials_c_table.item_count)

    def init_all_table(self):
        self.init_cdrs_table()
        self.init_clientinfo_table()
        self.init_commands_table()
        self.init_credentials_a_table()
        self.init_credentials_c_table()
        self.init_locations_table()
        self.init_sessions_table()
        self.init_tariffs_table()
        self.init_tokens_table()

    def add_item_to_table(self, tablename, item):
        table = self.dynamodb.Table(tablename)
        table.put_item(Item=item)

    def init_locations_table(self):
        location = {
			"country_code": "BE",
			"party_id": "BEK",
			"id": "LOC1",
			"type": 'ON_STREET',
			"name": "Gent Zuid",
			"address": "F.Rooseveltlaan 3A",
			"city": "Gent",
			"postal_code": "9000",
			"state": "CA",
			"country": "BEL",
			"coordinates": {
				"latitude": "3.729944",
				"longitude": "51.047599"
			},
			"related_locations": [{
				"latitude": "3.729944",
				"longitude": "51.047599",
				"name": {
					"language": "fr",
					"text": "salut les amis!"
					}
				}],
			"evses": [{
				"uid": "3256",
				"evse_id": "BE*BEC*E041503001",
				"status": "AVAILABLE",
				"status_schedule": [],
				"capabilities": ["RESERVABLE"],
				"connectors": [{
					"id": "1",
					"standard": "IEC_62196_T2",
					"format": "CABLE",
					"power_type": "AC_3_PHASE",
					"max_voltage": 220,
					"max_amperage": 16,
					"tariff_ids": ["11"],
					"last_updated": "2015-03-16T10:10:02Z"
					}, {
					"id": "2",
					"standard": "IEC_62196_T2",
					"format": "SOCKET",
					"power_type": "AC_3_PHASE",
					"max_voltage": 220,
					"max_amperage": 16,
					"tariff_ids": ["13"],
					"last_updated": "2015-03-18T08:12:01Z"
					}],
				"physical_reference": "1",
				"floor_level": "-1",
				"last_updated": "2015-06-28T08:12:01Z"
				}],
			"directions": [{
				"language": "en",
				"text": "Right Left Right"
			}],
			"operator": {'name': 'me'},
			"suboperator": {'name': 'you', 'website': 'you.com'},
			"owner": {'name': 'he'},
			"facilities": ['RESTAURANT'],
			"time_zone": "Europe/Oslo",
			"opening_times": {
				"twentyfourseven": True,
				"exceptional_closings": [{
				"period_begin": "2018-12-25T03:00:00Z",
				"period_end": "2018-12-25T05:00:00Z"
				}],
				"exceptional_openings": [{
				"period_begin": "2018-12-25T03:00:00Z",
				"period_end": "2018-12-25T05:00:00Z"
				}]
				},
			"charging_when_closed": False,
			"images": [{'url': 'ekaros.io', 'category': 'CHARGER', 'type': 'jpeg'}],
			'energy_mix': {
				"is_green_energy": False,
				"energy_sources": [
					{ "source": "GENERAL_GREEN", "percentage": 35.9 },
					{ "source": "GAS", "percentage": 6.3 },
					{ "source": "COAL", "percentage": 33.2 },
					{ "source": "GENERAL_FOSSIL", "percentage": 2.9 },
					{ "source": "NUCLEAR", "percentage": 21.7 }
					],
				"environ_impact": [
					{ "category": "NUCLEAR_WASTE", "amount": 0.0006 },
					{ "category": "CARBON_DIOXIDE", "amount": 372 }
					],
				"supplier_name": "E.ON Energy Deutschland",
				"energy_product_name": "E.ON DirektStrom eco"
				},
			"last_updated": "2015-03-16T10:10:02Z"
			}
        proxy = AWSProxy()
        proxy.add_or_update_location("BE", "BEK", "LOC1", location)

    def init_sessions_table(self):
        session ={
			"country_code": "BE",
			"party_id": "BEK",
			"id": "101",
			'start_date_time': "2018-12-25T05:00:00Z",
			'end_date_time': "2018-12-25T05:12:32Z",
			'kwh': 150.0,
			'cdr_token': {"uid": "TOKEN_ID", "type": "APP_USER", "contract_id": "CONTRACT_ID"},
			'auth_method': 'AUTH_REQUEST',
			'authorization_reference': 'an authorization ref',
			'location_id': 'LOC1',
			'evse_uid': "3256",
			'connector_id': "1",
			'meter_id': "meter1",
			'currency': "EUR",
			'charging_periods': [{"start_date_time": "2018-12-25T05:00:00Z",'dimensions': [{'type': 'ENERGY', 'volume':34.5}],'tariff_id': 'tariff_id'}],
			'total_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'status': 'COMPLETED',
			'last_updated': "2015-03-16T10:10:02Z",
			}
        proxy = AWSProxy()
        proxy.add_or_update_session("BE", "BEK", "101", session)

    def init_clientinfo_table(self):
        clientinfo ={
			'country_code': "BE",
			'party_id': "BEK",
			'role': "EMSP",
			'status': 'CONNECTED',
			'last_updated': "2017-03-16T10:10:02Z",
			}
        proxy = AWSProxy()
        proxy.add_or_update_clientinfo("BE", "BEK", clientinfo)

    def init_cdrs_table(self):
        cdr ={
			'country_code': "BE",
			'party_id': "BEK",
			'id': "T01",
			'start_date_time': "2015-03-16T10:10:02Z",
			'end_date_time': "2017-03-16T10:10:02Z",
			'session_id':"S01",
			'cdr_token': {"uid": "1234abcd","type": "OTHER","contract_id": "C101"},
			'auth_method': 'AUTH_REQUEST',
			'authorization_reference': "blah",
			'cdr_location': {"id": "cdr_loc1","name": "Gent Zuid","address": "F.Rooseveltlaan 3A","city": "Gent","postal_code": "9000","country": "BEL","coordinates": {"latitude": "3.729944","longitude": "51.047599"},"evse_uid": "1","evse_id": "e1","connector_id": "1","connector_standard": "CHADEMO","connector_format": "SOCKET","connector_power_type": "DC"},
			'meter_id': "M01",
			'currency': "EUR",
			'tariffs': [{'country_code': "BE",'party_id': "BEC",'id': "T01",'currency': "EUR",'type': "PROFILE_CHEAP",'tariff_alt_text': {"language": "en","text": "Standard Tariff"},'tariff_alt_url': 'hostname.be','min_price': {"excl_vat": 98.51, "incl_vat": 102.54},'max_price': {"excl_vat": 198.51, "incl_vat": 502.54},'elements': [{"price_components": [{"type": "ENERGY","price": 1.1,"vat": 0.1,"step_size": 2}],"restrictions": {'start_time': "12:30",'end_time': "12:48",'start_date': "2015-12-27",'end_date': "2015-12-24",'min_kwh': 20,'max_kwh': 50,'min_current': 5,'max_current': 20,'min_power': 5,'max_power': 20,'min_duration': 60,'max_duration': 3600,'day_of_week':  ['MONDAY', 'WEDNESDAY'],'reservation': 'RESERVATION'}}],'start_date_time': "2015-03-16T10:10:02Z",'end_date_time': "2017-03-16T10:10:02Z",'energy_mix': {"is_green_energy": False,"energy_sources": [{ "source": "GENERAL_GREEN", "percentage": 35.9 },{ "source": "GAS", "percentage": 6.3 }],"environ_impact": [{ "category": "NUCLEAR_WASTE", "amount": 0.0006 },],"supplier_name": "E.ON Energy Deutschland","energy_product_name": "E.ON DirektStrom eco"},'last_updated': "2017-03-16T10:10:02Z"}],
			'charging_periods': [{"start_date_time": "2018-12-25T05:00:00Z",'dimensions': [{'type': 'ENERGY', 'volume':34.5}],'tariff_id': 'tariff_id'}], 
			'signed_data': {"encoding_method": "GP","encoding_method_version": 2,"public_key": "12345","signed_values": [{"nature": "blah","plain_data": "blah","signed_data": "xxxx"}],"url": "here.com"},
			'total_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'total_fixed_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'total_energy': 10.5,
			'total_energy_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'total_time': 660,
			'total_time_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'total_parking_time': 58,
			'total_parking_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'total_reservation_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'remark': "nothing in particular",
			'credit': True,
			'credit_reference_id': "CB1",
			'last_updated': "2017-03-16T10:10:02Z"
			}
        proxy = AWSProxy()
        proxy.add_cdr("BE", "BEK", cdr)

    def init_commands_table(self):
        command ={
			'command_type': "RESERVATION",
			'command_id': 'C01',
            'command_result': "ACCEPTED",
			}
        self.add_item_to_table('commands', command)

    def init_tokens_table(self):
        token ={
			'country_code': "BE",
			'party_id': "BEK",
			'uid': "token1",
			'type': 'RFID',
			'contract_id': 'contract1',
			'visual_number': '1234567890',
			'issuer': 'me',
			'group_id': '123abc',
			'valid': True,
			'whitelist': 'ALWAYS',
			'language': 'en',
			'default_profile_type': 'FAST',
			'energy_contract': {"supplier_name": "you","contract_id": "123abc"},
			'last_updated': "2017-03-16T10:10:02Z",
			}
        self.add_item_to_table('tokens', {'country_party_uid': 'be_bek_token1', 'token': token})


    def init_tariffs_table(self):
        tariff ={
			'country_code': "BE",
			'party_id': "BEK",
			'id': "T01",
			'currency': "EUR",
			'type': "PROFILE_CHEAP",
			'tariff_alt_text': {"language": "en","text": "Standard Tariff"},
			'tariff_alt_url': 'hostname.be',
			'min_price': {"excl_vat": 98.51, "incl_vat": 102.54},
			'max_price': {"excl_vat": 198.51, "incl_vat": 502.54},
			'elements': [{"price_components": [{"type": "ENERGY","price": 1.1,"vat": 0.1,"step_size": 2}],"restrictions": {'start_time': "12:30",'end_time': "12:48",'start_date': "2015-12-27",'end_date': "2015-12-24",'min_kwh': 20,'max_kwh': 50,'min_current': 5,'max_current': 20,'min_power': 5,'max_power': 20,'min_duration': 60,'max_duration': 3600,'day_of_week': ['MONDAY', 'WEDNESDAY'],'reservation': 'RESERVATION'}}],
			'start_date_time': "2015-03-16T10:10:02Z",
			'end_date_time': "2017-03-16T10:10:02Z",
			'energy_mix': {"is_green_energy": False,"energy_sources": [{ "source": "GENERAL_GREEN", "percentage": 35.9 },{ "source": "GAS", "percentage": 6.3 }],"environ_impact": [{ "category": "NUCLEAR_WASTE", "amount": 0.0006 },],"supplier_name": "E.ON Energy Deutschland","energy_product_name": "E.ON DirektStrom eco"},
			'last_updated': "2017-03-16T10:10:02Z",
			}
        proxy = AWSProxy()
        proxy.add_or_update_tariff("BE", "BEK","T01", tariff)


    def init_credentials_a_table(self):
        credentials ={
			'token': "1234567890abcdefgh2019",
			'role_party_country': 'emp_eka_de',
            'credentials': {'token': '1234567890abcdefgh2019', 'url': 'ekaros.io', 'roles':[{'role': 'EMP', 'business_details': {'name': 'EKAROS mobility GmbH'}, 'party_id': 'EKA', 'country_code': 'DE'}]} ,
			}
        self.add_item_to_table('credentials_a', credentials)


    def init_credentials_c_table(self):
        credentials ={
			'token': "testcredentials2019",
			'role_party_country': 'emp_eka_de',
            'credentials': {'token': 'testcredentials2019', 'url': 'ekaros.io', 'roles':[{'role': 'EMP', 'business_details': {'name': 'EKAROS mobility GmbH'}, 'party_id': 'EKA', 'country_code': 'DE'}]} ,
			}
        self.add_item_to_table('credentials_c', credentials)


