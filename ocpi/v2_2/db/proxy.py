from abc import ABC, abstractmethod

class AbstractDbProxy(ABC):

	@abstractmethod
	def get_location(self, country_code, party_id, location_id):
		return None

	@abstractmethod
	def get_evse(self, country_code, party_id, location_id, evse_uid):
		return None

	@abstractmethod
	def get_connector(self, country_code, party_id, location_id, evse_uid, connector_id):
		return None

	@abstractmethod
	def add_or_update_location(self, country_code, party_id, location_id, location):
		return None

	@abstractmethod
	def get_session(self, country_code, party_id, session_id):
		return None

	@abstractmethod
	def add_or_update_session(self, country_code, party_id, session_id, session):
		return None

	@abstractmethod
	def get_cdr(self, country_code, party_id, cdr_id):
		return None

	@abstractmethod
	def add_cdr(self, country_code, party_id, cdr):
		return None

	@abstractmethod
	def get_tariff(self, country_code, party_id, tariff_id):
		return None

	@abstractmethod
	def add_or_update_tariff(self, country_code, party_id, tariff_id, tariff):
		return None

	@abstractmethod
	def delete_tariff(self, country_code, party_id, tariff_id): # returns a bool indicating whether the delete operation was successfull
		return None

	@abstractmethod
	def get_tokens(self, date_from, date_to): # date_from is inclusive, date_to exclusive
		return None

	@abstractmethod
	# returns a boolean flag indicating whether the update process was successfull or not
	def update_command_result(self, command_type, command_id, command_result):
		return None

	@abstractmethod
	def get_clientinfo(self, country_code, party_id):
		return None

	@abstractmethod
	def add_or_update_clientinfo(self, country_code, party_id, clientinfo):
		return None

	@abstractmethod
	def get_credentials_from_token_a(self, token_a):
		pass

	@abstractmethod
	def get_credentials_from_token_c(self, token_c):
		pass

	@abstractmethod
	def register_client(self, credentials, version, endpoints):
		# adds this client to the registration list
		# also invalidate temporary token  
		pass

	@abstractmethod
	def unregister_client(self, credentials):
		# invalidate client's credentials
		pass

	@abstractmethod
	def is_registered(self, credentials):
		# returns whether the given client belongs to the registration list
		pass