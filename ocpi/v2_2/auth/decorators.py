from functools import wraps
from flask import request, g
from ..core.responses import HttpErrorResponse, UnknownLocationResponseJson
from ..core.queries import first_or_none
from .services import AuthorizationService

# 4.1.2 Authorization header
# Every OCPI HTTP request MUST add a 'Authorization' header. The header looks as follows:
# Authorization: Token IpbJOXxkxOAuKR92z0nEcmVF3Qw09VG7I7d/WCg0koM=
# The literal 'Token' indicates that the token based authentication mechanism is used, 
# in OCPI this is called the 'credentials token'. 'Credentials tokens' are exchanged via the credentials module.

# Its parameter is a string consisting of printable, non-whitespace ASCII characters.
# The credentials token must uniquely identify the requesting party.
# This way, the server can use the information in the Authorization header to link the request to the correct requesting party’s account.

# If the header is missing or the credentials token doesn’t match any known party
# then the server SHALL respond with a HTTP 401 - Unauthorized status code.


def require_credentials_token(allow_token_a: bool = False):
	def decorator(func):
		@wraps(func)
		def wrapper(*args, **kwargs):
			auth_token = request.headers.get('Authorization')
			if auth_token:
				try:
					authService = AuthorizationService()
					credentials = authService.resolve_credentials_from_token_c(auth_token)
					if not credentials and allow_token_a:
						credentials = authService.resolve_credentials_from_token_a(auth_token)
					if not credentials:
						return HttpErrorResponse("invalid credentials", 401).make_response()
				except Exception:
					#traceback.print_exc()
					return HttpErrorResponse("unable to validate credentials", 401).make_response()
			else:
				return HttpErrorResponse("credentials token is missing", 401).make_response()
			g.credentials = credentials
			return func(*args, **kwargs)
		return wrapper
	return decorator


def require_role_authorization():
	def decorator(func):
		@wraps(func)
		def wrapper(*args, **kwargs):
			return __check_role_authorization(func, *args, **kwargs)
		return wrapper
	return decorator

def __check_role_authorization(func, *args, **kwargs):
	if not hasattr(g, 'credentials'):
		return HttpErrorResponse("no available credentials", 401).make_response()

	credentials = g.credentials
	if not credentials:
		return HttpErrorResponse("invalid credentials", 401).make_response()
	
	country_code = kwargs['country_code'] if 'country_code' in kwargs else None 
	party_id = kwargs['party_id'] if 'party_id' in kwargs else None 

	role = first_or_none(lambda r: r.country_code == country_code and  r.party_id == party_id, credentials.roles)	
	if not role:
		return HttpErrorResponse('Invalid Location or unauthorized access', 403).make_response()
	return func(*args, **kwargs)

