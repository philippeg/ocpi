import sys
import uuid
import flask

from ocpi.v2_2.db.controller import DBController

class TokenManager():
	__db_controller = None
	def __init__(self):
		__db_controller = DBController.controller()

	def resolve_token_a(self, token):
		return self.__db_controller.get_credentials_from_token_a(token)

	def resolve_token_c(self, token):
		return self.__db_controller.get_credentials_from_token_c(token)


class AuthorizationService():
	token_manager = None
	def __init__(self, token_manager: TokenManager = TokenManager()):
		self.token_manager = token_manager

	@staticmethod
	def make_header(token):
		return {'Authorization': 'Token {}'.format(token)}

	@staticmethod
	def generate_token():
		return str(uuid.uuid4())

	def resolve_credentials_from_token_a(self, auth_token: str):
		token = self.parse_authorization_header(auth_token)
		if token:
			return self.token_manager.resolve_token_a(token)

	def resolve_credentials_from_token_c(self, auth_token: str):
		token = self.parse_authorization_header(auth_token)
		if token:
			return self.token_manager.resolve_token_c(token)


	def parse_authorization_header(self, value):
		"""Parse an HTTP Token authorization header transmitted by the web
		browser. The return value is either `None` if the header was invalid or
		not given, otherwise the token string .
		:param value: the authorization header to parse.
		:return: a string representing the token value or `None`.
		"""
		if not value:
			return
		#value = wsgi_to_bytes(value)
		try:
			auth_type, auth_info = value.split(None, 1)
			auth_type = auth_type.lower()
		except ValueError:
			return
		# TODO validate new client token (no whitespaces, ...)?
		if auth_type == "token":
			try:
				token = auth_info #  base64.b64decode(auth_info)?
			except Exception:
				return
			return token # to_unicode(token, "utf-8")?



