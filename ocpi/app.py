from flask import Flask, jsonify

from . import ocpi
from .v2_2.core.responses import HttpErrorResponse

# error handlers
# 5. If the requested resource does NOT exist, the server SHOULD return a HTTP 404 - Not Found.
def page_not_found(error):
	return HttpErrorResponse(error.description, 404).make_response()

def custom400(error):
	return HttpErrorResponse(error.description, 400).make_response()

# TODO HTPPS!
# TODO Catch all exceptions?

class OCPI(object):
	def __init__(self, app):
		app.register_blueprint(ocpi, url_prefix='/ocpi')

		app.url_map.strict_slashes = False

		app.register_error_handler(404, page_not_found)
		app.register_error_handler(400, custom400)

