import unittest
import unittest.mock as mock

from ocpi_tests.v2_2.test_base import TestBase, without_authentication

class VersionInformationTests(TestBase):

	def test_get_with_token_a_returns_200(self):
		self.assertAuthorizationWithTokenA(200, '/ocpi/versions', self.GET())

	def test_get_with_token_c_returns_200(self):
		self.assertAuthorizationWithTokenC(200, '/ocpi/versions', self.GET())

	def test_get_without_token_returns_401(self):
		self.assertAuthorizationWithoutToken(401, '/ocpi/versions', self.GET())

	def test_post_not_allowed(self):
		self.assertHttpCode(405, '/ocpi/versions', self.POST())

	def test_put_not_allowed(self):
		self.assertHttpCode(405, '/ocpi/versions', self.PUT())

	def test_delete_not_allowed(self):
		self.assertHttpCode(405, '/ocpi/versions', self.DELETE())

	@without_authentication
	def test_get_returns_valid_response_format(self):
		response = self.performRequest('/ocpi/versions', self.GET())
		self.assertValidOcpiResponseFormat(response, 1000, "Success")

	@without_authentication
	def test_get_returns_json_response_data(self):
		response = self.performRequest('/ocpi/versions', self.GET())
		self.assertJsonResponseDataIsNotNone(response)

	@without_authentication
	def test_get_returns_version_v2_2(self):
		response = self.performRequest('/ocpi/versions', self.GET())
		json_data = response.get_json()['data']
		version_v2_2 = list(filter(lambda v: v['version'] == '2.2', json_data))
		self.assertEqual(1, len(version_v2_2))

	@without_authentication
	def test_get_returns_single_version(self):
		response = self.performRequest('/ocpi/versions', self.GET())
		json_data = response.get_json()['data']
		self.assertEqual(1, len(json_data))

	@without_authentication
	def test_get_returns_valid_urls_for_version(self):
		response = self.performRequest('/ocpi/versions', self.GET())
		json_data = response.get_json()['data']
		for version in json_data:
			url_response = self.performRequest(version['url'], self.GET())
			self.assertEqual(url_response.status_code, 200)
			# check version number consistency
			self.assertEqual(version['version'], url_response.get_json()['data']['version'])

if __name__ == '__main__':
	unittest.main()
