import unittest
import unittest.mock as mock

import requests

from ocpi_tests.v2_2.test_base import TestBase

from ocpi.v2_2.core.exceptions import RequestException
from ocpi.v2_2.core.requests import client_get, client_post, client_put
from ocpi.v2_2.objects.versions import Versions, Version

def throw_timeout_exception(url, headers):
	raise requests.Timeout()

def throw_request_exception(url, headers):
	raise RequestException("exception test")

class RequestsTests(TestBase):
	@mock.patch("ocpi.v2_2.core.requests.requests.get", throw_timeout_exception)
	def test_client_get_timeout_throws_expected_exception(self):
		with self.assertRaises(RequestException):
			client_get('http://timeout_test.ut', object())

	@mock.patch("ocpi.v2_2.core.requests.requests.get", throw_request_exception)
	def test_client_get_requests_exeption_throws_expected_exception(self):
		with self.assertRaises(RequestException):
			client_get('http://request_exception_test.ut', object())

	def test_client_get_invalid_url_throws_request_exception(self):
		with self.assertRaises(RequestException) as e:
			client_get('invalid_url', object())
		self.assertIn('Invalid URL', str(e.exception))
		self.assertIn('invalid_url', str(e.exception))

	def test_client_get_invalid_return_type_throws_request_exception(self):
		with mock.patch("ocpi.v2_2.core.requests.requests.get", return_value = {'unit': 'test'}):
			with self.assertRaises(RequestException) as e:
				client_get('unit_tests.de', Versions)
			self.assertIn('Failed validating', str(e.exception))

	def test_client_get_valid_return_type_returns_expected_instance(self):
		expected = [Version(version='2.1.1', url="https://www.server.com/ocpi/2.1.1/"), Version(version='2.2', url="https://www.server.com/ocpi/2.2/")]
		with mock.patch("ocpi.v2_2.core.requests.requests.get", return_value = expected):
			versions = client_get('unit_tests.de', Versions)
			self.assertEqual(expected, versions)


	@mock.patch("ocpi.v2_2.core.requests.requests.post", throw_timeout_exception)
	def test_client_post_timeout_throws_expected_exception(self):
		with self.assertRaises(RequestException):
			client_post('http://timeout_test.ut', {}, object)

	@mock.patch("ocpi.v2_2.core.requests.requests.post", throw_request_exception)
	def test_client_post_requests_exeption_throws_expected_exception(self):
		with self.assertRaises(RequestException):
			client_post('http://request_exception_test.ut', {}, object)

	def test_client_post_invalid_url_throws_request_exception(self):
		with self.assertRaises(RequestException) as e:
			client_post('invalid_url', {}, object)
		self.assertIn('Invalid URL', str(e.exception))
		self.assertIn('invalid_url', str(e.exception))


	def test_client_post_invalid_return_type_throws_request_exception(self):
		with mock.patch("ocpi.v2_2.core.requests.requests.post", return_value = {'unit': 'test'}):
			with self.assertRaises(RequestException) as e:
				client_post('unit_tests.de', {}, Versions)
			self.assertIn('Failed validating', str(e.exception))

	def test_client_post_valid_return_type_returns_expected_instance(self):
		expected = [Version(version='2.1.1', url="https://www.server.com/ocpi/2.1.1/"), Version(version='2.2', url="https://www.server.com/ocpi/2.2/")]
		with mock.patch("ocpi.v2_2.core.requests.requests.post", return_value = expected):
			versions = client_post('unit_tests.de', {}, Versions)
			self.assertEqual(expected, versions)


	@mock.patch("ocpi.v2_2.core.requests.requests.put", throw_timeout_exception)
	def test_client_put_timeout_throws_expected_exception(self):
		with self.assertRaises(RequestException):
			client_put('http://timeout_test.ut', {}, object)

	@mock.patch("ocpi.v2_2.core.requests.requests.put", throw_request_exception)
	def test_client_put_requests_exeption_throws_expected_exception(self):
		with self.assertRaises(RequestException):
			client_put('http://request_exception_test.ut', {}, object)

	def test_client_put_invalid_url_throws_request_exception(self):
		with self.assertRaises(RequestException) as e:
			client_put('invalid_url', {}, object)
		self.assertIn('Invalid URL', str(e.exception))
		self.assertIn('invalid_url', str(e.exception))


	def test_client_put_invalid_return_type_throws_request_exception(self):
		with mock.patch("ocpi.v2_2.core.requests.requests.put", return_value = {'unit': 'test'}):
			with self.assertRaises(RequestException) as e:
				client_put('unit_tests.de', {}, Versions)
			self.assertIn('Failed validating', str(e.exception))

	def test_client_put_valid_return_type_returns_expected_instance(self):
		expected = [Version(version='2.1.1', url="https://www.server.com/ocpi/2.1.1/"), Version(version='2.2', url="https://www.server.com/ocpi/2.2/")]
		with mock.patch("ocpi.v2_2.core.requests.requests.put", return_value = expected):
			versions = client_put('unit_tests.de', {}, Versions)
			self.assertEqual(expected, versions)


