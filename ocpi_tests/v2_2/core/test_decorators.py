import unittest
import unittest.mock as mock

from ocpi.v2_2.core.responses import ResponseHttp
from ocpi.v2_2.core.decorators import *

class ValidateJsonRequestDecoratorTests(unittest.TestCase):

	def validate_json_request_test(self, json_request, expected_msg, expected_code):
		@validate_json_request
		def f():
			return ResponseHttp('OK', 200).make_response()
		request_mock = mock.MagicMock()
		request_mock.json = json_request
		with mock.patch("ocpi.v2_2.core.decorators.request", request_mock):
			self.assertEqual(f(), (expected_msg, expected_code))

	def test_validate_json_request_not_valid_json(self):
		self.validate_json_request_test(None, "Content-Type=application/json required", 400)

	def test_validate_json_request_valid_json(self):
		self.validate_json_request_test({ "unit": "test"}, 'OK', 200)

	def require_last_updated_json_field_test(self, json_request, expected_msg, expected_code):
		@require_last_updated_json_field
		def f():
			return ResponseHttp('OK', 200).make_response()
		request_mock = mock.MagicMock()
		request_mock.json = json_request
		with mock.patch("ocpi.v2_2.core.decorators.request", request_mock):
			self.assertEqual(f(), (expected_msg, expected_code))

	def test_require_last_updated_json_field_missing_field(self):
		self.require_last_updated_json_field_test({ "unit": "test"}, "Any request to the PUT/PATCH methods SHALL contain the last_updated field.", 400)

	def test_require_last_updated_json_field_valid_json(self):
		self.require_last_updated_json_field_test({ "unit": "test", "last_updated" : "Now"}, 'OK', 200)


