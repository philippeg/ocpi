import unittest
import unittest.mock as mock
import json

from ocpi_tests.v2_2.test_base import TestBase

from ocpi.v2_2.objects.credentials import Credentials
from ocpi.v2_2.objects.versions import Versions, Version, Endpoints, Endpoint
from ocpi.v2_2.objects.common import BusinessDetails
from ocpi.v2_2.core.utils import *

class UtilsTests(TestBase):
	def test_pick_version_with_unique_2_2_client_version(self):
		versions = Versions([Version(version='2.2', url='example.com')])
		version = pick_version(versions, '2.2')
		self.assertIsNotNone(version)
		self.assertEqual('example.com', version.url)
		self.assertEqual('2.2', version.version)

	def test_pick_version_with_existing_2_2_client_version(self):
		versions = Versions([Version(version='2.2', url='example.com'), Version(version='2.1.1', url='not_mutual.com')])
		version = pick_version(versions, '2.2')
		self.assertIsNotNone(version)
		self.assertEqual('example.com', version.url)
		self.assertEqual('2.2', version.version)

	def test_pick_version_with_missing_2_2_client_version(self):
		versions = Versions([Version(version='2.0', url='example.com')])
		version = pick_version(versions, '2.2')
		self.assertIsNone(version)

	def test_pick_version_with_invalid_argument(self):
		version = pick_version({'unit':'tests'}, '2.2')
		self.assertIsNone(version)

	def test_get_client_url_for_module_returns_expected_value(self):
		endpoints = Endpoints(version="2.2",
				endpoints=[
					Endpoint(identifier= "credentials", role= "CPO", url= "https://example.com/ocpi/cpo/2.2/credentials/"),
					Endpoint(identifier= "locations", role= "CPO", url= "https://example.com/ocpi/cpo/2.2/locations/")])
		self.assertEqual('https://example.com/ocpi/cpo/2.2/credentials/', get_client_url_for_module('credentials', endpoints))
		self.assertEqual('https://example.com/ocpi/cpo/2.2/locations/', get_client_url_for_module('locations', endpoints))
		self.assertIsNone(get_client_url_for_module('invalid', endpoints))

	def test_patch_json_type_with_assigned_field_returns_expected_result(self):
		version = Version(version='2.2', url='example.com')
		patch_json = { 'url': 'modified.com'}

		result = patch_json_type(version, patch_json)

		self.assertIsInstance(result, Version)
		self.assertEqual(result, Version(version='2.2', url='modified.com'))

	def test_patch_json_type_with_unassigned_field_returns_expected_result(self):
		details = BusinessDetails(name='ocpi')
		patch_json = { 'website': 'site.com'}

		result = patch_json_type(details, patch_json)

		self.assertIsInstance(result, BusinessDetails)
		self.assertEqual(result, BusinessDetails(name='ocpi', website='site.com'))

	def test_build_next_url_without_additional_args_returns_expected_string(self):
		result = build_next_url("http://test.com/ocpi/module", dict(), 10)
		self.assertEqual("<http://test.com/ocpi/module?offset=10>; rel=\"next\"", result)

	def test_build_next_url_with_additional_args_returns_expected_string(self):
		result = build_next_url("http://test.com/ocpi/module", {"limit": "50", "start_date": "01-01-99"}, 10)
		self.assertEqual("<http://test.com/ocpi/module?limit=50&start_date=01-01-99&offset=10>; rel=\"next\"", result)

	def test_build_next_url_with_existing_offset_arg_returns_expected_string(self):
		result = build_next_url("http://test.com/ocpi/module", dict(), 10)
		result = build_next_url("http://test.com/ocpi/module", {"offset": "5", "limit": "50", "start_date": "01-01-99"}, 10)
		self.assertEqual("<http://test.com/ocpi/module?limit=50&start_date=01-01-99&offset=10>; rel=\"next\"", result)