import unittest
import unittest.mock as mock

from ocpi_tests.v2_2.test_base import TestBase

from ocpi.v2_2.core.queries import *

class QueriesTests(TestBase):
	def test_any_item_no_matching_item_returns_false(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = any_item(lambda x: x[0] > 4, source)
		self.assertFalse(result)

	def test_any_item_matching_item_returns_true(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = any_item(lambda x: x[0] == 3, source)
		self.assertTrue(result)

	def test_all_item_item_not_verifying_predicate_returns_false(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = all_item(lambda x: x[0] < 3, source)
		self.assertFalse(result)

	def test_all_item_all_items_verifying_predicate_returns_true(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = all_item(lambda x: x[0] < 4, source)
		self.assertTrue(result)

	def test_min_value_empty_list_raises_exception(self):
		source = []
		with self.assertRaises(ValueError):
			min_value(lambda x: x[0], source)
		
	def test_min_value_single_item_returns_expected_value(self):
		source = [(1, 'a')]
		result = min_value(lambda x: x[0], source)
		self.assertEqual(1, result)

	def test_min_value_returns_expected_result(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = min_value(lambda x: x[0], source)
		self.assertEqual(1, result)

	def test_max_value_empty_list_raises_exception(self):
		source = []
		with self.assertRaises(ValueError):
			max_value(lambda x: x[0], source)
		
	def test_max_value_single_item_returns_expected_value(self):
		source = [(1, 'a')]
		result = max_value(lambda x: x[0], source)
		self.assertEqual(1, result)

	def test_max_value_returns_expected_result(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = max_value(lambda x: x[0], source)
		self.assertEqual(3, result)

	def test_take_negative_count_raises_exception(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		with self.assertRaises(ValueError):
			take(source, -1)
		
	def test_take_zero_count_returns_empty_list(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = take(source, 0)
		self.assertEquals([], result)
		
	def test_take_count_greater_than_count_returns_source(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = take(source, 4)
		self.assertEquals(source, result)

	def test_take_returns_expected_result(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = take(source, 2)
		self.assertEquals([(1, 'a'), (2, 'b')], result)


	def test_skip_negative_count_raises_exception(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		with self.assertRaises(ValueError):
			skip(source, -1)
		
	def test_skip_zero_count_returns_source(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = skip(source, 0)
		self.assertEquals(source, result)
		
	def test_skip_count_greater_than_count_returns_empty_list(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = skip(source, 4)
		self.assertEquals([], result)

	def test_skip_returns_expected_result(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = skip(source, 2)
		self.assertEquals([(3, 'c')], result)

	def test_first_or_none_returns_expected_result(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = first_or_none(lambda x: x[0] == 2, source)
		self.assertEquals((2, 'b'), result)

	def test_first_or_none_no_matching_item_returns_none(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = first_or_none(lambda x: x[0] == 4, source)
		self.assertIsNone(result)

	def test_first_or_none_empty_source_returns_none(self):
		source = []
		result = first_or_none(lambda x: x[0] == 4, source)
		self.assertIsNone(result)

	def test_count_returns_expected_result(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = count(lambda x: x[0] <= 2, source)
		self.assertEquals(2, result)
		result = count(lambda x: x[0] > 0, source)
		self.assertEquals(3, result)

	def test_count_no_matching_item_returns_zero(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = count(lambda x: x[0] == 4, source)
		self.assertEquals(0, result)

	def test_select_returns_expected_result(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = select(lambda x: x[0], source)
		self.assertEquals([1,2,3], result)
		result = select(lambda x: x[1], source)
		self.assertEquals(['a','b','c'], result)

	def test_where_returns_expected_result(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = where(lambda x: x[0] == 2, source)
		self.assertEquals([(2, 'b')], result)
		result = where(lambda x: x[0] > 0, source)
		self.assertEquals(source, result)

	def test_where_no_matching_item_returns_zero(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = where(lambda x: x[0] == 4, source)
		self.assertEquals([], result)


	def test_update_or_add_source_does_not_contains_item_appends_new_item(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = update_or_add(lambda x: x[0] == 4, (4, 'd'), source)
		self.assertEquals([(1, 'a'), (2, 'b'), (3, 'c'), (4, 'd')], result)

	def test_update_or_add_sources_contains_item_updates_item(self):
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = update_or_add(lambda x: x[0] == 3, (3, 'd'), source)
		self.assertEquals([(1, 'a'), (2, 'b'), (3, 'd')], result)
		source = [(1, 'a'), (2, 'b'), (3, 'c')]
		result = update_or_add(lambda x: x[0] == 1, (1, 'd'), source)
		self.assertEquals([(1, 'd'), (2, 'b'), (3, 'c')], result)

