import unittest
from ocpi_tests.v2_2.test_base import TestBase

from ocpi.v2_2.core.responses import SuccessResponseJson, ClientErrorResponseJson, InvalidOrMissingParametersResponseJson, \
	NotEnoughInformationResponseJson, UnknownLocationResponseJson, ServerErrorResponseJson, UnableToUseClientAPIResponseJson, \
		UnsupportedVersionResponseJson, NoMatchingOrMissingEnpointsResponseJson, UnknownReceiverResponseJson, TimeoutOnForwardedRequestResponseJson, \
			ConnectionProblemResponseJson

class ResponseJsonTests(TestBase):
	
	def test_responses_with_default_arguments(self):
		with self.app_context():
			for test in [
				{'type':SuccessResponseJson, 'status_code':1000, 'status_message':"Success"},
				{'type':ClientErrorResponseJson, 'status_code':2000, 'status_message':"Generic client error"},
				{'type':InvalidOrMissingParametersResponseJson, 'status_code':2001, 'status_message':"Invalid or missing parameters"},
				{'type':NotEnoughInformationResponseJson, 'status_code':2002, 'status_message':"Not enough information"},
				{'type':UnknownLocationResponseJson, 'status_code':2003, 'status_message':"Unknown Location"},
				{'type':ServerErrorResponseJson, 'status_code':3000, 'status_message':"Generic server error"},
				{'type':UnableToUseClientAPIResponseJson, 'status_code':3001, 'status_message':"Unable to use the client’s API"},
				{'type':UnsupportedVersionResponseJson, 'status_code':3002, 'status_message':"Unsupported version"},
				{'type':NoMatchingOrMissingEnpointsResponseJson, 'status_code':3003, 'status_message':"No matching endpoints or expected endpoints missing"},
				{'type':UnknownReceiverResponseJson, 'status_code':4001, 'status_message':"Unknown receiver (TO address is unknown)"},
				{'type':TimeoutOnForwardedRequestResponseJson, 'status_code':4002, 'status_message':"Timeout on forwarded request (message is forwarded, but request times out)"},
				{'type':ConnectionProblemResponseJson, 'status_code':4003, 'status_message':"Connection problem (receiving party is not connected)"}]:

				response = test['type']().make_response()
				self.assertValidOcpiResponseFormat(response, test['status_code'], test['status_message'])
				self.assertOcpiResponseDataEquals(response, None)
			
	def test_responses_with_specific_arguments(self):
		data = { 'test': True }
		status_message = "Test message"

		with self.app_context():
			for test in [
				{'type':SuccessResponseJson, 'status_code':1000},
				{'type':ClientErrorResponseJson, 'status_code':2000},
				{'type':InvalidOrMissingParametersResponseJson, 'status_code':2001},
				{'type':NotEnoughInformationResponseJson, 'status_code':2002},
				{'type':UnknownLocationResponseJson, 'status_code':2003},
				{'type':ServerErrorResponseJson, 'status_code':3000},
				{'type':UnableToUseClientAPIResponseJson, 'status_code':3001},
				{'type':UnsupportedVersionResponseJson, 'status_code':3002},
				{'type':NoMatchingOrMissingEnpointsResponseJson, 'status_code':3003},
				{'type':UnknownReceiverResponseJson, 'status_code':4001},
				{'type':TimeoutOnForwardedRequestResponseJson, 'status_code':4002},
				{'type':ConnectionProblemResponseJson, 'status_code':4003}]:

				response = test['type'](data, status_message).make_response()
				self.assertValidOcpiResponseFormat(response, test['status_code'], status_message)
				self.assertOcpiResponseDataEquals(response, data)

	# TODO check if necessary
	#def test_ResponseJson_Decimal_serialization(self):
	#	with app.app_context():
	#		data = { 'decimal': Decimal(7)}
	#		response = ResponseJson(data, 0000).make_response()
	#		self.assertEqual({'decimal': 7.0}, response.get_json()['data'])

if __name__ == '__main__':
	unittest.main()