import unittest
import unittest.mock as mock

from flask import url_for

from ocpi_tests.v2_2.test_base import *

class VersionDetailsTests(TestBase):

	def test_get_with_token_a_returns_200(self):
		self.assertAuthorizationWithTokenA(200, '/ocpi/2.2', self.GET())

	def test_get_with_token_c_returns_200(self):
		self.assertAuthorizationWithTokenC(200, '/ocpi/2.2', self.GET())

	def test_get_without_token_returns_401(self):
		self.assertAuthorizationWithoutToken(401, '/ocpi/2.2', self.GET())

	def test_post_not_allowed(self):
		self.assertHttpCode(405, '/ocpi/2.2', self.POST())

	def test_put_not_allowed(self):
		self.assertHttpCode(405, '/ocpi/2.2', self.PUT())

	def test_delete_not_allowed(self):
		self.assertHttpCode(405, '/ocpi/2.2', self.DELETE())

	@without_authentication
	def test_get_returns_valid_response_format(self):
		response = self.performRequest('/ocpi/2.2', self.GET())
		self.assertValidOcpiResponseFormat(response, 1000, "Success")

	@without_authentication
	def test_get_returns_json_response_data(self):
		response = self.performRequest('/ocpi/2.2', self.GET())
		self.assertJsonResponseDataIsNotNone(response)

	@without_authentication
	def test_get_returns_expected_version_details(self):
		url_credentials = self.get_url_for('ocpi.get_credentials')
		url_locations = self.get_url_for('ocpi.get_location', country_code='C', party_id='P',location_id='L').split('/C',1)[0]
		url_sessions = self.get_url_for('ocpi.get_session', _external = True, country_code='C', party_id='P',session_id='L').split('/C',1)[0]
		url_cdrs = self.get_url_for('ocpi.get_cdr', _external = True, country_code='C', party_id='P',cdr_id='L').split('/C',1)[0]
		url_tariffs = self.get_url_for('ocpi.get_tariff', _external = True, country_code='C', party_id='P',tariff_id='L').split('/C',1)[0]
		url_tokens = self.get_url_for('ocpi.get_tokens', _external = True)
		url_clientinfo = self.get_url_for('ocpi.get_clientinfo', _external = True, country_code='C', party_id='P').split('/C',1)[0]
	
	
		credential_endpoints = {'identifier': 'credentials', 'role': 'MSP', 'url': url_credentials}
		locations_endpoints = {'identifier': 'locations', 'role': 'MSP', 'url': url_locations}
		sessions_endpoint = {"identifier": "sessions", "role": "MSP", "url": url_sessions}
		cdrs_endpoint = {"identifier": "cdrs", "role": "MSP", "url": url_cdrs}
		tariffs_endpoint = {"identifier": "tariffs", "role": "MSP", "url": url_tariffs}
		tokens_endpoint = {"identifier": "tokens", "role": "MSP","url": url_tokens}	
		clientinfo_endpoint = {"identifier": "hubclientinfo", "role": "MSP", "url": url_clientinfo}	

		expected = {'version': '2.2', 'endpoints':[credential_endpoints, locations_endpoints, sessions_endpoint, cdrs_endpoint, tariffs_endpoint, tokens_endpoint, clientinfo_endpoint]}

		response = self.performRequest('/ocpi/2.2', self.GET())
		version_details = response.get_json()['data']
		self.assertEqual(version_details, expected)
		

if __name__ == '__main__':
	unittest.main()
