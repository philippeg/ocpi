import os
os.sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from functools import wraps
from flask import url_for

import unittest
import unittest.mock as mock

from app import app
from ocpi.v2_2.objects.common import BusinessDetails, Response
from ocpi.v2_2.objects.credentials import Credentials, CredentialsRole
from ocpi.v2_2.auth.services import TokenManager, AuthorizationService



class TestingConfig(object):
	DEBUG = False
	TESTING = True
	SERVER_NAME = 'localhost'
	AUTH_TOKEN_A = 'A=bJOXxkxOAuKR92z0nEcmVF3Qw09VG7I7d/WCg0koM='
	AUTH_TOKEN_C = 'C=bJOXxkxOAuKR92z0nEcmVF3Qw09VG7I7d/WCg0koM='

# Authorization decorators

# authorize testing tokens based on OCPI's token logic 
def __credentials(token):
	return Credentials(token=token, url='test_url', roles=[CredentialsRole(role='CPO', business_details=BusinessDetails(name= 'Unit Tests'), party_id= 'UNI', country_code= 'DE') ])
	
def with_test_token_authentication(func):
	@wraps(func)
	@mock.patch.object(TokenManager, 'resolve_token_c', lambda self, token: __credentials(token) if token == app.config['AUTH_TOKEN_C'] else None)
	@mock.patch.object(TokenManager, 'resolve_token_a', lambda self, token: __credentials(token) if token == app.config['AUTH_TOKEN_A'] else None)
	def wrapper(*args, **kwargs):
		return func(*args, **kwargs)
	return wrapper

def with_custom_authentication(resolve_token_c, resolve_token_a):
	def decorator(func):
		@wraps(func)
		@mock.patch.object(TokenManager, 'resolve_token_c', resolve_token_c)
		@mock.patch.object(TokenManager, 'resolve_token_a', resolve_token_a)
		def wrapper(*args, **kwargs):
			return func(*args, **kwargs)
		return wrapper
	return decorator

# skip authentication mechanism
def without_authentication(func):
	@wraps(func)
	def wrapper(*args, **kwargs):
		with mock.patch.object(AuthorizationService, 'resolve_credentials_from_token_c' , lambda self, token: __credentials(token)):
			return func(*args, **kwargs)
	return wrapper


def mock_check_role_authorization(func, *args, **kwargs):
	return func(*args, **kwargs)

def without_role_autorization(func):
	@wraps(func)
	def wrapper(*args, **kwargs):
		with mock.patch('ocpi.v2_2.auth.decorators.__check_role_authorization',  mock_check_role_authorization):
			return func(*args, **kwargs)
	return wrapper


class TestBase(unittest.TestCase):

	def setUp(self):
		self.app = self.create_test_client()

	def create_test_client(self):
		app.config.from_object(TestingConfig)
		return app.test_client()

	def app_context(self):
		return app.app_context()

	def get_url_for(self, endpoint, **kargv):
		with app.app_context():
			return url_for(endpoint, **kargv)

	def GET(self):
		return self.app.get

	def POST(self):
		return self.app.post

	def PUT(self):
		return self.app.put

	def PATCH(self):
		return self.app.patch

	def DELETE(self):
		return self.app.delete

	def resolve_test_token_a_throws(self, token):
		raise Exception('A very specific bad thing happened.')


	def resolve_test_token_c(self, token):
		return Credentials(token=token, url='test_url', roles=[CredentialsRole(role='CPO', business_details=BusinessDetails(name= 'Unit Tests'), party_id= 'UNI', country_code= 'DE') ])
	
	def TokenA(self):
		return app.config['AUTH_TOKEN_A']

	def TokenC(self):
		return app.config['AUTH_TOKEN_C']

	def AuthorizationToken(self, token = None):
		return {'Authorization': 'Token {}'.format(token)}

	def AuthorizationTokenA(self):
		return self.AuthorizationToken(self.TokenA())
		
	def AuthorizationTokenC(self):
		return self.AuthorizationToken(self.TokenC())

	def assertHttpCode(self, http_code, route, method, headers = {'Authorization': 'Token None'}, data = None):
		response = method(route, headers=headers, json=data)
		self.assertEqual(response.status_code, http_code)

	def assertAuthorizationWithTokenA(self, status_code, route, method, data=None):
		self.assertAuthorizationWithToken(status_code, route, method, self.AuthorizationTokenA(), data)

	def assertAuthorizationWithTokenC(self, status_code, route, method, data=None):
		self.assertAuthorizationWithToken(status_code, route, method, self.AuthorizationTokenC(), data)

	def assertAuthorizationWithoutToken(self, status_code, route, method):
		self.assertAuthorizationWithToken(status_code, route, method, {'Authorization': 'Token None'})

	def assertAuthorizationWithTokenAandTokenC(self, route, method):
		self.assertAuthorizationWithToken(200, route, method, self.AuthorizationTokenA())
		self.assertAuthorizationWithToken(200, route, method, self.AuthorizationTokenC())
		self.assertAuthorizationWithToken(401, route, method, {'Authorization': None})


	@with_test_token_authentication
	def assertAuthorizationWithToken(self, http_code, route, method, token, data=None):
		self.assertHttpCode(http_code, route, method, token, data)

	def performRequest(self, route, method, data = None, headers={'Authorization': 'Token None'}):
		return method(route, headers=headers, data=data, content_type='application/json')

	def assertJsonResponseDataIsNotNone(self, response):
		self.assertIsNotNone(response.get_json())
		json_data = response.get_json()['data']
		self.assertIsNotNone(json_data)

	def assertJsonResponseIsValidInstanceOf(self, type, data):
		try:
			obj = type(data)
			self.assertIsNotNone(obj)
		except Exception as e:
			self.fail(e)

	# 4.1.6. Response format
	def assertValidOcpiResponseFormat(self, response, status_code, status_message):
		json = response.get_json()
		self.assertJsonResponseIsValidInstanceOf(Response, json)
		obj = Response(json)
		self.assertEqual(status_code, obj.status_code)
		self.assertEqual(status_message, obj.status_message)
		
	def assertOcpiResponseDataEquals(self, response, data):
		json = response.get_json()
		self.assertTrue('data' in json)
		self.assertEqual(data, json['data'])

	# Endpoints
	def collect_routes(self):
		with self.app.application.app_context():
			routes = []
			for rule in self.app.application.url_map.iter_rules():
				#options = {}
				#for arg in rule.arguments:
				#	options[arg] = "[{0}]".format(arg)
				methods = [m for m in rule.methods if m not in ['OPTIONS', 'HEAD']]
				try:
					url = rule.rule
					routes.append({'url': url, 'method': next(iter(methods))})
				except Exception:
					pass
			return routes
