import unittest
from ocpi.v2_2.db.aws.aws_proxy import *

class AwsProxyTests(unittest.TestCase):

	def test_build_key_returns_expected_results(self):
		proxy = AWSProxy()
		testcases = [('_', ['a','b','c'], 'a_b_c'), ('_', ['a','B','c'], 'a_b_c'), ('&', ['A','B'], 'a&b')]
		for (sep,args,expected) in testcases:
			result = proxy.build_key(*args, separator = sep)
			self.assertEqual(expected, result)

	def test_build_key_with_default_separator_returns_expected_results(self):
		proxy = AWSProxy()
		testcases = [(['a','b','c'], 'a_b_c'), (['a','B','c'], 'a_b_c'), (['A','B'], 'a_b')]
		for (args,expected) in testcases:
			result = proxy.build_key(*args)
			self.assertEqual(expected, result)

	def test_is_date_between_without_date_from_returns_expected_boolean(self):
		proxy = AWSProxy()
		date  = '2017-03-16T10:10:02Z'
		date_before = '2017-01-16T10:10:02Z'
		date_after = '2017-06-16T10:10:02Z'

		testcases = [(date, None, date_after, True),
		(date, None, date, False),
		(date, None, date_before, False)]
		for (date, date_from, date_to, expected) in testcases:
			result = proxy.is_date_between(date, date_from, date_to)
			self.assertEqual(expected, result)

	def test_is_date_between_without_date_to_returns_expected_boolean(self):
		proxy = AWSProxy()
		date  = '2017-03-16T10:10:02Z'
		date_before = '2017-01-16T10:10:02Z'
		date_after = '2017-06-16T10:10:02Z'

		testcases = [(date, date_before, None, True),
		(date, date, None, True),
		(date, date_after, None, False)]
		for (date, date_from, date_to, expected) in testcases:
			result = proxy.is_date_between(date, date_from, date_to)
			self.assertEqual(expected, result)

	def test_is_date_between_without_date_limits_returns_expected_boolean(self):
		proxy = AWSProxy()
		date  = '2017-03-16T10:10:02Z'

		testcases = [(date, None, None, True), 
		(None, None, None, False)]
		for (date, date_from, date_to, expected) in testcases:
			result = proxy.is_date_between(date, date_from, date_to)
			self.assertEqual(expected, result)


	def test_is_date_between_with_date_limits_returns_expected_boolean(self):
		proxy = AWSProxy()
		date  = '2017-03-16T10:10:02Z'
		date_before = '2017-01-16T10:10:02Z'
		date_after = '2017-06-16T10:10:02Z'

		testcases = [(date, date_before, date_after, True), 
		(date_before, date_before, date_after, True), 
		(date_after, date_before, date_after, False), 
		(date_before, date, date_after, False), 
		(date_after, date_before, date, False)]
		for (date, date_from, date_to, expected) in testcases:
			result = proxy.is_date_between(date, date_from, date_to)
			self.assertEqual(expected, result)

if __name__ == '__main__':
	unittest.main()