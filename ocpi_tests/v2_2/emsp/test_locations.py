import unittest
import unittest.mock as mock
import json

from ocpi_tests.v2_2.test_base import *

from ocpi.v2_2.objects.locations import Location, EVSE, Connector, GeoLocation
from ocpi.v2_2.objects.credentials import Credentials, CredentialsRole, BusinessDetails

	
class LocationsTests(TestBase):
	
	def locations_endpoints_examples(self):
		return ['/ocpi/emsp/2.2/locations/BE/BEC/LOC1','/ocpi/emsp/2.2/locations/BE/BEC/LOC1/3256','/ocpi/emsp/2.2/locations/BE/BEC/LOC1/3256/1']

	@without_role_autorization
	def test_get_with_token_c_and_invalid_request_returns_400(self):
		for url in self.locations_endpoints_examples():
			self.assertAuthorizationWithTokenC(400, url, self.GET())

	def test_get_with_token_a_returns_401(self):
		for url in self.locations_endpoints_examples():
			self.assertAuthorizationWithTokenA(401, url, self.GET())

	def test_get_without_token_returns_401(self):
		for url in self.locations_endpoints_examples():
			self.assertAuthorizationWithoutToken(401, url, self.GET())

	def resolve_test_token_a_throws(self, token):
		raise Exception('A very specific bad thing happened.')

	def resolve_test_token_c(self, token):
		return Credentials(token=token, url='test_url', roles=[CredentialsRole(role='CPO', business_details=BusinessDetails(name= 'Unit Tests'), party_id= 'UNI', country_code= 'DE') ])
	

	@with_custom_authentication(resolve_test_token_c, None)
	def test_get_location_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/locations/FR/UNI/LOC1', self.GET())

	@with_custom_authentication(resolve_test_token_c, None)
	def test_get_evse_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/locations/FR/UNI/LOC1/3256', self.GET())

	@with_custom_authentication(resolve_test_token_c, None)
	def test_get_connector_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/locations/FR/UNI/LOC1/3256/1', self.GET())

#	@without_authentication
#	def test_get_returns_valid_response_format(self):
#		for url in self.locations_endpoints_examples():
#			response = self.performRequest(url, self.GET())
#			self.assertValidOcpiResponseFormat(response, 1000, "Success")


	def configure_mock_controller_with_get_location(self, mock_controller, get_location_side_effect):
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		mock_proxy.get_location.side_effect = get_location_side_effect
		return mock_proxy


	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_location_returns_location_response(self, mock_controller):
		def get_location_side_effect(country_code, party_id, location_id):
			if country_code == "BE" and party_id == 'BEK' and location_id == 'LOC1':
				return Location(country_code="BE",party_id="BEK",id="LOC1",type='ON_STREET',address= "F.Rooseveltlaan 3A", city='Gent',country="BEL",coordinates = GeoLocation(latitude= "3.729944",longitude= "51.047599"), last_updated="2015-06-28T08:12:01Z")

		self.configure_mock_controller_with_get_location(mock_controller, get_location_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/locations/BE/BEK/LOC1', self.GET())
			
		json = response.get_json()['data']
		self.assertIsNotNone(Location(json))

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_location_with_invalid_arguments_returns_2001_status_code(self, mock_controller):
		def get_location_side_effect(country_code, party_id, location_id):
			return None
		self.configure_mock_controller_with_get_location(mock_controller, get_location_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/locations/BE/BEC/LOC1', self.GET())

		self.assertValidOcpiResponseFormat(response, 2001, 'Location BE/BEC/LOC1 does not exist in the eMSP system')

			
	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_evse_returns_location_response(self, mock_controller):
		def get_evse_side_effect(country_code, party_id, location_id, evse_uid):
			if country_code == "BE" and party_id == 'BEC' and location_id == 'LOC1' and evse_uid == '3256':
				connector = Connector(id= "1",standard= "IEC_62196_T2",format= "CABLE",power_type= "AC_3_PHASE", max_voltage= 220, max_amperage= 16, last_updated="2015-06-28T08:12:01Z")
				return EVSE(uid= "3256", status= 'AVAILABLE', connectors = [connector], last_updated="2015-06-28T08:12:01Z")

		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		mock_proxy.get_evse.side_effect = get_evse_side_effect

		response = self.performRequest('/ocpi/emsp/2.2/locations/BE/BEC/LOC1/3256', self.GET())
		json = response.get_json()['data']
		self.assertIsNotNone(EVSE(json))

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_evse_with_invalid_arguments_returns_2001_status_code(self, mock_controller):
		def get_evse_side_effect(country_code, party_id, location_id, evse_uid):
			return None
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		mock_proxy.get_evse.side_effect = get_evse_side_effect

		response = self.performRequest('/ocpi/emsp/2.2/locations/BE/BEC/LOC1/3256', self.GET())
		self.assertValidOcpiResponseFormat(response, 2001, 'EVSE BE/BEC/LOC1/3256 does not exist in the eMSP system')

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_connector_returns_location_response(self, mock_controller):
		def get_connector_side_effect(country_code, party_id, location_id, evse_uid, connector_id):
			if country_code == "BE" and party_id == 'BEC' and location_id == 'LOC1' and evse_uid == '3256' and connector_id == '1':
				return Connector(id= "1",standard= "IEC_62196_T2",format= "CABLE",power_type= "AC_3_PHASE", max_voltage= 220, max_amperage= 16, last_updated="2015-06-28T08:12:01Z")
		
		mock_proxy = mock.MagicMock()
		mock_proxy.get_connector.side_effect = get_connector_side_effect
		mock_controller.return_value = mock_proxy

		response = self.performRequest('/ocpi/emsp/2.2/locations/BE/BEC/LOC1/3256/1', self.GET())
		json = response.get_json()['data']
		self.assertIsNotNone(Connector(json))

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_connector_with_invalid_arguments_returns_2001_status_code(self, mock_controller):
		def get_connector_side_effect(country_code, party_id, location_id, evse_uid, connector_id):
			return None
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		mock_proxy.get_connector.side_effect = get_connector_side_effect

		response = self.performRequest('/ocpi/emsp/2.2/locations/BE/BEC/LOC1/3256/1', self.GET())
		self.assertValidOcpiResponseFormat(response, 2001, 'Connector BE/BEC/LOC1/3256/1 does not exist in the eMSP system')

	#PUT unit tests
	def create_test_location_as_json(self):
		location = Location(country_code="BE",party_id="BEK",id="LOC1",type='ON_STREET',address= "F.Rooseveltlaan 3A", city='Gent',country="BEL",coordinates = GeoLocation(latitude="3.729944",longitude="51.047599"), last_updated="2015-06-28T08:12:01Z")
		return str(location)

	@with_custom_authentication(resolve_test_token_c, None)
	def test_put_location_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/locations/FR/UNI/LOC1', self.PUT())
	
	@with_custom_authentication(resolve_test_token_c, resolve_test_token_a_throws)
	def test_put_evse_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/locations/FR/UNI/LOC1/3256', self.PUT())

	@with_custom_authentication(resolve_test_token_c, resolve_test_token_a_throws)
	def test_put_connector_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/locations/FR/UNI/LOC1/3256/1', self.PUT())

	@without_authentication
	@without_role_autorization
	def test_put_location_with_missing_last_updated_fields_returns_400(self):
		self.assertHttpCode(400, '/ocpi/emsp/2.2/locations/FR/UNI/LOC1', self.PUT(), data = {"unit": "test"})
	
	@without_authentication
	@without_role_autorization
	def test_put_evse_with_missing_last_updated_fields_returns_400(self):
		self.assertHttpCode(400, '/ocpi/emsp/2.2/locations/FR/UNI/LOC1/3256', self.PUT(), data = {"unit": "test"})

	@without_authentication
	@without_role_autorization
	def test_put_connector_with_missing_last_updated_fields_returns_400(self):
		self.assertHttpCode(400, '/ocpi/emsp/2.2/locations/FR/UNI/LOC1/3256/1', self.PUT(), data = {"unit": "test"})

	def as_json(self, instance):
		return json.dumps(instance)

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_put_location_updates_location(self, mock_controller):
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		location = Location(country_code="BE",party_id="BEK",id="LOC1",type='ON_STREET',address= "F.Rooseveltlaan 3A", city='Gent',country="BEL",coordinates = GeoLocation(latitude= "3.729944",longitude= "51.047599"), last_updated="2015-06-28T08:12:01Z")

		response = self.performRequest('/ocpi/emsp/2.2/locations/BE/BEK/LOC1', self.PUT(), data=self.as_json(location))

		mock_proxy.add_or_update_location.assert_called_with('BE', 'BEK', 'LOC1', location)
		json = response.get_json()['data']
		self.assertIsNotNone(Location(json))

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_put_evse_updates_location(self, mock_controller):
		connector = Connector(id= "1",standard= "IEC_62196_T2",format= "CABLE",power_type= "AC_3_PHASE", max_voltage= 220, max_amperage= 16, last_updated="2015-06-28T08:12:01Z")
		evse = EVSE(uid= "3256", status= 'AVAILABLE', connectors = [connector], last_updated="2015-06-28T08:12:01Z")
		location = Location(country_code="BE",party_id="BEK",id="LOC1",evses=[evse], type='ON_STREET',address= "F.Rooseveltlaan 3A", city='Gent',country="BEL",coordinates = GeoLocation(latitude= "3.729944",longitude= "51.047599"), last_updated="2015-06-28T08:12:01Z")
		updated_evse = EVSE(uid= "3256", status= 'BLOCKED', connectors = [connector], last_updated="2018-06-28T08:12:01Z")
		updated_location = Location(country_code="BE",party_id="BEK",id="LOC1",evses=[updated_evse], type='ON_STREET',address= "F.Rooseveltlaan 3A", city='Gent',country="BEL",coordinates = GeoLocation(latitude= "3.729944",longitude= "51.047599"), last_updated="2018-06-28T08:12:01Z")

		def get_location_side_effect(country_code, party_id, location_id):
			if country_code == "BE" and party_id == 'BEK' and location_id == 'LOC1':
				return location
		mock_proxy = self.configure_mock_controller_with_get_location(mock_controller, get_location_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/locations/BE/BEK/LOC1/3256', self.PUT(), data=self.as_json(updated_evse))

		mock_proxy.add_or_update_location.assert_called_with('BE', 'BEK', 'LOC1', updated_location)
		json = response.get_json()['data']
		self.assertIsNotNone(EVSE(json))

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_put_connector_updates_location(self, mock_controller):
		connector = Connector(id= "1",standard= "IEC_62196_T2",format= "CABLE",power_type= "AC_3_PHASE", max_voltage= 220, max_amperage= 16, last_updated="2015-06-28T08:12:01Z")
		evse = EVSE(uid= "3256", status= 'AVAILABLE', connectors = [connector], last_updated="2015-06-28T08:12:01Z")
		location = Location(country_code="BE",party_id="BEK",id="LOC1",evses=[evse], type='ON_STREET',address= "F.Rooseveltlaan 3A", city='Gent',country="BEL",coordinates = GeoLocation(latitude= "3.729944",longitude= "51.047599"), last_updated="2015-06-28T08:12:01Z")
		updated_connector = Connector(id= "1",standard= "IEC_62196_T2",format= "CABLE",power_type= "AC_3_PHASE", max_voltage= 220, max_amperage= 32, last_updated="2018-06-28T08:12:01Z")
		updated_evse = EVSE(uid= "3256", status= 'AVAILABLE', connectors = [updated_connector], last_updated="2018-06-28T08:12:01Z")
		updated_location = Location(country_code="BE",party_id="BEK",id="LOC1",evses=[updated_evse], type='ON_STREET',address= "F.Rooseveltlaan 3A", city='Gent',country="BEL",coordinates = GeoLocation(latitude= "3.729944",longitude= "51.047599"), last_updated="2018-06-28T08:12:01Z")
		
		def get_location_side_effect(country_code, party_id, location_id):
			if country_code == "BE" and party_id == 'BEK' and location_id == 'LOC1':
				return location
		mock_proxy = self.configure_mock_controller_with_get_location(mock_controller, get_location_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/locations/BE/BEK/LOC1/3256/1', self.PUT(), data=self.as_json(updated_connector))

		mock_proxy.add_or_update_location.assert_called_with('BE', 'BEK', 'LOC1', updated_location)
		json = response.get_json()['data']
		self.assertIsNotNone(Connector(json))


	#PATCH unit tests
	@with_custom_authentication(resolve_test_token_c, None)
	def test_patch_location_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/locations/FR/UNI/LOC1', self.PATCH())
	
	@with_custom_authentication(resolve_test_token_c, resolve_test_token_a_throws)
	def test_patch_evse_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/locations/FR/UNI/LOC1/3256', self.PATCH())

	@with_custom_authentication(resolve_test_token_c, resolve_test_token_a_throws)
	def test_patch_connector_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/locations/FR/UNI/LOC1/3256/1', self.PATCH())

	@without_authentication
	@without_role_autorization
	def test_patch_location_with_missing_last_updated_fields_returns_400(self):
		self.assertHttpCode(400, '/ocpi/emsp/2.2/locations/FR/UNI/LOC1', self.PATCH(), data = {"unit": "test"})
	
	@without_authentication
	@without_role_autorization
	def test_patch_evse_with_missing_last_updated_fields_returns_400(self):
		self.assertHttpCode(400, '/ocpi/emsp/2.2/locations/FR/UNI/LOC1/3256', self.PATCH(), data = {"unit": "test"})

	@without_authentication
	@without_role_autorization
	def test_patch_connector_with_missing_last_updated_fields_returns_400(self):
		self.assertHttpCode(400, '/ocpi/emsp/2.2/locations/FR/UNI/LOC1/3256/1', self.PATCH(), data = {"unit": "test"})


	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_patch_location_updates_location(self, mock_controller):
		location = Location(country_code="BE",party_id="BEK",id="LOC1",type='ON_STREET',address= "F.Rooseveltlaan 3A", city='Gent',country="BEL",coordinates = GeoLocation(latitude= "3.729944",longitude= "51.047599"), last_updated="2015-06-28T08:12:01Z")
		#change the location name
		patch = json.dumps({ 'name': 'Interparking Gent Zuid', 'last_updated':"2018-06-28T08:12:01Z"})
		patched_location = Location(country_code="BE",party_id="BEK",id="LOC1", name='Interparking Gent Zuid', type='ON_STREET',address= "F.Rooseveltlaan 3A", city='Gent',country="BEL",coordinates = GeoLocation(latitude= "3.729944",longitude= "51.047599"), last_updated="2018-06-28T08:12:01Z")		
		
		def get_location_side_effect(country_code, party_id, location_id):
			if country_code == "BE" and party_id == 'BEK' and location_id == 'LOC1':
				return location
		mock_proxy = self.configure_mock_controller_with_get_location(mock_controller, get_location_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/locations/BE/BEK/LOC1', self.PATCH(), data=patch)

		mock_proxy.add_or_update_location.assert_called_with('BE', 'BEK', 'LOC1', patched_location)
		json_response = response.get_json()['data']
		self.assertIsNotNone(Location(json_response))

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_patch_evse_updates_location(self, mock_controller):
		connector = Connector(id= "1",standard= "IEC_62196_T2",format= "CABLE",power_type= "AC_3_PHASE", max_voltage= 220, max_amperage= 16, last_updated="2015-06-28T08:12:01Z")
		evse = EVSE(uid= "3256", status= 'AVAILABLE', connectors = [connector], last_updated="2015-06-28T08:12:01Z")
		location = Location(country_code="BE",party_id="BEK",id="LOC1",evses=[evse], type='ON_STREET',address= "F.Rooseveltlaan 3A", city='Gent',country="BEL",coordinates = GeoLocation(latitude= "3.729944",longitude= "51.047599"), last_updated="2015-06-28T08:12:01Z")
		# a simple status update
		patch = { 'status': 'CHARGING', 'last_updated':"2018-06-28T08:12:01Z"}
		updated_evse = EVSE(uid= "3256", status= 'CHARGING', connectors = [connector], last_updated="2018-06-28T08:12:01Z")
		updated_location = Location(country_code="BE",party_id="BEK",id="LOC1",evses=[updated_evse], type='ON_STREET',address= "F.Rooseveltlaan 3A", city='Gent',country="BEL",coordinates = GeoLocation(latitude= "3.729944",longitude= "51.047599"), last_updated="2018-06-28T08:12:01Z")
		
		def get_location_side_effect(country_code, party_id, location_id):
			if country_code == "BE" and party_id == 'BEK' and location_id == 'LOC1':
				return location
		mock_proxy = self.configure_mock_controller_with_get_location(mock_controller, get_location_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/locations/BE/BEK/LOC1/3256', self.PATCH(), data=self.as_json(patch))

		mock_proxy.add_or_update_location.assert_called_with('BE', 'BEK', 'LOC1', updated_location)
		json = response.get_json()['data']
		self.assertIsNotNone(EVSE(json))

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_patch_connector_updates_location(self, mock_controller):
		connector = Connector(id= "1",standard= "IEC_62196_T2",format= "CABLE",power_type= "AC_3_PHASE", max_voltage= 220, max_amperage= 16, last_updated="2015-06-28T08:12:01Z")
		evse = EVSE(uid= "3256", status= 'AVAILABLE', connectors = [connector], last_updated="2015-06-28T08:12:01Z")
		location = Location(country_code="BE",party_id="BEK",id="LOC1",evses=[evse], type='ON_STREET',address= "F.Rooseveltlaan 3A", city='Gent',country="BEL",coordinates = GeoLocation(latitude= "3.729944",longitude= "51.047599"), last_updated="2015-06-28T08:12:01Z")
		patch = { 'max_amperage': 32, 'last_updated':"2018-06-28T08:12:01Z"}
		updated_connector = Connector(id= "1",standard= "IEC_62196_T2",format= "CABLE",power_type= "AC_3_PHASE", max_voltage= 220, max_amperage= 32, last_updated="2018-06-28T08:12:01Z")
		updated_evse = EVSE(uid= "3256", status= 'AVAILABLE', connectors = [updated_connector], last_updated="2018-06-28T08:12:01Z")
		updated_location = Location(country_code="BE",party_id="BEK",id="LOC1",evses=[updated_evse], type='ON_STREET',address= "F.Rooseveltlaan 3A", city='Gent',country="BEL",coordinates = GeoLocation(latitude= "3.729944",longitude= "51.047599"), last_updated="2018-06-28T08:12:01Z")
		
		def get_location_side_effect(country_code, party_id, location_id):
			if country_code == "BE" and party_id == 'BEK' and location_id == 'LOC1':
				return location
		mock_proxy = self.configure_mock_controller_with_get_location(mock_controller, get_location_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/locations/BE/BEK/LOC1/3256/1', self.PATCH(), data=self.as_json(patch))

		mock_proxy.add_or_update_location.assert_called_with('BE', 'BEK', 'LOC1', updated_location)
		json = response.get_json()['data']
		self.assertIsNotNone(Connector(json))


if __name__ == '__main__':
	unittest.main()
