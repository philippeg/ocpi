import unittest
import unittest.mock as mock

import json

from ocpi_tests.v2_2.test_base import *

from ocpi.v2_2.objects.common import Price
from ocpi.v2_2.objects.cdrs import ChargingPeriod
from ocpi.v2_2.objects.sessions import Session
from ocpi.v2_2.objects.cdrs import CdrToken, CdrDimension
	
class SessionsTests(TestBase):

	def resolve_test_token_a_throws(self, token):
		raise Exception('A very specific bad thing happened.')

	def resolve_test_token_c(self, token):
		return Credentials(token=token, url='test_url', roles=[CredentialsRole(role='CPO', business_details=BusinessDetails(name= 'Unit Tests'), party_id= 'UNI', country_code= 'DE') ])
	
	def create_test_session(self):
		return Session(country_code="BE",party_id="BEK",id="S1",start_date_time="2015-06-28T08:12:01Z", kwh=34.5, cdr_token=CdrToken(uid="T1", type="AD_HOC_USER", contract_id="C1"), auth_method="COMMAND", location_id="LOC1", evse_uid="3542", connector_id="1", currency="EUR", status="COMPLETED", last_updated="2015-06-28T08:12:01Z")
	
	def create_test_session_as_json(self):
		return str(self.create_test_session())

	# GET unit tests
	@without_role_autorization
	def test_get_with_token_c_and_invalid_request_returns_400(self):
		self.assertAuthorizationWithTokenC(400, '/ocpi/emsp/2.2/sessions/BE/BEC/S1', self.GET())

	def test_get_with_token_a_returns_401(self):
		self.assertAuthorizationWithTokenA(401, '/ocpi/emsp/2.2/sessions/BE/BEC/S1', self.GET())

	def test_get_without_token_returns_401(self):
		self.assertAuthorizationWithoutToken(401, '/ocpi/emsp/2.2/sessions/BE/BEC/S1', self.GET())

	@with_custom_authentication(resolve_test_token_c, None)
	def test_get_session_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/sessions/FR/UNI/S1', self.GET())

	def configure_mock_controller_with_get_session(self, mock_controller, get_session_side_effect):
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		mock_proxy.get_session.side_effect = get_session_side_effect
		return mock_proxy

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_session_returns_session_response(self, mock_controller):
		def get_session_side_effect(country_code, party_id, session_id):
			if country_code == "BE" and party_id == 'BEK' and session_id == 'S1':
				return self.create_test_session()

		self.configure_mock_controller_with_get_session(mock_controller, get_session_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/sessions/BE/BEK/S1', self.GET())
			
		json = response.get_json()['data']
		self.assertIsNotNone(Session(json))

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_session_with_invalid_arguments_returns_2001_status_code(self, mock_controller):
		def get_session_side_effect(country_code, party_id, session_id):
			return None
		self.configure_mock_controller_with_get_session(mock_controller, get_session_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/sessions/BE/BEC/S1', self.GET())

		self.assertValidOcpiResponseFormat(response, 2001, 'Session BE/BEC/S1 does not exist in the eMSP system')


	#PUT unit tests
	@with_custom_authentication(resolve_test_token_c, None)
	def test_put_session_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/sessions/FR/UNI/S1', self.PUT())


	@without_authentication
	@without_role_autorization
	def test_put_session_with_missing_last_updated_fields_returns_400(self):
		self.assertHttpCode(400, '/ocpi/emsp/2.2/sessions/FR/UNI/S1', self.PUT(), data = {"unit": "test"})
	
	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_put_session_updates_location(self, mock_controller):
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		session = self.create_test_session()
		
		response = self.performRequest('/ocpi/emsp/2.2/sessions/BE/BEK/S1', self.PUT(), data=json.dumps(session))

		mock_proxy.add_or_update_session.assert_called_with('BE', 'BEK', 'S1', session)
		json_response = response.get_json()['data']
		self.assertIsNotNone(Session(json_response))


	#PATCH unit tests
	@with_custom_authentication(resolve_test_token_c, None)
	def test_patch_session_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/sessions/FR/UNI/S1', self.PATCH())
	
	@without_authentication
	@without_role_autorization
	def test_patch_session_with_missing_last_updated_fields_returns_400(self):
		self.assertHttpCode(400, '/ocpi/emsp/2.2/sessions/FR/UNI/S1', self.PATCH(), data = {"unit": "test"})
	
	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_patch_session_updates_total_cost(self, mock_controller):
		#update the total cost
		patch = json.dumps({ "total_cost": {"excl_vat": 0.60,"incl_vat": 0.66},"last_updated": "2019-06-23T08:11:00Z"})
		patched_session = self.create_test_session()
		patched_session.total_cost = Price(excl_vat=0.60, incl_vat=0.66)
		patched_session.last_updated = "2019-06-23T08:11:00Z"
		
		def get_session_side_effect(country_code, party_id, session_id):
			if country_code == "BE" and party_id == 'BEK' and session_id == 'S1':
				return self.create_test_session()
		mock_proxy = self.configure_mock_controller_with_get_session(mock_controller, get_session_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/sessions/BE/BEK/S1', self.PATCH(), data=patch)

		mock_proxy.add_or_update_session.assert_called_with('BE', 'BEK', 'S1', patched_session)
		json_response = response.get_json()['data']
		self.assertIsNotNone(Session(json_response))

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_patch_session_adds_new_charging_period(self, mock_controller):
		#update the total cost
		patch = json.dumps({ "kwh": 15.00, "charging_periods": [{"start_date_time": "2019-06-23T08:16:02Z","dimensions": [{"type": "ENERGY","volume": 2200}]}],"total_cost": {"excl_vat": 0.80,"incl_vat": 0.88},"last_updated": "2019-06-23T08:16:02Z"})
		patched_session = self.create_test_session()
		patched_session.kwh = 15.00
		patched_session.charging_periods = [ChargingPeriod(start_date_time = "2019-06-23T08:16:02Z", dimensions= [CdrDimension(type="ENERGY",volume= 2200)])]
		patched_session.total_cost = Price(excl_vat=0.80, incl_vat=0.88)
		patched_session.last_updated = "2019-06-23T08:16:02Z"
		
		def get_session_side_effect(country_code, party_id, session_id):
			if country_code == "BE" and party_id == 'BEK' and session_id == 'S1':
				return self.create_test_session()
		mock_proxy = self.configure_mock_controller_with_get_session(mock_controller, get_session_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/sessions/BE/BEK/S1', self.PATCH(), data=patch)

		mock_proxy.add_or_update_session.assert_called_with('BE', 'BEK', 'S1', patched_session)
		json_response = response.get_json()['data']
		self.assertIsNotNone(Session(json_response))


if __name__ == '__main__':
	unittest.main()