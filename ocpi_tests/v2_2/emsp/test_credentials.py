import unittest
import unittest.mock as mock

from ocpi_tests.v2_2.test_base import TestBase, without_authentication

from flask import json, jsonify

from ocpi.config import *
from ocpi.v2_2.objects.credentials import Credentials
from ocpi.v2_2.objects.versions import Versions, Version, Endpoints, Endpoint
from ocpi.v2_2.core.exceptions import RequestException, ClientResponseException

from ocpi.v2_2.emsp.credentials import get_client_versions_and_endpoints_for_2_2, create_credentials_with_new_token

def returns_unit_test_versions(url, headers):
	return jsonify([{'version':'2.2', 'url': 'unit_test.ut/2.2'}])


class CredentialsTests(TestBase):
	# GET
	def test_get_with_token_a_returns_200(self):
		self.assertAuthorizationWithTokenA(200, '/ocpi/emsp/2.2/credentials', self.GET())

	def test_get_with_token_c_returns_200(self):
		self.assertAuthorizationWithTokenC(200, '/ocpi/emsp/2.2/credentials', self.GET())

	def test_get_without_token_returns_401(self):
		self.assertAuthorizationWithoutToken(401, '/ocpi/emsp/2.2/credentials', self.GET())

	@without_authentication
	def test_get_returns_valid_response_format(self):
		response = self.performRequest('/ocpi/emsp/2.2/credentials', self.GET())
		self.assertValidOcpiResponseFormat(response, 1000, "Success")

	@without_authentication
	def test_get_returns_json_response_data(self):
		response = self.performRequest('/ocpi/emsp/2.2/credentials', self.GET())
		self.assertJsonResponseDataIsNotNone(response)

	@without_authentication
	def test_get_credentials_response(self):
		response = self.performRequest('/ocpi/emsp/2.2/credentials', self.GET(), headers= {'Authorization': 'abc123'})
		data = response.get_json()['data']
		
		self.assertJsonResponseIsValidInstanceOf(Credentials, data)
		credentials = Credentials(data)
		
		self.assertEqual("abc123", credentials.token)
		self.assertEqual(credentials.roles,[SERVER_CREDENTIALS_ROLE])
		url_response = self.performRequest(credentials.url, self.GET())
		self.assertEqual(url_response.status_code, 200)

	

	# Helpers
	def test_get_client_versions_and_endpoints_for_2_2_invalid_client_versions_url_raises_RequestException(self):
		with self.assertRaises(RequestException) as e:
			get_client_versions_and_endpoints_for_2_2('invalid_url', None)
		self.assertIn('Invalid URL', str(e.exception))

	def test_get_client_versions_and_endpoints_for_2_2_missing_version_raises_ClientResponseException(self):
		def get_client_side_effect(url, response_type, headers = None):
			if response_type == Versions:
				return Versions(versions=[Version(version='2.0', url='unit_tests.com')])
		with mock.patch("ocpi.v2_2.emsp.credentials.client_get", side_effect=get_client_side_effect):
			with self.assertRaises(ClientResponseException) as e:
				get_client_versions_and_endpoints_for_2_2('unit_test.com', None)
			self.assertIn('minimal expected implementation version (2.2) is not met', str(e.exception))	

	def test_get_client_versions_and_endpoints_for_2_2_exception_when_getting_versions_raises(self):
		def get_client_side_effect(url, response_type, headers = None):
			if response_type == Versions:
				raise RequestException()
		with mock.patch("ocpi.v2_2.emsp.credentials.client_get", side_effect=get_client_side_effect):
			with self.assertRaises(RequestException):
				get_client_versions_and_endpoints_for_2_2('unit_test.com', None)

	def test_get_client_versions_and_endpoints_for_2_2_exception_when_getting_versions_raises_RequestException(self):
		def get_client_side_effect(url, response_type, headers = None):
			if response_type == Versions:
				return Versions(versions=[Version(version='2.2', url='unit_test.com')])
			if response_type == Endpoints:
				raise RequestException()
		with mock.patch("ocpi.v2_2.emsp.credentials.client_get", side_effect=get_client_side_effect):
			with self.assertRaises(RequestException):
				get_client_versions_and_endpoints_for_2_2('unit_test.com', None)

	
	def test_get_client_versions_and_endpoints_for_2_2_missing_endpoints_raises_ClientResponseException(self):
		def get_client_side_effect(url, response_type, headers = None):
			if response_type == Versions:
				return Versions(versions=[Version(version='2.2', url='unit_test.com')])
			if response_type == Endpoints:
				return None
		with mock.patch("ocpi.v2_2.emsp.credentials.client_get", side_effect=get_client_side_effect):
			with self.assertRaises(ClientResponseException) as e:
				get_client_versions_and_endpoints_for_2_2('unit_test.com', None)
			self.assertIn('missing or invalid reponse for client available endpoints for v2.2', str(e.exception))	

	def test_get_client_versions_and_endpoints_for_2_2_returns_expected_results(self):
		expected_versions = Versions(versions=[Version(version='2.2', url='unit_test.com')])
		expected_endpoints = Endpoints(version='2.2', endpoints = [Endpoint(identifier= "credentials", role= "CPO", url= "https://example.com/ocpi/cpo/2.2/credentials/")]) 
		def get_client_side_effect(url, response_type, headers = None):
			if response_type == Versions:
				return expected_versions
			if response_type == Endpoints:
				return expected_endpoints
		with mock.patch("ocpi.v2_2.emsp.credentials.client_get", side_effect=get_client_side_effect):
			version, endpoints = get_client_versions_and_endpoints_for_2_2('unit_test.com', None)
			self.assertEqual(version, expected_versions[0])
			self.assertEqual(endpoints, expected_endpoints)

	def test_create_credentials_with_new_token_returns_expected_results(self):
		with self.app_context():
			credentials = create_credentials_with_new_token()
			self.assertIsNotNone(credentials)
			self.assertEqual('http://localhost/ocpi/versions', credentials.url)
			self.assertEqual(36, len(credentials.token))

	def credentials_object(self):
		credentials = {'token': 'ebf3b399-779f-4497-9b9d-ac6ad3cc44d2', 'url': 'http://localhost/ocpi/versions', 'roles':[SERVER_CREDENTIALS_ROLE]} 
		return Credentials(credentials)

	def credentials_as_json(self):
		return json.dumps(self.credentials_object())

	def mock_is_registered(self, mock_controller, return_value):
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		mock_proxy.is_registered.return_value = return_value
		return mock_proxy

	# POST
	def test_post_with_token_a_and_invalid_request_returns_400(self):
		self.assertAuthorizationWithTokenA(400, '/ocpi/emsp/2.2/credentials', self.POST(), data=self.credentials_as_json())

	def test_post_with_token_c_and_invalid_request_returns_400(self):
		self.assertAuthorizationWithTokenC(400, '/ocpi/emsp/2.2/credentials', self.POST(), data=self.credentials_as_json())

	def test_post_without_token_returns_401(self):
		self.assertAuthorizationWithoutToken(401, '/ocpi/emsp/2.2/credentials', self.POST())

	@without_authentication
	def test_post_credentials_with_invalid_json(self):
		for data in [None, '{']:
			response = self.performRequest('/ocpi/emsp/2.2/credentials', self.POST(), data=data)		
			self.assertEqual(response.status_code, 400)

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_post_credentials_with_invalid_credentials_request_object(self, mock_controller):
		self.mock_is_registered(mock_controller, False)
		response = self.performRequest('/ocpi/emsp/2.2/credentials', self.POST(), data=json.dumps({'data':None}))		
		self.assertEqual(response.json['status_code'], 2001)
		
	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_post_credentials_missing_mutual_version_returns_3003_response(self, mock_controller):
		self.mock_is_registered(mock_controller, False)
		with mock.patch("ocpi.v2_2.emsp.credentials.client_get", return_value=[Version({'version':'2.1.1', 'url': 'unit_test.ut/2.1'})]):
			auth_header = {'Authorization': None}
			response = self.performRequest('/ocpi/emsp/2.2/credentials', self.POST(), data=self.credentials_as_json(), headers=auth_header)
			self.assertValidOcpiResponseFormat(response, 3003, 'minimal expected implementation version (2.2) is not met')
		
	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_post_credentials_missing_available_endpoints_returns_3003_response(self, mock_controller):
		self.mock_is_registered(mock_controller, False)

		def get_client_with_missing_available_endpoints(url, response_type, headers = None):
			if response_type == Versions:
				return [Version({'version':'2.2', 'url': 'unit_test.ut/2.1'})]
			if response_type == Endpoints:
				return {}

		with mock.patch("ocpi.v2_2.emsp.credentials.client_get", side_effect=get_client_with_missing_available_endpoints):
			auth_header = {'Authorization': None}
			response = self.performRequest('/ocpi/emsp/2.2/credentials', self.POST(), data=self.credentials_as_json(), headers=auth_header)
			self.assertValidOcpiResponseFormat(response, 3003, 'missing or invalid reponse for client available endpoints for v2.2')


	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_post_credentials_already_registered_client_returns_405_http_error(self, mock_controller):
		self.mock_is_registered(mock_controller, True)
		
		auth_header = {'Authorization': None}
		response = self.performRequest('/ocpi/emsp/2.2/credentials', self.POST(), data=self.credentials_as_json(), headers= auth_header)
		self.assertEqual(405, response.status_code)

	@staticmethod
	def get_client_with_missing_available_endpoints(url, response_type, headers = None):
		if response_type == Versions:
			return [Version({'version':'2.2', 'url': 'unit_test.ut/2.1'})]
		if response_type == Endpoints:
			return Endpoints(version="2.2",
			endpoints=[
				Endpoint(identifier= "credentials", role= "CPO", url= "https://example.com/ocpi/cpo/2.2/credentials/"),
				Endpoint(identifier= "locations", role= "CPO", url= "https://example.com/ocpi/cpo/2.2/locations/")])

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_post_credentials_exception_when_generating_new_token_returns_server_error_response(self, mock_controller):
		self.mock_is_registered(mock_controller, False)

		def doRaiseException():
			raise Exception('could not generate token')

		with mock.patch("ocpi.v2_2.emsp.credentials.client_get", side_effect=CredentialsTests.get_client_with_missing_available_endpoints):
			with mock.patch("ocpi.v2_2.emsp.credentials.create_credentials_with_new_token", side_effect=doRaiseException):
				response = self.performRequest('/ocpi/emsp/2.2/credentials', self.POST(), data=self.credentials_as_json(), headers=self.AuthorizationToken())
				self.assertValidOcpiResponseFormat(response, 3000, 'could not generate token')



	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_post_credentials_with_valid_available_endpoints_registers_client(self, mock_controller):
		mock_proxy = self.mock_is_registered(mock_controller, False)
		expected_version = CredentialsTests.get_client_with_missing_available_endpoints(None, Versions)[0]
		expected_endpoints = CredentialsTests.get_client_with_missing_available_endpoints(None, Endpoints)
		expected_credentials = self.credentials_object()
		expected_credentials['token'] = 'hello'

		with mock.patch("ocpi.v2_2.emsp.credentials.client_get", side_effect=CredentialsTests.get_client_with_missing_available_endpoints):
			with mock.patch("ocpi.v2_2.emsp.credentials.create_credentials_with_new_token", return_value=Credentials(token='hello', url='hi', roles=[SERVER_CREDENTIALS_ROLE])):
				self.performRequest('/ocpi/emsp/2.2/credentials', self.POST(), data=self.credentials_as_json(), headers=self.AuthorizationToken())
				mock_proxy.register_client.assert_called_with(expected_credentials, expected_version, expected_endpoints)
			

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_post_credentials_returns_expected_response(self, mock_controller):
		self.mock_is_registered(mock_controller, False)
		with mock.patch("ocpi.v2_2.emsp.credentials.client_get", side_effect=CredentialsTests.get_client_with_missing_available_endpoints):
			request_object = self.credentials_object()
			response = self.performRequest('/ocpi/emsp/2.2/credentials', self.POST(), data=self.credentials_as_json(), headers=self.AuthorizationToken())
			credentials = response.get_json()['data']

			self.assertEqual(36, len(credentials['token']))
			self.assertEqual(credentials['url'], request_object['url'])
			self.assertEqual(credentials['roles'], request_object['roles'])


	# PUT
	def test_put_with_token_a_returns_401(self):
		self.assertAuthorizationWithTokenA(401, '/ocpi/emsp/2.2/credentials', self.PUT(), data=self.credentials_as_json())

	def test_put_with_token_c_and_invalid_request_returns_400(self):
		self.assertAuthorizationWithTokenC(400, '/ocpi/emsp/2.2/credentials', self.PUT(), data=self.credentials_as_json())

	def test_put_without_token_returns_401(self):
		self.assertAuthorizationWithoutToken(401, '/ocpi/emsp/2.2/credentials', self.PUT())

	@without_authentication
	def test_put_credentials_with_invalid_json(self):
		for data in [None, '{']:
			response = self.performRequest('/ocpi/emsp/2.2/credentials', self.PUT(), data=data)		
			self.assertEqual(response.status_code, 400)

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_put_credentials_with_invalid_credentials_request_object(self, mock_controller):
		self.mock_is_registered(mock_controller, True)
		response = self.performRequest('/ocpi/emsp/2.2/credentials', self.PUT(), data=json.dumps({'data':None}))		
		self.assertEqual(response.json['status_code'], 2001)
	
	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_put_credentials_missing_mutual_version_returns_3003_response(self, mock_controller):
		self.mock_is_registered(mock_controller, True)
		with mock.patch("ocpi.v2_2.emsp.credentials.client_get", return_value=[Version({'version':'2.1.1', 'url': 'unit_test.ut/2.1'})]):
			response = self.performRequest('/ocpi/emsp/2.2/credentials', self.PUT(), data=self.credentials_as_json(), headers=self.AuthorizationToken())
			self.assertValidOcpiResponseFormat(response, 3003, 'minimal expected implementation version (2.2) is not met')
		
	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_put_credentials_missing_available_endpoints_returns_3003_response(self, mock_controller):
		self.mock_is_registered(mock_controller, True)
		def get_client_with_missing_available_endpoints(url, response_type, headers = None):
			if response_type == Versions:
				return [Version({'version':'2.2', 'url': 'unit_test.ut/2.1'})]
			if response_type == Endpoints:
				return {}

		with mock.patch("ocpi.v2_2.emsp.credentials.client_get", side_effect=get_client_with_missing_available_endpoints):
			response = self.performRequest('/ocpi/emsp/2.2/credentials', self.PUT(), data=self.credentials_as_json(), headers=self.AuthorizationToken())
			self.assertValidOcpiResponseFormat(response, 3003, 'missing or invalid reponse for client available endpoints for v2.2')


	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_put_credentials_with_not_yet_registered_client_returns_405_http_error(self, mock_controller):
		self.mock_is_registered(mock_controller, False)
		response = self.performRequest('/ocpi/emsp/2.2/credentials', self.PUT(), data=self.credentials_as_json(), headers=self.AuthorizationToken())
		self.assertEqual(405, response.status_code)

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')	
	@without_authentication
	def test_put_credentials_exception_when_generating_new_token_returns_server_error_response(self, mock_controller):
		self.mock_is_registered(mock_controller, True)
		def doRaiseException():
			raise Exception('could not generate token')

		with mock.patch("ocpi.v2_2.emsp.credentials.client_get", side_effect=CredentialsTests.get_client_with_missing_available_endpoints):
			with mock.patch("ocpi.v2_2.emsp.credentials.create_credentials_with_new_token", side_effect=doRaiseException):
				response = self.performRequest('/ocpi/emsp/2.2/credentials', self.PUT(), data=self.credentials_as_json(), headers=self.AuthorizationToken())
				self.assertValidOcpiResponseFormat(response, 3000, 'could not generate token')



	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_put_credentials_with_valid_available_endpoints_registers_client(self, mock_controller):
		mock_proxy = self.mock_is_registered(mock_controller, True)
		expected_version = CredentialsTests.get_client_with_missing_available_endpoints(None, Versions)[0]
		expected_endpoints = CredentialsTests.get_client_with_missing_available_endpoints(None, Endpoints)
		expected_credentials = self.credentials_object()
		expected_credentials['token'] = 'hello'

		with mock.patch("ocpi.v2_2.emsp.credentials.client_get", side_effect=CredentialsTests.get_client_with_missing_available_endpoints):
			with mock.patch("ocpi.v2_2.emsp.credentials.create_credentials", return_value=Credentials(token='hello', url='hi', roles=[SERVER_CREDENTIALS_ROLE])):
				self.performRequest('/ocpi/emsp/2.2/credentials', self.PUT(), data=self.credentials_as_json(), headers=self.AuthorizationToken())
				mock_proxy.register_client.assert_called_with(expected_credentials, expected_version, expected_endpoints)
			

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_put_credentials_returns_expected_response(self, mock_controller):
		self.mock_is_registered(mock_controller, True)
		with mock.patch("ocpi.v2_2.emsp.credentials.client_get", side_effect=CredentialsTests.get_client_with_missing_available_endpoints):
			request_object = self.credentials_object()
			response = self.performRequest('/ocpi/emsp/2.2/credentials', self.PUT(), data=self.credentials_as_json(), headers=self.AuthorizationToken())
			credentials = response.get_json()['data']

			self.assertEqual(36, len(credentials['token']))
			self.assertEqual(credentials['url'], request_object['url'])
			self.assertEqual(credentials['roles'], request_object['roles'])

	# DELETE
	def test_delete_with_token_a_returns_401(self):
		self.assertAuthorizationWithTokenA(401, '/ocpi/emsp/2.2/credentials', self.DELETE(), data=self.credentials_as_json())

	def test_delete_with_token_c_and_invalid_request_returns_400(self):
		self.assertAuthorizationWithTokenC(400, '/ocpi/emsp/2.2/credentials', self.DELETE(), data=self.credentials_as_json())

	def test_delete_without_token_returns_401(self):
		self.assertAuthorizationWithoutToken(401, '/ocpi/emsp/2.2/credentials', self.DELETE())

	@without_authentication
	def test_delete_credentials_with_invalid_json(self):
		for data in [None, '{']:
			response = self.performRequest('/ocpi/emsp/2.2/credentials', self.DELETE(), data=data)		
			self.assertEqual(response.status_code, 400)

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_delete_credentials_with_invalid_credentials_request_object(self, mock_controller):
		self.mock_is_registered(mock_controller, True)
		response = self.performRequest('/ocpi/emsp/2.2/credentials', self.DELETE(), data=json.dumps({'data':None}))		
		self.assertEqual(response.json['status_code'], 2001)

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_delete_credentials_with_not_yet_registered_client_returns_405_http_error(self, mock_controller):
		self.mock_is_registered(mock_controller, False)
		response = self.performRequest('/ocpi/emsp/2.2/credentials', self.DELETE(), data=self.credentials_as_json(), headers=self.AuthorizationToken())
		self.assertEqual(405, response.status_code)

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_delete_credentials_with_valid_credentials_unregisters_client(self, mock_controller):
		mock_proxy = self.mock_is_registered(mock_controller, True)
		expected_argument = self.credentials_object()
		self.performRequest('/ocpi/emsp/2.2/credentials', self.DELETE(), data=self.credentials_as_json(), headers=self.AuthorizationToken())
		mock_proxy.unregister_client.assert_called_with(expected_argument)
			

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_delete_credentials_returns_expected_response(self, mock_controller):
		self.mock_is_registered(mock_controller, True)
		response = self.performRequest('/ocpi/emsp/2.2/credentials', self.DELETE(), data=self.credentials_as_json(), headers=self.AuthorizationToken())
		self.assertValidOcpiResponseFormat(response, 1000, "client successful unregistered")
		
				
if __name__ == '__main__':
	unittest.main()
