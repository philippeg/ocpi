import unittest
import unittest.mock as mock

import json

from ocpi_tests.v2_2.test_base import *

from ocpi.v2_2.objects.tariffs import Tariff, TariffElement, PriceComponent

	
class TariffsTests(TestBase):

	def resolve_test_token_a_throws(self, token):
		raise Exception('A very specific bad thing happened.')

	def resolve_test_token_c(self, token):
		return Credentials(token=token, url='test_url', roles=[CredentialsRole(role='CPO', business_details=BusinessDetails(name= 'Unit Tests'), party_id= 'UNI', country_code= 'DE') ])
	
	def create_test_tariff(self):
		price_component = PriceComponent(type="ENERGY",price= 1.1,vat= 0.1,step_size= 2)
		tariff_element = TariffElement(price_components=[price_component])
		return Tariff(country_code="BE",party_id="BEK",id="T1", currency="EUR", elements= [tariff_element], last_updated="2015-06-28T08:12:01Z")

	# GET unit tests
	@without_role_autorization
	def test_get_with_token_c_and_invalid_request_returns_400(self):
		self.assertAuthorizationWithTokenC(400, '/ocpi/emsp/2.2/tariffs/BE/BEC/T1', self.GET())

	def test_get_with_token_a_returns_401(self):
		self.assertAuthorizationWithTokenA(401, '/ocpi/emsp/2.2/tariffs/BE/BEC/T1', self.GET())

	def test_get_without_token_returns_401(self):
		self.assertAuthorizationWithoutToken(401, '/ocpi/emsp/2.2/tariffs/BE/BEC/T1', self.GET())

	@with_custom_authentication(resolve_test_token_c, None)
	def test_get_tariff_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/tariffs/FR/UNI/T1', self.GET())

	def configure_mock_controller_with_get_tariff(self, mock_controller, get_tariff_side_effect):
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		mock_proxy.get_tariff.side_effect = get_tariff_side_effect
		return mock_proxy

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_tariff_returns_tariff_response(self, mock_controller):
		def get_tariff_side_effect(country_code, party_id, tariff_id):
			if country_code == "BE" and party_id == 'BEK' and tariff_id == 'T1':
				return self.create_test_tariff()

		self.configure_mock_controller_with_get_tariff(mock_controller, get_tariff_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/tariffs/BE/BEK/T1', self.GET())
			
		json = response.get_json()['data']
		self.assertIsNotNone(Tariff(json))

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_tariff_with_invalid_arguments_returns_2001_status_code(self, mock_controller):
		def get_tariff_side_effect(country_code, party_id, tariff_id):
			return None
		self.configure_mock_controller_with_get_tariff(mock_controller, get_tariff_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/tariffs/BE/BEC/T1', self.GET())

		self.assertValidOcpiResponseFormat(response, 2001, 'Tariff BE/BEC/T1 does not exist in the eMSP system')


	#PUT unit tests
	@with_custom_authentication(resolve_test_token_c, None)
	def test_put_tariff_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/tariffs/FR/UNI/T1', self.PUT())


	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_put_tariff_updates_tariff(self, mock_controller):
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		tariff = self.create_test_tariff()
		
		response = self.performRequest('/ocpi/emsp/2.2/tariffs/BE/BEK/T1', self.PUT(), data=json.dumps(tariff))

		mock_proxy.add_or_update_tariff.assert_called_with('BE', 'BEK', 'T1', tariff)
		json_response = response.get_json()['data']
		self.assertIsNotNone(Tariff(json_response))
		self.assertEqual(response.status_code, 200)


	#DELETE unit tests
	@with_custom_authentication(resolve_test_token_c, None)
	def test_delete_tariff_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/tariffs/FR/UNI/T1', self.DELETE())
	

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_delete_with_existing_tariff_deletes_object(self, mock_controller):
		def delete_tariff_side_effect(country_code, party_id, tariff_id):
			return True
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		mock_proxy.delete_tariff.side_effect = delete_tariff_side_effect

		response = self.performRequest('/ocpi/emsp/2.2/tariffs/BE/BEK/T1', self.DELETE())

		mock_proxy.delete_tariff.assert_called_with('BE', 'BEK', 'T1')
		self.assertEqual(response.status_code, 200)
		
	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_delete_with_invalid_tariff_returns_400_status_code(self, mock_controller):
		def delete_tariff_side_effect(country_code, party_id, tariff_id):
			return False
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		mock_proxy.delete_tariff.side_effect = delete_tariff_side_effect

		response = self.performRequest('/ocpi/emsp/2.2/tariffs/BE/BEK/T1', self.DELETE())

		mock_proxy.delete_tariff.assert_called_with('BE', 'BEK', 'T1')
		self.assertEqual(response.status_code, 400)

if __name__ == '__main__':
	unittest.main()