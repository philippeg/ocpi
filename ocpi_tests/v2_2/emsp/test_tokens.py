import unittest
import unittest.mock as mock

import json

from ocpi_tests.v2_2.test_base import *

from ocpi.v2_2.objects.tokens import Token, EnergyContract

	
class TokensTests(TestBase):

	def resolve_test_token_a_throws(self, token):
		raise Exception('A very specific bad thing happened.')

	def resolve_test_token_c(self, token):
		return Credentials(token=token, url='test_url', roles=[CredentialsRole(role='CPO', business_details=BusinessDetails(name= 'Unit Tests'), party_id= 'UNI', country_code= 'DE') ])
	
	def create_test_token(self):
		energy_contract = EnergyContract (supplier_name= "you",contract_id= "123abc")
		return Token(country_code="BE",party_id="BEK",uid= "token1",type= 'RFID',contract_id= 'contract1',visual_number= '1234567890',issuer= 'me', group_id= '123abc',valid= True, whitelist='ALWAYS',language= 'en',default_profile_type= 'FAST',energy_contract= energy_contract,last_updated= "2017-03-16T10:10:02Z")


	# GET unit tests
	@without_role_autorization
	def test_get_with_token_c_returns_200(self):
		self.assertAuthorizationWithTokenC(200, '/ocpi/emsp/2.2/tokens', self.GET())

	def test_get_with_token_a_returns_401(self):
		self.assertAuthorizationWithTokenA(401, '/ocpi/emsp/2.2/tokens', self.GET())

	def test_get_without_token_returns_401(self):
		self.assertAuthorizationWithoutToken(401, '/ocpi/emsp/2.2/tokens', self.GET())

	@with_custom_authentication(resolve_test_token_c, None)
	def test_get_tokens_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/tokens', self.GET())

	def configure_mock_controller_with_get_tokens(self, mock_controller, get_tokens_side_effect):
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		mock_proxy.get_tokens.side_effect = get_tokens_side_effect
		return mock_proxy

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_tokens_returns_tokens_response(self, mock_controller):
		def get_tokens_side_effect(date_from, date_to):
			return [self.create_test_token()] * 2
		self.configure_mock_controller_with_get_tokens(mock_controller, get_tokens_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/tokens', self.GET())
			
		json = response.get_json()['data']
		self.assertEqual("2", response.headers['X-Total-Count'])
		self.assertEqual("100", response.headers['X-Limit'])
		self.assertFalse('Link' in response.headers)
		self.assertEqual(2, len(json))
		for t in json:
			self.assertIsNotNone(Token(t))

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_tokens_returns_paginated_tokens_response(self, mock_controller):
		def get_tokens_side_effect(date_from, date_to):
			return [self.create_test_token()] * 2
		self.configure_mock_controller_with_get_tokens(mock_controller, get_tokens_side_effect)
		url = self.get_url_for('ocpi.get_tokens')

		response = self.performRequest('/ocpi/emsp/2.2/tokens?limit=1', self.GET())
			
		json = response.get_json()['data']
		self.assertEqual("2", response.headers['X-Total-Count'])
		self.assertEqual("100", response.headers['X-Limit'])
		self.assertEqual('<'+ url + "?limit=1&offset=1" + '>; rel=\"next\"', response.headers['Link'])
		self.assertEqual(1, len(json))
		for t in json:
			self.assertIsNotNone(Token(t))

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_tokens_with_additional_parameters_returns_paginated_tokens_response(self, mock_controller):
		def get_tokens_side_effect(date_from, date_to):
			return [self.create_test_token()] * 2
		self.configure_mock_controller_with_get_tokens(mock_controller, get_tokens_side_effect)
		url = self.get_url_for('ocpi.get_tokens')

		response = self.performRequest('/ocpi/emsp/2.2/tokens?limit=1&date_from=2016-01-01T00:00:00Z&date_to=2016-12-31T23:59:59Z', self.GET())
			
		self.assertEqual("2", response.headers['X-Total-Count'])
		self.assertEqual("100", response.headers['X-Limit'])
		self.assertEqual('<'+ url + "?limit=1&date_from=2016-01-01T00:00:00Z&date_to=2016-12-31T23:59:59Z&offset=1" + '>; rel=\"next\"', response.headers['Link'])
	
	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_tokens_with_offset_parameters_returns_paginated_tokens_response(self, mock_controller):
		def get_tokens_side_effect(date_from, date_to):
			return [self.create_test_token()] * 3
		self.configure_mock_controller_with_get_tokens(mock_controller, get_tokens_side_effect)
		url = self.get_url_for('ocpi.get_tokens')

		response = self.performRequest('/ocpi/emsp/2.2/tokens?offset=1&limit=1&date_from=2016-01-01T00:00:00Z', self.GET())
			
		self.assertEqual("3", response.headers['X-Total-Count'])
		self.assertEqual("100", response.headers['X-Limit'])
		self.assertEqual('<'+ url + "?limit=1&date_from=2016-01-01T00:00:00Z&offset=2" + '>; rel=\"next\"', response.headers['Link'])
	


	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_tokens_calls_data_store_with_expected_arguments(self, mock_controller):
		def get_tokens_side_effect(date_from, date_to):
			return [self.create_test_token(), self.create_test_token()]
		mock_proxy = self.configure_mock_controller_with_get_tokens(mock_controller, get_tokens_side_effect)

		self.performRequest('/ocpi/emsp/2.2/tokens?date_from=2016-01-01T00:00:00Z&date_to=2016-12-31T23:59:59Z', self.GET())
			
		mock_proxy.get_tokens.assert_called_with('2016-01-01T00:00:00Z', '2016-12-31T23:59:59Z')




if __name__ == '__main__':
	unittest.main()