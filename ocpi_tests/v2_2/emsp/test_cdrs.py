import unittest
import unittest.mock as mock

import json

from ocpi_tests.v2_2.test_base import *

from ocpi.v2_2.objects.common import GeoLocation, Price
from ocpi.v2_2.objects.cdrs import ChargingPeriod
from ocpi.v2_2.objects.cdrs import CDR, CdrToken, CdrLocation, CdrDimension

	
class CdrsTests(TestBase):

	def resolve_test_token_a_throws(self, token):
		raise Exception('A very specific bad thing happened.')

	def resolve_test_token_c(self, token):
		return Credentials(token=token, url='test_url', roles=[CredentialsRole(role='CPO', business_details=BusinessDetails(name= 'Unit Tests'), party_id= 'UNI', country_code= 'DE') ])
	
	def create_test_cdr(self):
		cdr_token = CdrToken(uid="T1", type="AD_HOC_USER", contract_id="C1")
		cdr_location = CdrLocation(id="cdr_loc1",name= "Gent Zuid",address= "F.Rooseveltlaan 3A",city= "Gent",postal_code= "9000",country= "BEL", coordinates= GeoLocation(latitude= "3.729944",longitude= "51.047599"),evse_uid= "1",evse_id= "e1",connector_id= "1",connector_standard= "CHADEMO",connector_format= "SOCKET",connector_power_type= "DC")
		charging_period = ChargingPeriod(start_date_time= "2018-12-25T05:00:00Z",dimensions= [CdrDimension(type='ENERGY', volume=34.5)],tariff_id= 'tariff_id')
		price = Price(excl_vat= 98.51,incl_vat= 102.54)
		return CDR(country_code="BE",party_id="BEK",id="C1",start_date_time="2015-06-28T08:12:01Z", end_date_time="2015-06-28T08:22:04Z", cdr_token=cdr_token, auth_method="COMMAND", cdr_location=cdr_location, currency="EUR", charging_periods=[charging_period], total_cost=price, total_energy=100.5, total_time=30, last_updated="2015-06-28T08:12:01Z")
	
	def create_test_cdr_as_json(self):
		return str(self.create_test_cdr())

	# GET unit tests
	@without_role_autorization
	def test_get_with_token_c_and_invalid_request_returns_400(self):
		self.assertAuthorizationWithTokenC(400, '/ocpi/emsp/2.2/cdrs/BE/BEC/C1', self.GET())

	def test_get_with_token_a_returns_401(self):
		self.assertAuthorizationWithTokenA(401, '/ocpi/emsp/2.2/cdrs/BE/BEC/C1', self.GET())

	def test_get_without_token_returns_401(self):
		self.assertAuthorizationWithoutToken(401, '/ocpi/emsp/2.2/cdrs/BE/BEC/C1', self.GET())

	@with_custom_authentication(resolve_test_token_c, None)
	def test_get_cdr_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/cdrs/FR/UNI/S1', self.GET())

	def configure_mock_controller_with_get_cdr(self, mock_controller, get_cdr_side_effect):
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		mock_proxy.get_cdr.side_effect = get_cdr_side_effect
		return mock_proxy

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_cdr_returns_cdr_response(self, mock_controller):
		def get_cdr_side_effect(country_code, party_id, cdr_id):
			if country_code == "BE" and party_id == 'BEK' and cdr_id == 'C1':
				return self.create_test_cdr()

		self.configure_mock_controller_with_get_cdr(mock_controller, get_cdr_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/cdrs/BE/BEK/C1', self.GET())
			
		json = response.get_json()['data']
		self.assertIsNotNone(CDR(json))

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_cdr_with_invalid_arguments_returns_2001_status_code(self, mock_controller):
		def get_cdr_side_effect(country_code, party_id, cdr_id):
			return None
		self.configure_mock_controller_with_get_cdr(mock_controller, get_cdr_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/cdrs/BE/BEC/C1', self.GET())

		self.assertValidOcpiResponseFormat(response, 2001, 'CDR BE/BEC/C1 does not exist in the eMSP system')


	#POST unit tests
	@with_custom_authentication(resolve_test_token_c, None)
	def test_post_cdr_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/cdrs/FR/UNI', self.POST())

	@without_authentication
	@without_role_autorization
	def test_post_cdr_with_invalid_cdr_request_returns_2001_status_code(self):
		
		response = self.performRequest('/ocpi/emsp/2.2/cdrs/BE/BEK', self.POST(), data=json.dumps({'unit':'test'}))
		
		self.assertValidOcpiResponseFormat(response, 2001, 'Invalid or missing parameters')


	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_post_cdr_adds_new_cdr(self, mock_controller):
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		cdr = self.create_test_cdr()
		url = self.get_url_for('ocpi.get_cdr', country_code='BE', party_id='BEK',cdr_id='C1') #.split('/C',1)[0]
		response = self.performRequest('/ocpi/emsp/2.2/cdrs/BE/BEK', self.POST(), data=json.dumps(cdr))

		mock_proxy.add_cdr.assert_called_with('BE', 'BEK', cdr)
		json_response = response.get_json()['data']

		self.assertTrue('location' in json_response)
		self.assertEqual(url, json_response['location'])


if __name__ == '__main__':
	unittest.main()