import unittest
import unittest.mock as mock

import json

from ocpi_tests.v2_2.test_base import *

from ocpi.v2_2.objects.commands import CommandResult

class CommandsTests(TestBase):
	
	def create_test_command_result(self):
		return CommandResult(result='ACCEPTED')
		
	def create_test_command_result_as_json(self):
		return json.dumps(self.create_test_command_result())

	#POST unit tests
	@without_role_autorization
	def test_post_with_token_c_and_invalid_request_returns_400(self):
		self.assertAuthorizationWithTokenC(400, '/ocpi/emsp/2.2/commands/RESERVE_NOW/2', self.POST(), data=self.create_test_command_result_as_json())

	def test_post_with_token_a_returns_401(self):
		self.assertAuthorizationWithTokenA(401, '/ocpi/emsp/2.2/commands/RESERVE_NOW/2', self.POST(), data=self.create_test_command_result_as_json())

	def test_post_without_token_returns_401(self):
		self.assertAuthorizationWithoutToken(401, '/ocpi/emsp/2.2/commands/RESERVE_NOW/2', self.POST())


	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_post_command_with_invalid_command_id_request_returns_2001_status_code(self, mock_controller):
		def update_command_result_side_effect(command_type, command_id, command_result):
			return False
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		mock_proxy.update_command_result.side_effect = update_command_result_side_effect

		response = self.performRequest('/ocpi/emsp/2.2/commands/UNLOCK_CONNECTOR/2', self.POST(), data=self.create_test_command_result_as_json())
		
		self.assertValidOcpiResponseFormat(response, 2001, 'Command UNLOCK_CONNECTOR/2 does not exist in the eMSP system')


	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	def test_post_command_updates_command_result(self, mock_controller):
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy

		response = self.performRequest('/ocpi/emsp/2.2/commands/UNLOCK_CONNECTOR/2', self.POST(), data=self.create_test_command_result_as_json())

		mock_proxy.update_command_result.assert_called_with('UNLOCK_CONNECTOR', '2', self.create_test_command_result())
		self.assertValidOcpiResponseFormat(response, 1000, 'Success')


if __name__ == '__main__':
	unittest.main()