import unittest
import unittest.mock as mock

import json

from ocpi_tests.v2_2.test_base import *

from ocpi.v2_2.objects.clientinfo import ClientInfo

	
class ClientInfoTests(TestBase):

	def resolve_test_token_a_throws(self, token):
		raise Exception('A very specific bad thing happened.')

	def resolve_test_token_c(self, token):
		return Credentials(token=token, url='test_url', roles=[CredentialsRole(role='CPO', business_details=BusinessDetails(name= 'Unit Tests'), party_id= 'UNI', country_code= 'DE') ])
	
	def create_test_clientinfo(self):
		return ClientInfo(country_code="BE",party_id="BEK",role="EMSP", status="CONNECTED", last_updated="2015-06-28T08:12:01Z")
	
	# GET unit tests
	@without_role_autorization
	def test_get_with_token_c_and_invalid_request_returns_400(self):
		self.assertAuthorizationWithTokenC(400, '/ocpi/emsp/2.2/clientinfo/BE/BEC', self.GET())

	def test_get_with_token_a_returns_401(self):
		self.assertAuthorizationWithTokenA(401, '/ocpi/emsp/2.2/clientinfo/BE/BEC', self.GET())

	def test_get_without_token_returns_401(self):
		self.assertAuthorizationWithoutToken(401, '/ocpi/emsp/2.2/clientinfo/BE/BEC', self.GET())

	@with_custom_authentication(resolve_test_token_c, None)
	def test_get_tariff_with_non_matching_credentials_returns_403(self):
		self.assertHttpCode(403, '/ocpi/emsp/2.2/clientinfo/FR/UNI', self.GET())

	def configure_mock_controller_with_get_clientinfo(self, mock_controller, get_clientinfo_side_effect):
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		mock_proxy.get_clientinfo.side_effect = get_clientinfo_side_effect
		return mock_proxy

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_clientinfo_returns_clientinfo_response(self, mock_controller):
		def get_clientinfo_side_effect(country_code, party_id):
			if country_code == "BE" and party_id == 'BEK':
				return self.create_test_clientinfo()

		self.configure_mock_controller_with_get_clientinfo(mock_controller, get_clientinfo_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/clientinfo/BE/BEK', self.GET())
			
		json = response.get_json()['data']
		self.assertIsNotNone(ClientInfo(json))

	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_get_clientinfo_with_invalid_arguments_returns_2001_status_code(self, mock_controller):
		def get_clientinfo_side_effect(country_code, party_id):
			return None
		self.configure_mock_controller_with_get_clientinfo(mock_controller, get_clientinfo_side_effect)

		response = self.performRequest('/ocpi/emsp/2.2/clientinfo/BE/BEC', self.GET())

		self.assertValidOcpiResponseFormat(response, 2001, 'ClientInfo BE/BEC does not exist in the eMSP system')


	#PUT unit tests
	def test_put_with_token_c_and_invalid_request_returns_400(self):
		self.assertAuthorizationWithTokenC(400, '/ocpi/emsp/2.2/clientinfo', self.PUT())

	def test_put_with_token_a_returns_401(self):
		self.assertAuthorizationWithTokenA(401, '/ocpi/emsp/2.2/clientinfo', self.PUT())

	def test_put_without_token_returns_401(self):
		self.assertAuthorizationWithoutToken(401, '/ocpi/emsp/2.2/clientinfo', self.PUT())


	@mock.patch('ocpi.v2_2.emsp.credentials.DBController.controller')
	@without_authentication
	@without_role_autorization
	def test_put_clientinfo_updates_clientinfo(self, mock_controller):
		mock_proxy = mock.MagicMock()
		mock_controller.return_value = mock_proxy
		clientinfo = self.create_test_clientinfo()
		
		response = self.performRequest('/ocpi/emsp/2.2/clientinfo', self.PUT(), data=json.dumps(clientinfo))

		mock_proxy.add_or_update_clientinfo.assert_called_with('BE', 'BEK', clientinfo)
		json_response = response.get_json()['data']
		self.assertIsNotNone(ClientInfo(json_response))
		self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
	unittest.main()