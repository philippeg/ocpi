import unittest
import datetime

from ocpi.v2_2.objects.tokens import *

class TokensObjectTests(unittest.TestCase):

	def test_energy_contract_schema_with_missing_required_property(self):
		json_str ={
			"contract_id": "123abc"
			}
		with self.assertRaises(ValueError):
			EnergyContract(json_str)

	def test_energy_contract_schema_with_valid_json(self):
		json_str ={
			"supplier_name": "me",
			"contract_id": "123abc"
			}
		obj = EnergyContract(json_str)
		self.assertEqual(json_str, obj)

	def test_token_schema_with_missing_required_property(self):
		json_str ={
			'country_code': "BE",
			'party_id': "BEK",
			'uid': "token1",
			'contract_id': 'contract1',
			'visual_number': '1234567890',
			'issuer': 'me',
			'group_id': '123abc',
			'valid': True,
			'whitelist': 'ALWAYS',
			'language': 'en',
			'default_profile_type': 'FAST',
			'energy_contract': {"supplier_name": "you","contract_id": "123abc"},
			'last_updated': "2017-03-16T10:10:02Z",
			}
		with self.assertRaises(ValueError):
			Token(json_str)

	def test_token_schema_with_valid_json(self):
		json_str ={
			'country_code': "BE",
			'party_id': "BEK",
			'uid': "token1",
			'type': 'RFID',
			'contract_id': 'contract1',
			'visual_number': '1234567890',
			'issuer': 'me',
			'group_id': '123abc',
			'valid': True,
			'whitelist': 'ALWAYS',
			'language': 'en',
			'default_profile_type': 'FAST',
			'energy_contract': {"supplier_name": "you","contract_id": "123abc"},
			'last_updated': "2017-03-16T10:10:02Z",
			}
		obj = Token(json_str)
		self.assertEqual(json_str, obj)


if __name__ == '__main__':
	unittest.main()

