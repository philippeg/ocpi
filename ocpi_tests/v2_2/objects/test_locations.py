import unittest
import datetime

from ocpi.v2_2.objects.locations import *

class LocationsObjectTests(unittest.TestCase):

	def test_status_schedule_with_missing_status(self):
		json_str = {
			"period_begin": "2018-12-25T03:00:00Z",
			"period_end": "2018-12-25T05:00:00Z"
			}
		with self.assertRaises(ValueError):
			StatusSchedule(json_str)
		
	def test_status_schedule_with_missing_period_begin(self):
		json_str = {
			"period_end": "2018-12-25T05:00:00Z",
			"status": 'AVAILABLE'
			}
		with self.assertRaises(ValueError):
			StatusSchedule(json_str)
		
	def test_status_schedule_with_valid_json(self):
		json_str = {
			"period_begin": "2018-12-25T03:00:00Z",
			"period_end": "2018-12-25T05:00:00Z",
			"status": 'AVAILABLE'
			}
		obj = StatusSchedule(json_str)
		self.assertEqual(json_str, obj)

	def test_connector_with_invalid_names(self):
		json_str = {
			"id": "1",
			"standard": "IEC_62196_T2",
			"format": "CABLE",
			"power_type": "AC_3_PHASE",
			"voltage": 220,
			"amperage": 16,
			"tariff_id": "11",
			"last_updated": "2015-03-16T10:10:02Z"
			}
		with self.assertRaises(ValueError):
			Connector(json_str)
		
	def test_connector_with_valid_json(self):
		json_str = {
			"id": "1",
			"standard": "IEC_62196_T2",
			"format": "CABLE",
			"power_type": "AC_3_PHASE",
			"max_voltage": 220,
			"max_amperage": 16,
			"tariff_ids": ["11"],
			"last_updated": "2015-03-16T10:10:02Z"
			}
		obj = Connector(json_str)
		self.assertEqual(json_str, obj)

	def test_connector_with_all_properties(self):
		json_str = {
			"id": "1",
			"standard": "IEC_62196_T2",
			"format": "CABLE",
			"power_type": "AC_3_PHASE",
			"max_voltage": 220,
			"max_amperage": 16,
			"max_electric_power": 18,
			"tariff_ids": ["11", "12"],
			"terms_and_conditions": "Allright!",
			"last_updated": "2015-03-16T10:10:02Z"
			}
		obj = Connector(json_str)
		self.assertEqual(json_str, obj)

		
	def test_evse_with_valid_json(self):
		json_str = {
			"uid": "3256",
			"evse_id": "BE*BEC*E041503001",
			"status": "AVAILABLE",
			"status_schedule": [],
			"capabilities": ["RESERVABLE"],
			"connectors": [{
				"id": "1",
				"standard": "IEC_62196_T2",
				"format": "CABLE",
				"power_type": "AC_3_PHASE",
				"max_voltage": 220,
				"max_amperage": 16,
				"tariff_ids": ["11"],
				"last_updated": "2015-03-16T10:10:02Z"
				}, {
				"id": "2",
				"standard": "IEC_62196_T2",
				"format": "SOCKET",
				"power_type": "AC_3_PHASE",
				"max_voltage": 220,
				"max_amperage": 16,
				"tariff_ids": ["13"],
				"last_updated": "2015-03-18T08:12:01Z"
				}],
			"physical_reference": "1",
			"floor_level": "-1",
			"last_updated": "2015-06-28T08:12:01Z"
			}
		obj = EVSE(json_str)
		self.assertEqual(json_str, obj)

	def test_regular_hours_with_invalid_weekday_type(self):
		json_str = {
			"weekday": '8',
			"period_begin": "01:00",
			"period_end": "06:00"
			}
		with self.assertRaises(ValueError):
			RegularHours(json_str)
	
	def test_regular_hours_with_valid_json(self):
		json_str = {
			"weekday": 1,
			"period_begin": "01:00",
			"period_end": "06:00"
			}
		obj = RegularHours(json_str)
		self.assertEqual(json_str, obj)

	def test_exceptional_period_with_missing_required_property(self):
		json_str = {
			"period_begin": "01:00"
			}
		with self.assertRaises(ValueError):
			ExceptionalPeriod(json_str)
	
	def test_exceptional_period_with_valid_json(self):
		json_str = {
			"period_begin": "2018-12-25T03:00:00Z",
			"period_end": "2018-12-25T05:00:00Z"
			}
		obj = ExceptionalPeriod(json_str)
		self.assertEqual(json_str, obj)
		
	def test_hours_with_missing_required_property(self):
		json_str = {
			"exceptional_closings": [{
			"period_begin": "2018-12-25T03:00:00Z",
			"period_end": "2018-12-25T05:00:00Z"
			}]}
		with self.assertRaises(ValueError):
			Hours(json_str)

	def test_hours_with_valid_json(self):
		json_str = {
			"twentyfourseven": True,
			"exceptional_closings": [{
			"period_begin": "2018-12-25T03:00:00Z",
			"period_end": "2018-12-25T05:00:00Z"
			}],
			"exceptional_openings": [{
			"period_begin": "2018-12-25T03:00:00Z",
			"period_end": "2018-12-25T05:00:00Z"
			}]
			}
		obj = Hours(json_str)
		self.assertEqual(json_str, obj)

	def test_location_with_missing_required_property(self):
		json_str ={
			"id": "LOC1",
			"name": "Gent Zuid",
			"address": "F.Rooseveltlaan 3A",
			"city": "Gent",
			"postal_code": "9000",
			"country_code": "BEL",
			"coordinates": {
			"latitude": "3.729944",
			"longitude": "51.047599"
			},
			"evses_uid": "3256",
			"evse_id": "BE*BEC*E041503003",
			"connectors_id": "1",
			"connectors_standard": "IEC_62196_T2",
			"connectors_format": "SOCKET",
			"connectors_power_type": "AC_1_PHASE"
			}
		with self.assertRaises(ValueError):
			Location(json_str)

	def test_location_with_valid_json(self):
		json_str = {
			"country_code": "BE",
			"party_id": "BEK",
			"id": "LOC1",
			"type": 'ON_STREET',
			"name": "Gent Zuid",
			"address": "F.Rooseveltlaan 3A",
			"city": "Gent",
			"postal_code": "9000",
			"state": "CA",
			"country": "BEL",
			"coordinates": {
				"latitude": "3.729944",
				"longitude": "51.047599"
			},
			"related_locations": [{
				"latitude": "3.729944",
				"longitude": "51.047599",
				"name": {
					"language": "fr",
					"text": "salut les amis!"
					}
				}],
			"evses": [{
				"uid": "3256",
				"evse_id": "BE*BEC*E041503001",
				"status": "AVAILABLE",
				"status_schedule": [],
				"capabilities": ["RESERVABLE"],
				"connectors": [{
					"id": "1",
					"standard": "IEC_62196_T2",
					"format": "CABLE",
					"power_type": "AC_3_PHASE",
					"max_voltage": 220,
					"max_amperage": 16,
					"tariff_ids": ["11"],
					"last_updated": "2015-03-16T10:10:02Z"
					}, {
					"id": "2",
					"standard": "IEC_62196_T2",
					"format": "SOCKET",
					"power_type": "AC_3_PHASE",
					"max_voltage": 220,
					"max_amperage": 16,
					"tariff_ids": ["13"],
					"last_updated": "2015-03-18T08:12:01Z"
					}],
				"physical_reference": "1",
				"floor_level": "-1",
				"last_updated": "2015-06-28T08:12:01Z"
				}],
			"directions": [{
				"language": "fr",
				"text": "salut les amis!"
			}],
			"operator": {'name': 'me'},
			"suboperator": {'name': 'you', 'website': 'you.com'},
			"owner": {'name': 'he'},
			"facilities": ['RESTAURANT'],
			"time_zone": "Europe/Oslo",
			"opening_times": {
				"twentyfourseven": True,
				"exceptional_closings": [{
				"period_begin": "2018-12-25T03:00:00Z",
				"period_end": "2018-12-25T05:00:00Z"
				}],
				"exceptional_openings": [{
				"period_begin": "2018-12-25T03:00:00Z",
				"period_end": "2018-12-25T05:00:00Z"
				}]
				},
			"charging_when_closed": False,
			"images": [{'url': 'ekaros.io', 'category': 'CHARGER', 'type': 'jpeg'}],
			'energy_mix': {
				"is_green_energy": False,
				"energy_sources": [
					{ "source": "GENERAL_GREEN", "percentage": 35.9 },
					{ "source": "GAS", "percentage": 6.3 },
					{ "source": "COAL", "percentage": 33.2 },
					{ "source": "GENERAL_FOSSIL", "percentage": 2.9 },
					{ "source": "NUCLEAR", "percentage": 21.7 }
					],
				"environ_impact": [
					{ "category": "NUCLEAR_WASTE", "amount": 0.0006 },
					{ "category": "CARBON_DIOXIDE", "amount": 372 }
					],
				"supplier_name": "E.ON Energy Deutschland",
				"energy_product_name": "E.ON DirektStrom eco"
				},
			"last_updated": "2015-03-16T10:10:02Z"
			}
		obj = Location(json_str)
		self.assertEqual(json_str, obj)


if __name__ == '__main__':
	unittest.main()