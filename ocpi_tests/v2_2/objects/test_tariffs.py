import unittest
import datetime

from ocpi.v2_2.objects.tariffs import *

class TariffsObjectTests(unittest.TestCase):
	def test_price_component_schema_with_missing_required_property(self):
		json_str ={
			"price": 1.1,
			"vat": 0.1,
			"step_size": 2
			}
		with self.assertRaises(ValueError):
			PriceComponent(json_str)

	def test_price_component_schema_with_valid_json(self):
		json_str ={
			"type": "ENERGY",
			"price": 1.1,
			"vat": 0.1,
			"step_size": 2
			}
		obj = PriceComponent(json_str)
		self.assertEqual(json_str, obj)


	def test_tariff_restrictions_schema_with_invalid_start_time(self):
		json_str ={
			'start_time': "28-30",
			'end_time': "12:48",
			'start_date': "2015-12-27",
			'end_date': "2015-12-24",
			'min_kwh': 20,
			'max_kwh': 50,
			'min_current': 5,
			'max_current': 20,
			'min_power': 5,
			'max_power': 20,
			'min_duration': 60,
			'max_duration': 3600,
			'day_of_week':  ['MONDAY', 'WEDNESDAY'],
			'reservation': 'RESERVATION',
			}
		with self.assertRaises(ValueError):
			TariffRestrictions(json_str)

	def test_tariff_restrictions_schema_with_valid_json(self):
		json_str ={
			'start_time': "12:30",
			'end_time': "12:48",
			'start_date': "2015-12-27",
			'end_date': "2015-12-24",
			'min_kwh': 20,
			'max_kwh': 50,
			'min_current': 5,
			'max_current': 20,
			'min_power': 5,
			'max_power': 20,
			'min_duration': 60,
			'max_duration': 3600,
			'day_of_week':  ['MONDAY', 'WEDNESDAY'],
			'reservation': 'RESERVATION',
			}
		obj = TariffRestrictions(json_str)
		self.assertEqual(json_str, obj)

	def test_tariff_element_schema_with_missing_required_property(self):
		json_str ={
			"price_components": [],
			"restrictions": {'start_time': "12:30",'end_time': "12:48",'start_date': "2015-12-27",'end_date': "2015-12-24",'min_kwh': 20,'max_kwh': 50,'min_current': 5,'max_current': 20,'min_power': 5,'max_power': 20,'min_duration': 60,'max_duration': 3600,'day_of_week': ['MONDAY', 'WEDNESDAY'],'reservation': 'RESERVATION'},
			}
		with self.assertRaises(ValueError):
			TariffElement(json_str)

	def test_tariff_element_schema_with_valid_json(self):
		json_str ={
			"price_components": [{"type": "ENERGY","price": 1.1,"vat": 0.1,"step_size": 2}],
			"restrictions": {'start_time': "12:30",'end_time': "12:48",'start_date': "2015-12-27",'end_date': "2015-12-24",'min_kwh': 20,'max_kwh': 50,'min_current': 5,'max_current': 20,'min_power': 5,'max_power': 20,'min_duration': 60,'max_duration': 3600,'day_of_week': ['MONDAY', 'WEDNESDAY'],'reservation': 'RESERVATION'},
			}
		obj = TariffElement(json_str)
		self.assertEqual(json_str, obj)

	def test_tariff_schema_with_missing_required_property(self):
		json_str ={
			'country_code': "BE",
			'id': "T01",
			'currency': "EUR",
			'type': "PROFILE_CHEAP",
			'tariff_alt_text': {"language": "en","text": "Standard Tariff"},
			'tariff_alt_url': 'hostname.be',
			'min_price': {"excl_vat": 98.51, "incl_vat": 102.54},
			'max_price': {"excl_vat": 198.51, "incl_vat": 502.54},
			'elements': [{"price_components": [{"type": "ENERGY","price": 1.1,"vat": 0.1,"step_size": 2}],"restrictions": {'start_time': "12:30",'end_time': "12:48",'start_date': "2015-12-27",'end_date': "2015-12-24",'min_kwh': 20,'max_kwh': 50,'min_current': 5,'max_current': 20,'min_power': 5,'max_power': 20,'min_duration': 60,'max_duration': 3600,'day_of_week':['MONDAY', 'WEDNESDAY'],'reservation': 'RESERVATION'}}],
			'start_date_time': "2015-03-16T10:10:02Z",
			'end_date_time': "2017-03-16T10:10:02Z",
			'energy_mix': {"is_green_energy": False,"energy_sources": [{ "source": "GENERAL_GREEN", "percentage": 35.9 },{ "source": "GAS", "percentage": 6.3 }],"environ_impact": [{ "category": "NUCLEAR_WASTE", "amount": 0.0006 },],"supplier_name": "E.ON Energy Deutschland","energy_product_name": "E.ON DirektStrom eco"},
			'last_updated': "2017-03-16T10:10:02Z",
			}
		with self.assertRaises(ValueError):
			Tariff(json_str)

	def test_tariff_schema_with_valid_json(self):
		json_str ={
			'country_code': "BE",
			'party_id': "BEC",
			'id': "T01",
			'currency': "EUR",
			'type': "PROFILE_CHEAP",
			'tariff_alt_text': {"language": "en","text": "Standard Tariff"},
			'tariff_alt_url': 'hostname.be',
			'min_price': {"excl_vat": 98.51, "incl_vat": 102.54},
			'max_price': {"excl_vat": 198.51, "incl_vat": 502.54},
			'elements': [{"price_components": [{"type": "ENERGY","price": 1.1,"vat": 0.1,"step_size": 2}],"restrictions": {'start_time': "12:30",'end_time': "12:48",'start_date': "2015-12-27",'end_date': "2015-12-24",'min_kwh': 20,'max_kwh': 50,'min_current': 5,'max_current': 20,'min_power': 5,'max_power': 20,'min_duration': 60,'max_duration': 3600,'day_of_week': ['MONDAY', 'WEDNESDAY'],'reservation': 'RESERVATION'}}],
			'start_date_time': "2015-03-16T10:10:02Z",
			'end_date_time': "2017-03-16T10:10:02Z",
			'energy_mix': {"is_green_energy": False,"energy_sources": [{ "source": "GENERAL_GREEN", "percentage": 35.9 },{ "source": "GAS", "percentage": 6.3 }],"environ_impact": [{ "category": "NUCLEAR_WASTE", "amount": 0.0006 },],"supplier_name": "E.ON Energy Deutschland","energy_product_name": "E.ON DirektStrom eco"},
			'last_updated': "2017-03-16T10:10:02Z",
			}
		obj = Tariff(json_str)
		self.assertEqual(json_str, obj)

if __name__ == '__main__':
	unittest.main()