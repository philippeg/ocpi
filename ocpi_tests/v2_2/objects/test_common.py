import unittest
import datetime

from ocpi.v2_2.objects.common import *

class CommonObjectTests(unittest.TestCase):

	def get_utc(self):
		return datetime.datetime.utcnow().isoformat()


	def test_display_text_with_missing_required_property(self):
		json_str = {
			"language": "en",
			"comment": "2.00 euro p/hour including VAT."
			}
		with self.assertRaises(ValueError):
			DisplayText(json_str)

	def test_display_text_with_valid_json(self):
		json_str = {
			"language": "en",
			"text": "2.00 euro p/hour including VAT."
			}
		obj = DisplayText(json_str)
		self.assertEqual(json_str, obj)

	def test_price_schema_with_missing_required_property(self):
		json_str ={
			"incl_vat": 102.54,
			}
		with self.assertRaises(ValueError):
			Price(json_str)

	def test_price_schema_with_valid_json(self):
		json_str ={
			"excl_vat": 98.51,
			"incl_vat": 102.54,
			}
		obj = Price(json_str)
		self.assertEqual(json_str, obj)

	def test_energy_source_with_missing_required_property(self):
		json_str = {"source": "GENERAL_GREEN"}
		with self.assertRaises(ValueError):
			EnergySource(json_str)

	def test_energy_source_with_valid_json(self):
		json_str = { "source": "GENERAL_GREEN", "percentage": 35.9 }
		obj = EnergySource(json_str)
		self.assertEqual(json_str, obj)

	def test_environmental_impact_with_missing_required_property(self):
		json_str = { "category": "NUCLEAR_WASTE"}
		with self.assertRaises(ValueError):
			EnvironmentalImpact(json_str)

	def test_environmental_impact_with_valid_json(self):
		json_str = { "category": "NUCLEAR_WASTE", "amount": 0.0006 }
		obj = EnvironmentalImpact(json_str)
		self.assertEqual(json_str, obj)

	def test_energy_mix_with_missing_required_property(self):
		json_str = {
			"energy_sources": [
			{ "source": "GENERAL_GREEN", "percentage": 35.9 },
			],
			"environ_impact": [
			{ "source": "NUCLEAR_WASTE", "amount": 0.0006 },
			],
			"supplier_name": "E.ON Energy Deutschland",
			"energy_product_name": "E.ON DirektStrom eco"
			}
		with self.assertRaises(ValueError):
			EnergyMix(json_str)

	def test_energy_mix_with_valid_json(self):
		json_str = {
			"is_green_energy": False,
			"energy_sources": [
			{ "source": "GENERAL_GREEN", "percentage": 35.9 },
			{ "source": "GAS", "percentage": 6.3 },
			{ "source": "COAL", "percentage": 33.2 },
			{ "source": "GENERAL_FOSSIL", "percentage": 2.9 },
			{ "source": "NUCLEAR", "percentage": 21.7 }
			],
			"environ_impact": [
			{ "category": "NUCLEAR_WASTE", "amount": 0.0006 },
			{ "category": "CARBON_DIOXIDE", "amount": 372 }
			],
			"supplier_name": "E.ON Energy Deutschland",
			"energy_product_name": "E.ON DirektStrom eco"
			}
		obj = EnergyMix(json_str)
		self.assertEqual(json_str, obj)
	
	def test_geo_location_with_invalid_latitude_type(self):
		json_str = {
			"latitude": "3.729944",
			"longitude": "hello"
			}
		with self.assertRaises(ValueError):
			GeoLocation(json_str)

	def test_geo_location_with_valid_json(self):
		json_str = {
			"latitude": "3.729944",
			"longitude": "51.047599"
			}
		obj = GeoLocation(json_str)
		self.assertEqual(json_str, obj)

	def test_additional_geo_location_with_invalid_name_type(self):
		json_str = {
			"latitude": "3.729944",
			"longitude": "51.047599",
			"name": 3
			}
		with self.assertRaises(ValueError):
			AdditionalGeoLocation(json_str)

	def test_additional_geo_location_with_only_required_properties(self):
		json_str = {
			"latitude": "3.729944",
			"longitude": "51.047599"
			}
		obj = AdditionalGeoLocation(json_str)
		self.assertEqual(json_str, obj)

	def test_additional_geo_location_with_valid_json(self):
		json_str = {
			"latitude": "3.729944",
			"longitude": "51.047599",
			"name": {
				"language": "fr",
				"text": "salut les amis!"
				}
			}
		obj = AdditionalGeoLocation(json_str)
		self.assertEqual(json_str, obj)
	
	def test_image_with_valid_json(self):
		json_str = {'url': 'ekaros.io', 'category': 'CHARGER', 'type': 'jpeg'}
		obj = Image(json_str)
		self.assertEqual(json_str, obj)

	def test_image_with_missing_required_properties(self):
		json_str = {'category': 'CHARGER', 'type': 'jpeg'}
		with self.assertRaises(ValueError):
			Image(json_str)

	def test_image_with_additional_property(self):
		json_str = {'url': 'ekaros.io', 'category': 'CHARGER', 'type': 'jpeg', 'prop': 'value'}
		with self.assertRaises(ValueError):
			Image(json_str)

	def test_image_with_attribute_not_in_enum(self):
		json_str = {'url': 'ekaros.io', 'category': 'SUPPLIER', 'type': 'jpeg'}
		with self.assertRaises(ValueError):
			Image(json_str)

	def test_response_with_only_mandatory_properties(self):
		json_str = {'status_code': 1000, 'timestamp': self.get_utc()}
		obj = Response(json_str)
		self.assertEqual(json_str, obj)

	def test_response_with_all_properties(self):
		json_str = {'data': 'yeah', 'status_code': 1000, 'status_message': 'OK', 'timestamp': self.get_utc()}
		obj = Response(json_str)
		self.assertEqual(json_str, obj)

	def test_response_with_unvalid_timestamp(self):
		json_str = {'status_code': 1000, 'timestamp': 'yeah'}
		with self.assertRaises(ValueError):
			Response(json_str)

	def test_response_with_string_data_property(self):
		json_str = {'data': 'yeah', 'status_code': 1000, 'status_message': 'OK', 'timestamp': self.get_utc()}
		obj = Response(json_str)
		self.assertEqual(json_str, obj)

	def test_response_with_array_data_property(self):
		json_str = {'data': ['yeah','no'], 'status_code': 1000, 'status_message': 'OK', 'timestamp': self.get_utc()}
		obj = Response(json_str)
		self.assertEqual(json_str, obj)

	def test_response_with_object_data_property(self):
		json_str = {'data': {'drink':'water'}, 'status_code': 1000, 'status_message': 'OK', 'timestamp': self.get_utc()}
		obj = Response(json_str)
		self.assertEqual(json_str, obj)

	def test_response_with_None_data_property(self):
		json_str = {'data': None, 'status_code': 1000, 'status_message': 'OK', 'timestamp': self.get_utc()}
		obj = Response(json_str)
		self.assertEqual(json_str, obj)


	def test_business_details_with_valid_json(self):
		json_str = {'name': 'EKAROS mobility GmbH', 'website': 'ekaros.io', 
			'logo':{'url': 'ekaros.io', 'category': 'OTHER', 'type': 'jpeg'}}
		obj = BusinessDetails(json_str)
		self.assertEqual(json_str, obj)

if __name__ == '__main__':
	unittest.main()
