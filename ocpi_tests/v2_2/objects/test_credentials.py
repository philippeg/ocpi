import unittest
import datetime

from ocpi.v2_2.objects.credentials import *

class CredentialsObjectTests(unittest.TestCase):
	def test_credentials_role_with_valid_json(self):
		json_str = {'role': 'CPO', 'business_details': {'name': 'EKAROS mobility GmbH'}, 'party_id': 'EKA', 'country_code': 'DE'} 
		obj = CredentialsRole(json_str)
		self.assertEqual(json_str, obj)

	def test_credentials_with_valid_json(self):
		json_str = {'token': 'abc', 'url': 'ekaros.io', 'roles':[{'role': 'CPO', 'business_details': {'name': 'EKAROS mobility GmbH'}, 'party_id': 'EKA', 'country_code': 'DE'}]} 
		obj = Credentials(json_str)
		self.assertEqual(json_str, obj)

	def test_credentials_with_empty_roles(self):
		json_str = {'token': 'abc', 'url': 'ekaros.io', 'roles':[]} 
		with self.assertRaises(ValueError):
			Credentials(json_str)

if __name__ == '__main__':
	unittest.main()