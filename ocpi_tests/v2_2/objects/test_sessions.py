import unittest
import datetime

from ocpi.v2_2.objects.sessions import *
from ocpi.v2_2.objects.cdrs import CdrDimension

class SessionsObjectTests(unittest.TestCase):

	def test_session_schema_with_missing_required_property(self):
		json_str ={
			"country_code": "BE",
			"party_id": "BEC",
			"id": "101",
			'kwh': 150.0,
			'cdr_token': {"uid": "TOKEN_ID", "type": "APP_USER", "contract_id": "CONTRACT_ID"},
			'auth_method': 'AUTH_REQUEST',
			'location_id': 'LOC1',
			'evse_uid': "3256",
			'connector_id': "1",
			'currency': "EUR",
			'total_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'status': 'COMPLETED',
			'last_updated': "2015-03-16T10:10:02Z",
			}
		with self.assertRaises(ValueError):
			Session(json_str)

	def test_session_schema_with_valid_json(self):
		json_str ={
			"country_code": "BE",
			"party_id": "BEC",
			"id": "101",
			'start_date_time': "2018-12-25T05:00:00Z",
			'end_date_time': "2018-12-25T05:12:32Z",
			'kwh': 150.0,
			'cdr_token': {"uid": "TOKEN_ID", "type": "APP_USER", "contract_id": "CONTRACT_ID"},
			'auth_method': 'AUTH_REQUEST',
			'authorization_reference': 'an authorization ref',
			'location_id': 'LOC1',
			'evse_uid': "3256",
			'connector_id': "1",
			'meter_id': "meter1",
			'currency': "EUR",
			'charging_periods': [{"start_date_time": "2018-12-25T05:00:00Z",'dimensions': [CdrDimension(type='ENERGY', volume=34.5)],'tariff_id': 'tariff_id'}],
			'total_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'status': 'COMPLETED',
			'last_updated': "2015-03-16T10:10:02Z",
			}
		obj = Session(json_str)
		self.assertEqual(json_str, obj)


if __name__ == '__main__':
	unittest.main()