import unittest
import datetime

from ocpi.v2_2.objects.cdrs import *

class CdrsObjectTests(unittest.TestCase):
	def test_cdr_token_schema_with_missing_required_property(self):
		json_str ={
			"uid": "1234abcd",
			"contract_id": "C101"
			}
		with self.assertRaises(ValueError):
			CdrToken(json_str)

	def test_cdr_session_schema_with_valid_json(self):
		json_str ={
			"uid": "1234abcd",
			"type": "OTHER",
			"contract_id": "C101"
			}
		obj = CdrToken(json_str)
		self.assertEqual(json_str, obj)

	def test_cdr_dimension_schema_with_missing_required_property(self):
		json_str ={
			"volume": 20.1
			}
		with self.assertRaises(ValueError):
			CdrDimension(json_str)

	def test_cdr_dimension_schema_with_valid_json(self):
		json_str ={
			"type": "ENERGY_EXPORT",
			"volume": 20.1
			}
		obj = CdrDimension(json_str)
		self.assertEqual(json_str, obj)

	def test_cdr_location_schema_with_missing_required_property(self):
		json_str ={
			"id": "cdr_loc1",
			"name": "Gent Zuid",
			"address": "F.Rooseveltlaan 3A",
			"city": "Gent",
			"postal_code": "9000",
			"country": "BEL",
			"evse_uid": "1",
			"evse_id": "e1",
			"connector_id": "1",
			"connector_standard": "CHADEMO",
			"connector_format": "SOCKET",
			"connector_power_type": "DC",
			}
		with self.assertRaises(ValueError):
			CdrLocation(json_str)

	def test_cdr_location_schema_with_valid_json(self):
		json_str ={
			"id": "cdr_loc1",
			"name": "Gent Zuid",
			"address": "F.Rooseveltlaan 3A",
			"city": "Gent",
			"postal_code": "9000",
			"country": "BEL",
			"coordinates": {
				"latitude": "3.729944",
				"longitude": "51.047599"
			},
			"evse_uid": "1",
			"evse_id": "e1",
			"connector_id": "1",
			"connector_standard": "CHADEMO",
			"connector_format": "SOCKET",
			"connector_power_type": "DC",
			}
		obj = CdrLocation(json_str)
		self.assertEqual(json_str, obj)

	def test_signed_value_schema_with_missing_required_property(self):
		json_str ={
			"plain_data": "blah",
			"signed_data": "xxxx"
			}
		with self.assertRaises(ValueError):
			SignedValue(json_str)

	def test_signed_value_schema_with_valid_json(self):
		json_str ={
			"nature": "blah",
			"plain_data": "blah",
			"signed_data": "xxxx"
			}
		obj = SignedValue(json_str)
		self.assertEqual(json_str, obj)

	def test_signed_data_schema_with_missing_required_property(self):
		json_str ={
			"encoding_method_version": 2,
			"public_key": "12345",
			"signed_values": [{"nature": "blah","plain_data": "blah","signed_data": "xxxx"}],
			"url": "here.com"
			}
		with self.assertRaises(ValueError):
			SignedData(json_str)

	def test_signed_data_schema_with_valid_json(self):
		json_str ={
			"encoding_method": "GP",
			"encoding_method_version": 2,
			"public_key": "12345",
			"signed_values": [{"nature": "blah","plain_data": "blah","signed_data": "xxxx"}],
			"url": "here.com"
			}
		obj = SignedData(json_str)
		self.assertEqual(json_str, obj)


	def test_cdr_schema_with_missing_required_property(self):
		json_str ={
			'country_code': "BE",
			'party_id': "BEC",
			'id': "T01",
			'end_date_time': "2017-03-16T10:10:02Z",
			'session_id':"S01",
			'cdr_token': {"uid": "1234abcd","type": "OTHER","contract_id": "C101"},
			'auth_method': 'AUTH_REQUEST',
			'authorization_reference': "blah",
			'cdr_location': {"id": "cdr_loc1","name": "Gent Zuid","address": "F.Rooseveltlaan 3A","city": "Gent","postal_code": "9000","country": "BEL","coordinates": {"latitude": "3.729944","longitude": "51.047599"},"evse_uid": "1","evse_id": "e1","connector_id": "1","connector_standard": "CHADEMO","connector_format": "SOCKET","connector_power_type": "DC"},
			'meter_id': "M01",
			'currency': "EUR",
			'tariffs': [{'country_code': "BE",'party_id': "BEC",'id': "T01",'currency': "EUR",'type': "PROFILE_CHEAP",'tariff_alt_text': {"language": "en","text": "Standard Tariff"},'tariff_alt_url': 'hostname.be','min_price': {"excl_vat": 98.51, "incl_vat": 102.54},'max_price': {"excl_vat": 198.51, "incl_vat": 502.54},'elements': [{"price_components": [{"type": "ENERGY","price": 1.1,"vat": 0.1,"step_size": 2}],"restrictions": {'start_time': "12:30",'end_time': "12:48",'start_date': "2015-12-27",'end_date': "2015-12-24",'min_kwh': 20,'max_kwh': 50,'min_current': 5,'max_current': 20,'min_power': 5,'max_power': 20,'min_duration': 60,'max_duration': 3600,'day_of_week': ['MONDAY', 'WEDNESDAY'],'reservation': 'RESERVATION'}}],'start_date_time': "2015-03-16T10:10:02Z",'end_date_time': "2017-03-16T10:10:02Z",'energy_mix': {"is_green_energy": False,"energy_sources": [{ "source": "GENERAL_GREEN", "percentage": 35.9 },{ "source": "GAS", "percentage": 6.3 }],"environ_impact": [{ "category": "NUCLEAR_WASTE", "amount": 0.0006 },],"supplier_name": "E.ON Energy Deutschland","energy_product_name": "E.ON DirektStrom eco"},'last_updated': "2017-03-16T10:10:02Z"}],
			'charging_periods': [{"start_date_time": "2018-12-25T05:00:00Z",'dimensions': [CdrDimension(type='ENERGY', volume=34.5)],'tariff_id': 'tariff_id'}], 
			'signed_data': {"encoding_method": "GP","encoding_method_version": 2,"public_key": "12345","signed_values": [{"nature": "blah","plain_data": "blah","signed_data": "xxxx"}],"url": "here.com"},
			'total_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'total_fixed_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'total_energy': 10.5,
			'total_energy_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'total_time': 660,
			'total_time_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'total_parking_time': 58,
			'total_parking_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'total_reservation_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'remark': "nothing in particular",
			'credit': True,
			'credit_reference_id': "CB1",
			'last_updated': "2017-03-16T10:10:02Z"
			}
		with self.assertRaises(ValueError):
			CDR(json_str)

	def test_cdr_schema_with_valid_json(self):
		json_str ={
			'country_code': "BE",
			'party_id': "BEC",
			'id': "T01",
			'start_date_time': "2015-03-16T10:10:02Z",
			'end_date_time': "2017-03-16T10:10:02Z",
			'session_id':"S01",
			'cdr_token': {"uid": "1234abcd","type": "OTHER","contract_id": "C101"},
			'auth_method': 'AUTH_REQUEST',
			'authorization_reference': "blah",
			'cdr_location': {"id": "cdr_loc1","name": "Gent Zuid","address": "F.Rooseveltlaan 3A","city": "Gent","postal_code": "9000","country": "BEL","coordinates": {"latitude": "3.729944","longitude": "51.047599"},"evse_uid": "1","evse_id": "e1","connector_id": "1","connector_standard": "CHADEMO","connector_format": "SOCKET","connector_power_type": "DC"},
			'meter_id': "M01",
			'currency': "EUR",
			'tariffs': [{'country_code': "BE",'party_id': "BEC",'id': "T01",'currency': "EUR",'type': "PROFILE_CHEAP",'tariff_alt_text': {"language": "en","text": "Standard Tariff"},'tariff_alt_url': 'hostname.be','min_price': {"excl_vat": 98.51, "incl_vat": 102.54},'max_price': {"excl_vat": 198.51, "incl_vat": 502.54},'elements': [{"price_components": [{"type": "ENERGY","price": 1.1,"vat": 0.1,"step_size": 2}],"restrictions": {'start_time': "12:30",'end_time': "12:48",'start_date': "2015-12-27",'end_date': "2015-12-24",'min_kwh': 20,'max_kwh': 50,'min_current': 5,'max_current': 20,'min_power': 5,'max_power': 20,'min_duration': 60,'max_duration': 3600,'day_of_week':  ['MONDAY', 'WEDNESDAY'],'reservation': 'RESERVATION'}}],'start_date_time': "2015-03-16T10:10:02Z",'end_date_time': "2017-03-16T10:10:02Z",'energy_mix': {"is_green_energy": False,"energy_sources": [{ "source": "GENERAL_GREEN", "percentage": 35.9 },{ "source": "GAS", "percentage": 6.3 }],"environ_impact": [{ "category": "NUCLEAR_WASTE", "amount": 0.0006 },],"supplier_name": "E.ON Energy Deutschland","energy_product_name": "E.ON DirektStrom eco"},'last_updated': "2017-03-16T10:10:02Z"}],
			'charging_periods': [{"start_date_time": "2018-12-25T05:00:00Z",'dimensions': [CdrDimension(type='ENERGY', volume=34.5)],'tariff_id': 'tariff_id'}], 
			'signed_data': {"encoding_method": "GP","encoding_method_version": 2,"public_key": "12345","signed_values": [{"nature": "blah","plain_data": "blah","signed_data": "xxxx"}],"url": "here.com"},
			'total_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'total_fixed_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'total_energy': 10.5,
			'total_energy_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'total_time': 660,
			'total_time_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'total_parking_time': 58,
			'total_parking_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'total_reservation_cost': {"excl_vat": 98.51,"incl_vat": 102.54},
			'remark': "nothing in particular",
			'credit': True,
			'credit_reference_id': "CB1",
			'last_updated': "2017-03-16T10:10:02Z"
			}
		obj = CDR(json_str)
		self.assertEqual(json_str, obj)
	

	def test_cdr_token_with_missing_required_property(self):
		json_str ={
			"uid": "TOKEN_ID",
			"contract_id": "CONTRACT_ID",
			}
		with self.assertRaises(ValueError):
			CdrToken(json_str)

	def test_cdr_token_with_valid_json(self):
		json_str ={
			"uid": "TOKEN_ID",
			"type": "APP_USER",
			"contract_id": "CONTRACT_ID",
			}
		obj = CdrToken(json_str)
		self.assertEqual(json_str, obj)

	def test_cdr_dimension_with_missing_required_property(self):
		json_str ={
			"type": "ENERGY",
			}
		with self.assertRaises(ValueError):
			CdrDimension(json_str)

	def test_cdr_dimension_with_valid_json(self):
		json_str ={
			"type": "ENERGY",
			"volume": 32.3
			}
		obj = CdrDimension(json_str)
		self.assertEqual(json_str, obj)

	def test_charging_period_schema_with_missing_required_property(self):
		json_str ={
			"start_date_time": "2018-12-25T05:00:00Z",
			'dimensions': [],
			'tariff_id': 'tariff_id'
			}
		with self.assertRaises(ValueError):
			ChargingPeriod(json_str)

	def test_charging_period_schema_with_valid_json(self):
		json_str ={
			"start_date_time": "2018-12-25T05:00:00Z",
			'dimensions': [CdrDimension(type='ENERGY', volume=34.5)],
			'tariff_id': 'tariff_id'
			}
		obj = ChargingPeriod(json_str)
		self.assertEqual(json_str, obj)




if __name__ == '__main__':
	unittest.main()