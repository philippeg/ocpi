import unittest
import datetime

from ocpi.v2_2.objects.versions import *

class VersionsObjectTests(unittest.TestCase):
	def test_version_with_valid_json(self):
		json_str = {'version': '2.2', 'url': "https://www.server.com/ocpi/2.2/"}
		obj = Version(json_str)
		self.assertEqual(json_str, obj)

	def test_version_with_invalid_version_number(self):
		json_str = {'version': '1.8', 'url': "https://www.server.com/ocpi/2.2/"}
		with self.assertRaises(ValueError):
			Version(json_str)
		
	def test_versions_with_valid_json(self):
		json_str = [{'version': '2.1.1', 'url': "https://www.server.com/ocpi/2.1.1/"}, {'version': '2.2', 'url': "https://www.server.com/ocpi/2.2/"}]
		obj = Versions(json_str)
		self.assertEqual(json_str, obj)

	def test_versions_with_unique_version(self):
		json_str = [{'version': '2.1.1', 'url': 'https://www.server.com/ocpi/2.1.1/'}] # [Version(version='2.2', url="https://www.server.com/ocpi/2.2/")]
		obj = Versions(json_str)
		self.assertEqual(json_str, obj)

	def test_versions_with_empty_array(self):
		json_str = []
		with self.assertRaises(ValueError):
			Versions(json_str)

	def test_endpoint_with_missing_url (self):
		json_str = {"identifier": "credentials","role": "CPO"}
		with self.assertRaises(ValueError):
			Endpoint(json_str)

	def test_endpoint_with_valid_json(self):
		json_str = {"identifier": "credentials","role": "CPO","url": "https://example.com/ocpi/cpo/2.2/credentials/"}
		obj = Endpoint(json_str)
		self.assertEqual(json_str, obj)

	def test_endpoints_with_missing_empty_endpoint_array(self):
		json_str = {"version": "2.2","endpoints": []}
		with self.assertRaises(ValueError):
			Endpoints(json_str)


	def test_endpoints_with_valid_cpo_with_only_two_modules(self):
		json_str = {"version": "2.2","endpoints": [
			{'identifier': "credentials", "role": "CPO", "url": "https://example.com/ocpi/cpo/2.2/credentials/"},
			{'identifier': "locations", "role": "CPO", "url": "https://example.com/ocpi/cpo/2.2/locations/"}
		]}
		obj = Endpoints(json_str)
		self.assertEqual(json_str, obj)

	def test_endpoints_with_valid_both_cpo_and_emsp_with_only_two_modules(self):
		json_str = {"version": "2.2", "endpoints": [
			{"identifier": "credentials", "role": "CPO","url": "https://example.com/ocpi/2.2/credentials/"},
			{"identifier": "locations", "role": "CPO", "url": "https://example.com/ocpi/cpo/2.2/locations/"},
			{"identifier": "tokens", "role": "CPO", "url": "https://example.com/ocpi/cpo/2.2/tokens/"},
			{"identifier": "locations", "role": "MSP", "url": "https://example.com/ocpi/msp/2.2/locations/"},
			{"identifier": "tokens", "role": "MSP", "url": "https://example.com/ocpi/msp/2.2/tokens/"}
		]}
		obj = Endpoints(json_str)
		self.assertEqual(json_str, obj)

if __name__ == '__main__':
	unittest.main()