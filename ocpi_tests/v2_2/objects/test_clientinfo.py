import unittest
import datetime

from ocpi.v2_2.objects.clientinfo import *

class ClientInfoObjectTests(unittest.TestCase):
	def test_clientinfo_schema_with_missing_required_property(self):
		json_str ={
			'country_code': "BE",
			'party_id': "BEK",
			'status': 'CONNECTED',
			'last_updated': "2017-03-16T10:10:02Z",
			}
		with self.assertRaises(ValueError):
			ClientInfo(json_str)

	def test_clientinfo_schema_with_valid_json(self):
		json_str ={
			'country_code': "BE",
			'party_id': "BEK",
			'role': "EMSP",
			'status': 'CONNECTED',
			'last_updated': "2017-03-16T10:10:02Z",
			}
		obj = ClientInfo(json_str)
		self.assertEqual(json_str, obj)

if __name__ == '__main__':
	unittest.main()