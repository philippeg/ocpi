import unittest
import datetime

from ocpi.v2_2.objects.commands import *

class CommandsObjectTests(unittest.TestCase):

	def test_command_response_schema_with_missing_required_property(self):
		json_str ={
			'result': "ACCEPTED",
			'message': {"language": "en","text": "Standard command"},
			}
		with self.assertRaises(ValueError):
			CommandResponse(json_str)

	def test_command_response_schema_with_valid_json(self):
		json_str ={
			'result': "ACCEPTED",
			'timeout': 60,
			'message': {"language": "en","text": "Standard command"},
			}
		obj = CommandResponse(json_str)
		self.assertEqual(json_str, obj)

	def test_command_result_schema_with_missing_required_property(self):
		json_str ={
			'message': {"language": "en","text": "Standard command"},
			}
		with self.assertRaises(ValueError):
			CommandResult(json_str)

	def test_command_result_schema_with_valid_json(self):
		json_str ={
			'result': "CANCELED_RESERVATION",
			'message': {"language": "en","text": "Standard command"},
			}
		obj = CommandResult(json_str)
		self.assertEqual(json_str, obj)


	def test_cancel_reservation_schema_with_missing_required_property(self):
		json_str ={
			'response_url': "http://here.com/comamnds/12",
			}
		with self.assertRaises(ValueError):
			CancelReservation(json_str)

	def test_cancel_reservation_schema_with_valid_json(self):
		json_str ={
			'response_url': "http://here.com/comamnds/12",
			'reservation_id': "1234567890abc"
			}
		obj = CancelReservation(json_str)
		self.assertEqual(json_str, obj)

	def test_reservation_now_schema_with_missing_required_property(self):
		json_str ={
			'response_url': "http://here.com/comamnds/12",
			'expiry_date': "2017-03-16T10:10:02Z",
			'reservation_id': "R1",
			'location_id': "LOC1",
			'evse_uid': 'EVSE1',
			'authorization_reference': "abc123"
			}
		with self.assertRaises(ValueError):
			ReserveNow(json_str)

	def test_reserve_now_schema_with_valid_json(self):
		json_str ={
			'response_url': "http://here.com/comamnds/12",
			'token': {'country_code': "BE",'party_id': "BEK",'uid': "token1",'type': 'RFID','contract_id': 'contract1','visual_number': '1234567890','issuer': 'me','group_id': '123abc','valid': True,'whitelist': 'ALWAYS','language': 'en','default_profile_type': 'FAST','energy_contract': {"supplier_name": "you","contract_id": "123abc"},'last_updated': "2017-03-16T10:10:02Z",},
			'expiry_date': "2017-03-16T10:10:02Z",
			'reservation_id': "R1",
			'location_id': "LOC1",
			'evse_uid': 'EVSE1',
			'authorization_reference': "abc123"
			}
		obj = ReserveNow(json_str)
		self.assertEqual(json_str, obj)

	def test_start_session_schema_with_missing_required_property(self):
		json_str ={
			'response_url': "http://here.com/comamnds/12",
			'token': {'country_code': "BE",'party_id': "BEK",'uid': "token1",'type': 'RFID','contract_id': 'contract1','visual_number': '1234567890','issuer': 'me','group_id': '123abc','valid': True,'whitelist': 'ALWAYS','language': 'en','default_profile_type': 'FAST','energy_contract': {"supplier_name": "you","contract_id": "123abc"},'last_updated': "2017-03-16T10:10:02Z",},
			'evse_uid': 'EVSE1',
			'authorization_reference': "abc123"
			}
		with self.assertRaises(ValueError):
			StartSession(json_str)

	def test_start_session_schema_with_valid_json(self):
		json_str ={
			'response_url': "http://here.com/comamnds/12",
			'token': {'country_code': "BE",'party_id': "BEK",'uid': "token1",'type': 'RFID','contract_id': 'contract1','visual_number': '1234567890','issuer': 'me','group_id': '123abc','valid': True,'whitelist': 'ALWAYS','language': 'en','default_profile_type': 'FAST','energy_contract': {"supplier_name": "you","contract_id": "123abc"},'last_updated': "2017-03-16T10:10:02Z",},
			'location_id': "LOC1",
			'evse_uid': 'EVSE1',
			'authorization_reference': "abc123"
			}
		obj = StartSession(json_str)
		self.assertEqual(json_str, obj)

	def test_stop_session_schema_with_missing_required_property(self):
		json_str ={
			'response_url': "http://here.com/comamnds/12",
			}
		with self.assertRaises(ValueError):
			StopSession(json_str)

	def test_stop_session_schema_with_valid_json(self):
		json_str ={
			'response_url': "http://here.com/comamnds/12",
			'session_id': 'S1',
			}
		obj = StopSession(json_str)
		self.assertEqual(json_str, obj)

	def test_unlock_connector_schema_with_missing_required_property(self):
		json_str ={
			'response_url': "http://here.com/comamnds/12",
			'location_id': "LOC1",
			'evse_uid': 'EVSE1',
			}
		with self.assertRaises(ValueError):
			UnlockConnector(json_str)

	def test_unlock_connector_schema_with_valid_json(self):
		json_str ={
			'response_url': "http://here.com/comamnds/12",
			'location_id': "LOC1",
			'evse_uid': 'EVSE1',
			'connector_id': "1"
			}
		obj = UnlockConnector(json_str)
		self.assertEqual(json_str, obj)


if __name__ == '__main__':
	unittest.main()

