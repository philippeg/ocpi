import unittest
import unittest.mock as mock

from ocpi.v2_2.auth.decorators import *
from ocpi.v2_2.core.responses import ResponseHttp

from ocpi_tests.v2_2.test_base import *
from ocpi.v2_2.auth.services import *


class AuthorizationDecoratorsTests(TestBase):
	def require_credentials_token_test(self, header, expected_msg, expected_code, allow_token_a = False):
		@require_credentials_token(allow_token_a)
		def f():
			return ResponseHttp('OK', 200).make_response()
		request_mock = mock.MagicMock()
		request_mock.headers = header

		with mock.patch("ocpi.v2_2.auth.decorators.request", request_mock):
			self.assertEqual(f(), (expected_msg, expected_code))


	def test_make_header_valid_token(self):
		header = AuthorizationService.make_header('abc123')
		self.assertEqual({'Authorization': 'Token abc123'}, header)

	def test_make_header_undefined_token(self):
		header = AuthorizationService.make_header(None)
		self.assertEqual({'Authorization': 'Token None'}, header)

	def test_missing_auth_header(self):
		self.require_credentials_token_test({}, 'credentials token is missing', 401)

	def test_empty_auth_header(self):
		self.require_credentials_token_test({'Authorization': None}, 'credentials token is missing', 401)


	@with_test_token_authentication
	def test_missing_token(self):
		self.require_credentials_token_test({'Authorization': 'Token'}, 'invalid credentials', 401)


	@with_test_token_authentication
	def test_invalid_credentials(self):
		self.require_credentials_token_test({'Authorization': 'Token X'}, 'invalid credentials', 401)

	def resolve_test_token_a_throws(self, token):
		raise Exception('A very specific bad thing happened.')

	def resolve_test_token_c_throws(self, token):
		raise Exception('A very specific bad thing happened.')

	@with_custom_authentication(resolve_test_token_c_throws, resolve_test_token_a_throws)
	def test_throws_during_validation(self):
		self.require_credentials_token_test({'Authorization': 'Token X'}, 'unable to validate credentials', 401)


	@with_test_token_authentication
	def test_valid_token_c_without_allow_a(self):
		with app.app_context():
			self.require_credentials_token_test(self.AuthorizationTokenC(), 'OK', 200, False)

	@with_test_token_authentication
	def test_valid_token_c_with_allow_a(self):
		with app.app_context():
			self.require_credentials_token_test(self.AuthorizationTokenC(), 'OK', 200)

	@with_test_token_authentication
	def test_valid_token_a_with_allow_a(self):
		with app.app_context():
			self.require_credentials_token_test(self.AuthorizationTokenA(), 'OK', 200, True)


	@with_test_token_authentication
	def test_valid_token_a_without_allow_a(self):
		with app.app_context():
			self.require_credentials_token_test(self.AuthorizationTokenA(), 'invalid credentials', 401, False)


	def require_role_authorization_test(self, mock_g, expected_msg, expected_code, country_code = None, party_id = None):
		@require_role_authorization()
		def f(country_code, party_id):
			return ResponseHttp('OK', 200).make_response()
		with mock.patch("ocpi.v2_2.auth.decorators.g", mock_g):
			self.assertEqual(f(country_code = country_code, party_id = party_id), (expected_msg, expected_code))

	def test_require_role_authorization_no_assigned_credentials_in_context_returns_401(self):
		mock_g = object()
		self.require_role_authorization_test(mock_g, 'no available credentials', 401)

	def test_require_role_authorization_with_none_credentials_in_context_returns_401(self):
		mock_g = mock.MagicMock()
		mock_g.credentials = None
		self.require_role_authorization_test(mock_g, 'invalid credentials', 401)

	def test_require_role_authorization_with_non_matching_country_returns_403(self):
		mock_g = mock.MagicMock()
		role = CredentialsRole(business_details=BusinessDetails(name= 'name'), party_id= 'UNI', country_code= 'DE', role='CPO')
		mock_g.credentials = Credentials(token='abc123', url='url', roles=[role])
	
		self.require_role_authorization_test(mock_g, 'Invalid Location or unauthorized access', 403, country_code = 'FR', party_id = 'UNI')

	def test_require_role_authorization_with_non_matching_party_returns_403(self):
		mock_g = mock.MagicMock()
		role = CredentialsRole(business_details=BusinessDetails(name= 'name'), party_id= 'UNI', country_code= 'DE', role='CPO')
		mock_g.credentials = Credentials(token='abc123', url='url', roles=[role])
	
		self.require_role_authorization_test(mock_g, 'Invalid Location or unauthorized access', 403, country_code = 'DE', party_id = 'TES')

	def test_require_role_authorization_with_matching_role_passes(self):
		mock_g = mock.MagicMock()
		role = CredentialsRole(business_details=BusinessDetails(name= 'name'), party_id= 'UNI', country_code= 'DE', role='CPO')
		mock_g.credentials = Credentials(token='abc123', url='url', roles=[role])
	
		self.require_role_authorization_test(mock_g, 'OK', 200, country_code = 'DE', party_id = 'UNI')

	