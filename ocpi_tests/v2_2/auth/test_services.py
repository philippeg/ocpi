import unittest
import unittest.mock as mock

from ocpi_tests.v2_2.test_base import TestBase, with_test_token_authentication

from ocpi.v2_2.auth.services import AuthorizationService
from ocpi.v2_2.core.responses import ResponseHttp
from ocpi.v2_2.objects.credentials import Credentials

class AuthorizationServiceTests(TestBase):
	def test_parse_authorization_header_with_empty_value_returns_none(self):
		auth = AuthorizationService()
		for value in [None, '']:
			result = auth.parse_authorization_header(value)
			self.assertIsNone(result)

	def test_parse_authorization_header_with_missing_prefix_returns_none(self):
		auth = AuthorizationService()
		result = auth.parse_authorization_header("123")
		self.assertIsNone(result)

	def test_parse_authorization_header_with_invalid_token_prefix_returns_none(self):
		auth = AuthorizationService()
		result = auth.parse_authorization_header("TOKKEN 123")
		self.assertIsNone(result)

	def test_parse_authorization_header_with_valid_value_returns_token(self):
		auth = AuthorizationService()
		result = auth.parse_authorization_header("TOKEN 123")
		self.assertEqual('123',result)

	def test_generate_token_returns_expected_value(self):
		token = AuthorizationService.generate_token()
		self.assertIsInstance(token, str)
		self.assertNotIn(' ', token)
		self.assertIn('-', token)
		self.assertEqual(36, len(token))

	@with_test_token_authentication
	def test_resolve_credentials_from_token_a_unknown_token_returns_none(self):
		token = 'Token 123'
		auth = AuthorizationService()
		result = auth.resolve_credentials_from_token_a(token)
		self.assertIsNone(result)

	@with_test_token_authentication
	def test_resolve_credentials_from_token_c_unknown_token_returns_none(self):
		token = 'Token 123'
		auth = AuthorizationService()
		result = auth.resolve_credentials_from_token_c(token)
		self.assertIsNone(result)

	@with_test_token_authentication
	def test_resolve_credentials_from_token_c_valid_token_returns_credentials(self):
		token = 'Token {}'.format(self.TokenC())
		auth = AuthorizationService()
		result = auth.resolve_credentials_from_token_c(token)
		self.assertIsNotNone(result)
		self.assertIsInstance(result, Credentials)

	@with_test_token_authentication
	def test_resolve_credentials_from_token_a_valid_token_returns_credentials(self):
		token = 'Token {}'.format(self.TokenA())
		auth = AuthorizationService()
		result = auth.resolve_credentials_from_token_a(token)
		self.assertIsNotNone(result)
		self.assertIsInstance(result, Credentials)

