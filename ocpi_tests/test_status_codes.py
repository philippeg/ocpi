import unittest
from ocpi_tests.v2_2.test_base import TestBase

# 5. Status codes

class StatusCodesTests(TestBase):

	# If the requested resource does NOT exist, the server SHOULD return a HTTP 404 - Not Found.
	def test_requested_resource_does_not_exist(self):
		self.assertHttpCode(404, '/ocpi/inexistant', self.GET())