import unittest
from ocpi_tests.v2_2.test_base import *

class OpenChargePointInterfaceTests(TestBase):
	routes = [
		{'url': '/static/<path:filename>', 'method': 'GET'},
		{'url': '/ocpi/versions', 'method': 'GET'},
		{'url': '/ocpi/2.2', 'method': 'GET'},
		{'url': '/ocpi/emsp/2.2/credentials', 'method': 'GET'},
		{'url': '/ocpi/emsp/2.2/credentials', 'method': 'POST'},
		{'url': '/ocpi/emsp/2.2/credentials', 'method': 'PUT'},
		{'url': '/ocpi/emsp/2.2/credentials', 'method': 'DELETE'},
		{'url': '/ocpi/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>', 'method': 'GET'},
		{'url': '/ocpi/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>/<string:evse_uid>', 'method': 'GET'},
		{'url': '/ocpi/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>/<string:evse_uid>/<string:connector_id>', 'method': 'GET'},
		{'url': '/ocpi/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>', 'method': 'PUT'},
		{'url': '/ocpi/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>/<string:evse_uid>', 'method': 'PUT'},
		{'url': '/ocpi/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>/<string:evse_uid>/<string:connector_id>', 'method': 'PUT'},
		{'url': '/ocpi/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>', 'method': 'PATCH'},
		{'url': '/ocpi/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>/<string:evse_uid>', 'method': 'PATCH'},
		{'url': '/ocpi/emsp/2.2/locations/<string:country_code>/<string:party_id>/<string:location_id>/<string:evse_uid>/<string:connector_id>', 'method': 'PATCH'},
		{'url': '/ocpi/emsp/2.2/sessions/<string:country_code>/<string:party_id>/<string:session_id>', 'method': 'GET'},
		{'url': '/ocpi/emsp/2.2/sessions/<string:country_code>/<string:party_id>/<string:session_id>', 'method': 'PUT'},
		{'url': '/ocpi/emsp/2.2/sessions/<string:country_code>/<string:party_id>/<string:session_id>', 'method': 'PATCH'},
		{'url': '/ocpi/emsp/2.2/cdrs/<string:country_code>/<string:party_id>/<string:cdr_id>', 'method': 'GET'},
		{'url': '/ocpi/emsp/2.2/cdrs/<string:country_code>/<string:party_id>', 'method': 'POST'},
		{'url': '/ocpi/emsp/2.2/tariffs/<string:country_code>/<string:party_id>/<string:tariff_id>', 'method': 'GET'},
		{'url': '/ocpi/emsp/2.2/tariffs/<string:country_code>/<string:party_id>/<string:tariff_id>', 'method': 'PUT'},
		{'url': '/ocpi/emsp/2.2/tariffs/<string:country_code>/<string:party_id>/<string:tariff_id>', 'method': 'DELETE'},
		{'url': '/ocpi/emsp/2.2/tokens', 'method': 'GET'},
		{'url': '/ocpi/emsp/2.2/commands/<string:command_type>/<string:command_id>', 'method': 'POST'},
		{'url': '/ocpi/emsp/2.2/clientinfo/<string:country_code>/<string:party_id>', 'method': 'GET'},
		{'url': '/ocpi/emsp/2.2/clientinfo', 'method': 'PUT'},]

	def test_ocpi_endpoints(self):
		expected_routes = self.routes
		public_routes = self.collect_routes()
		self.assertCountEqual(expected_routes, public_routes)
