# include standard modules
import argparse
from ocpi.v2_2.db.aws.aws_dynamodb_initializer import AwsDynamoDbInitializer

def main():
	# initiate the parser
	text="OCPI AWS dynamodb configuration tool."
	parser = argparse.ArgumentParser(description= text)
	parser.add_argument("--create", "-c", help="create the aws dynamodb tables. Example: --create all. Valid arguments are: all, credentials_a, credentials_c, locations, sessions, cdrs, tariffs, clientinfo, commands, tokens.")
	parser.add_argument("--init", "-i", help="init the aws dynamodb tables. Example: --init all. Valid arguments are: all, credentials_a, credentials_c, locations, sessions, cdrs, tariffs, clientinfo, commands, tokens. " )
	args = parser.parse_args()

	if args.create:
		print('Creating table(s): %s' % args.create)
		func = 'create_' + args.create + '_table'
		print('calling %s()' % func)
		creator = AwsDynamoDbInitializer()
		method_to_call = getattr(creator, func)
		method_to_call()

	if args.init:
		print('init table(s) %s' % args.init)
		func = 'init_' + args.create + '_table'
		print('calling %s()' % func)
		creator = AwsDynamoDbInitializer()
		method_to_call = getattr(creator, func)
		method_to_call()

if __name__ == '__main__':
	main()