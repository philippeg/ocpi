"""
This script runs the application using a development server.
It contains the definition of routes and views for the application.
"""
import sys

from flask import Flask, jsonify
from ocpi.app import OCPI

app = Flask(__name__)
wsgi_app = app.wsgi_app

ocpi = OCPI(app)


if __name__ == '__main__':
	import os
	_HOST = os.environ.get('SERVER_HOST', 'localhost')
	try:
		PORT = int(os.environ.get('SERVER_PORT', '5555'))
	except ValueError:
		PORT = 5555
	app.run(_HOST, PORT)
